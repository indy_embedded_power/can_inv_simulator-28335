/******************************************************************************
  FILENAME: Common_Led.h

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     3/2/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_LED_H        
#define COMMON_LED_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP280xx Header files


//****************************************
//  structure definitions
//****************************************


//****************************************
//  constant definitions
//****************************************
#define LED_MODE_OFF                      0
#define LED_MODE_BLINK_SLOW               1
#define LED_MODE_BLINK_FAST               2
#define LED_MODE_ON                       3


//****************************************
//  global variable definitions 
//****************************************


//****************************************
//  function macro definitions
//****************************************


//****************************************
//  function prototypes 
//****************************************
void LedInit(void);
void LedHandler(void);
void LedSetMode(Uint16 mode);
Uint16 LedGetMode(void);



#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
