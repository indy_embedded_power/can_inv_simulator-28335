/******************************************************************************
  FILENAME: App_Main.h

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     2/11/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:
    This file defines variables common throughout the system


*******************************************************************************/
#ifndef MAIN_H
#define MAIN_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************



//****************************************
//  structure definitions
//****************************************


//****************************************
//  constant definitions
//****************************************
#define PWM_FREQUENCY                     (20160L)

#define NUM_CHANNELS                      (2)         // number of H-bridge channels to control
#define CHAN1                             0           // indicies into arrays for channel variables
#define CHAN2                             1
#define CHAN3                             2

#define SQRT2                             (1.41421356)
#define SQRT2INV                          (0.70710678)

#define VDC_MAX                           (1022.90)
#define VDC_MAX_INV                       (1/VDC_MAX)
#define VPK_MAX                           (511.45)
#define VPK_MAX_INV                       (1/VPK_MAX)
#define IPK_MAX                           (240.0)
#define IPK_MAX_INV                       (1/IPK_MAX)


//****************************************
//  global variable definitions 
//****************************************


//****************************************
//  function macro definitions
//****************************************


//****************************************
//  function prototypes 
//****************************************


#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
