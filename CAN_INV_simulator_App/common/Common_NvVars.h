/******************************************************************************
  FILENAME: Common_NvVars.h

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     2/18/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_NVVARS_H        
#define COMMON_NVVARS_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                                      // DSP2833x Header files
#include "Common_AppTimers.h"                                   // Application Timer prototypes
#include "App_InvCtrl.h"                                        // prototypes for INVCTRL
#include "App_Main.h"                                           // prototypes for NUM_CHANNELS


//****************************************
//  constant definitions
//****************************************
#define ROMNVMEM_ADDRESS        (Uint32)0x00330000              // between end of Boot and beginning of App
#define ROMNVMEM_SIZE           (Uint32)0x00008000              // 32K bytes block of flash
#define NVMEM_COPY_SIZE         (ROMNVMEM_SIZE / (16))          // copy size in 16-bit words

#define NV_UPDATE               0x01
#define NV_CLEAR                0x00

#define NV_UPDATE_TIMEOUT       (500)                           // timeout in ms


//****************************************
//  structure definitions
//****************************************
typedef struct
{                                                               // 2048 bytes total allowed
  Uint16 Checksum;                                              //    1 word  16-bit checksum of structure
  Uint16 PageGood;                                              //    1 word  incremented every time structure is updated
  Uint16 Size;                                                  //    1 word  size of structure in words
  Uint16 AutoBootFlag;                                          //    1 word  1 to autoboot, 0 to stay in bootloader

  Uint16 CanAddrA;                                              //    1 word  eCANA node address
  Uint16 CanAddrB;                                              //    1 word  eCANB node address
  char   ModelNumber[8];                                        //    8 words Device Model # as ASCII
  char   SerialNumber1[8];                                      //    8 words Most significant characters of serial # as ASCII
  char   SerialNumber2[8];                                      //    8 words Least significant characters of serial # as ASCII
  char   SeriesNumber[8];                                       //    8 words XX.YY.ZZ as ASCII
  char   HwVersion[8];                                          //    8 words XX.YY.ZZ as ASCII

  float32 SetPointIlim;                                         //    2 words Peak Current Limit Set Point in Amps
  float32 SetPointOC;                                           //    2 words Over Current Alarm Set Point in Arms
  float32 SetPointUV;                                           //    2 words Under Voltage Alarm Set Point in Vrms
  float32 SetPointOV;                                           //    2 words Over Voltage Alarm Set Point in Vrms
  float32 SetPointOVPK;                                         //    2 words Peak Over Voltage Alarm Set Point in V
  float32 SetPointVout[NUM_CHANNELS];                           //    4 words Vout CHAN1 & CHAN2 in Vrms
  float32 IlimVr;                                               //    2 words Current Limit Virtual Resistance factor
  float32 SetpointFan;                                          //    2 words Default fan PWM Set Point in pu
  float32 SetPointUVLO;                                         //    2 words DcLink Under Voltage Lockout Set Point in V
  float32 SetPointOVLO;                                         //    2 words DcLink Over Voltage Lockout Set Point in V
  float32 XfmrScale;                                            //    2 words Output transformer scale factor, i.e. 0.5 = 2:1 turns ratio
                                                                //  326 total
} NV_T;

typedef union
{
  NV_T Var;                                                     // non-volatile variable structure
  Uint16 Copy[NVMEM_COPY_SIZE];                                 // structure broken into words for copying
} NV_VARS_T;


//****************************************
//  global variable definitions 
//****************************************
extern Uint16 NvUpdateFlag;
extern APPTMR_HANDLE_T NvUpdateTimerHandle;
extern NV_VARS_T NvVars;
extern const NV_VARS_T NvDefault;

//****************************************
//  function macro definitions
//****************************************
#define NV_UPDATE_SET()         {AppTmrRestart(NvUpdateTimerHandle, NV_UPDATE_TIMEOUT); NvUpdateFlag = NV_UPDATE;}
#define NV_UPDATE_CLR()         (NvUpdateFlag = NV_CLEAR)


//****************************************
//  function prototypes 
//****************************************
void NvVarsInit(void);
void NvVarsHandler(void);


#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
