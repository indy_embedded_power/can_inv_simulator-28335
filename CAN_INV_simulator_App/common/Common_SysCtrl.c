/******************************************************************************
  FILENAME: Common_SysCtrl.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_SysCtrl.h"                           // SysCtrl function prototypes
#include "Common_CodeStart.h"                         // for asm function prototypes and linker generated variables


//****************************************
//  global variables
//****************************************


//****************************************
// local constant definitions
//****************************************
#define DSP28_DIVSEL    2                             // Divide select Enable /2 for SYSCLKOUT
#define DSP28_PLLCR     10                            // PLL control register 150 MHz device
                                                      // [150 MHz = (30MHz * 10)/2]


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
// local function prototypes
//****************************************
static void SysCtrl_PllInit(Uint16 val, Uint16 divsel);
static void SysCtrl_PeripheralClocksInit(void);


/*****************************************************************************
    FUNCTION    SysCtrlInit()

    Description:
      Initialize the System Control registers to a known state.
      - Set the PLLCR for proper SYSCLKOUT frequency
      - Set the pre-scaler for the high and low frequency peripheral clocks
      - Enable the clocks to the peripherals

    Entrance Conditions:
      watchdog disabled in startup code
      should be first function call from main()
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
void SysCtrlInit(void)
{
  EALLOW;
  DevEmuRegs.DEVICECNF.bit.ENPROT  = 1;               // Memory Protection Configuration - Enable write/read pipeline protection
  SysCtrlRegs.PLLSTS.bit.OSCOFF    = 0;               // Signal from X1/X2 fed to PLL
  EDIS;
  
  SysCtrl_PllInit(DSP28_PLLCR, DSP28_DIVSEL);         // Initialize the PLL control: PLLCR and DIVSEL
  
  SysCtrl_PeripheralClocksInit();                     // Initialize the peripheral clocks
}


/*****************************************************************************
    FUNCTION    SysCtrlFlashInit()

    Description:
      initializes the Flash Control registers

    Entrance Conditions:
      This function MUST be executed out of RAM. Executing it
      out of OTP/Flash will yield unpredictable results

      60MHZ SYSCLK is assumed
            
    Exit Conditions:
     
    args:
      void
      
    returns:
      void
 *****************************************************************************/
#pragma CODE_SECTION(SysCtrlFlashInit, "ramfuncs");
void SysCtrlFlashInit(void)
{
  EALLOW;
  FlashRegs.FOPT.bit.ENPIPE        = 1;               //Enable Flash Pipeline mode to improve performance of code executed from Flash.
  FlashRegs.FBANKWAIT.bit.PAGEWAIT = 5;               //Set the Paged Waitstate for the Flash (150MHZ SYSCLK)
  FlashRegs.FBANKWAIT.bit.RANDWAIT = 5;               //Set the Random Waitstate for the Flash
  FlashRegs.FOTPWAIT.bit.OTPWAIT   = 8;               //Set the Waitstate for the OTP
  FlashRegs.FSTDBYWAIT.bit.STDBYWAIT   = 0x01FF;      //CAUTION
  FlashRegs.FACTIVEWAIT.bit.ACTIVEWAIT = 0x01FF;      //ONLY THE DEFAULT VALUE FOR THESE 2 REGISTERS SHOULD BE USED
  EDIS;

  asm(" RPT #7 || NOP");                              //Force a pipeline flush to ensure that the write to the last register configured occurs before returning.
}


/*****************************************************************************
    FUNCTION    SysCtrlWatchdogInit()

    Description:
      Initializes Watchdog Timer

    Entrance Conditions:
      
    Exit Conditions:
      Watchdog enabled
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
void SysCtrlWatchdogInit(void)
{
  EALLOW;
#ifdef _DEBUG
  SysCtrlRegs.WDCR = 0x00EF;                          // disable watchdog for debug
#else
  //--- Enable the Watchdog Timer
  SysCtrlRegs.WDCR = 0x00AF;                          // bit 15-8      0's:    reserved
#endif                                                // bit 7         1:      WDFLAG, write 1 to clear
                                                      // bit 6         0:      WDDIS, 1=disable WD
                                                      // bit 5-3       101:    WDCHK, WD check bits, always write as 101b
                                                      // bit 2-0       111:    WDPS, WD prescale bits, 000: WDCLK=OSCCLK/512/64
  EDIS;
}


/*****************************************************************************
    FUNCTION    SysCtrlServiceDog()

    Description:
      resets the watchdog timer

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
void SysCtrlServiceDog(void)
{
  EALLOW;
  SysCtrlRegs.WDKEY = 0x0055;
  SysCtrlRegs.WDKEY = 0x00AA;
  EDIS;
}

/*****************************************************************************
    FUNCTION    SysCtrlDisableDog()

    Description:
      Disables the watchdog timer

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
void SysCtrlDisableDog(void)
{
  EALLOW;
  SysCtrlRegs.WDCR = 0x0068;
  EDIS;
}


/*****************************************************************************
    FUNCTION    SysCtrl_PllInit()

    Description:
      initializes the PLLCR register

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      Uint16 val        PLL control register
      Uint16 divsel     Divide select
      
    returns:
      void
 *****************************************************************************/
static void SysCtrl_PllInit(Uint16 val, Uint16 divsel)
{
  volatile Uint16 iVol;

  // Make sure the PLL is not running in limp mode
  if (SysCtrlRegs.PLLSTS.bit.MCLKSTS != 0)
  {
    // OSCCLK failure detected. PLL running in limp mode. Re-enable missing clock logic.
    EALLOW;
    SysCtrlRegs.PLLSTS.bit.MCLKCLR = 1;
    EDIS;
    // Replace this line with a call to an appropriate SystemShutdown() function.
    ESTOP0;     
  }

  // DIVSEL MUST be 0 before PLLCR can be changed from 0x0000. 
  // It is set to 0 by an external reset XRSn. This puts us in 1/4
  if (SysCtrlRegs.PLLSTS.bit.DIVSEL != 0)
  {
    EALLOW;
    SysCtrlRegs.PLLSTS.bit.DIVSEL = 0;
    EDIS;
  }

  // Change the PLLCR
  if (SysCtrlRegs.PLLCR.bit.DIV != val)
  {
    EALLOW;
    SysCtrlRegs.PLLSTS.bit.MCLKOFF = 1;               // Before setting PLLCR turn off missing clock detect logic
    SysCtrlRegs.PLLCR.bit.DIV = val;
    EDIS;

    // Wait for PLL to lock.
    while(SysCtrlRegs.PLLSTS.bit.PLLLOCKS != 1)
    {
      SysCtrlServiceDog();                            // Service Watchdog while waiting
    }

    EALLOW;
    SysCtrlRegs.PLLSTS.bit.MCLKOFF = 0;               // Turn on missing clock detect logic
    EDIS;
  }

  // If switching to 1/2
  if ((divsel == 1) || (divsel == 2))
  {
    EALLOW;
    SysCtrlRegs.PLLSTS.bit.DIVSEL = divsel;
    EDIS;
  }
  
  if (divsel == 3)
  {
    EALLOW;
    SysCtrlRegs.PLLSTS.bit.DIVSEL = 2;                // If switching to 1/1 * First go to 1/2 and let the power settle
    DELAY_US(100L);                                   // The time required will depend on the system, this is only an example
    SysCtrlRegs.PLLSTS.bit.DIVSEL = 3;                // * Then switch to 1/1
    EDIS;
  }
}


/*****************************************************************************
    FUNCTION    SysCtrl_PeripheralClocksInit()

    Description:
      Initializes the clocks to the peripheral modules.
      To reduce power, leave clocks to unused peripherals disabled

      Note: If a peripherals clock is not enabled then you cannot
      read or write to the registers for that peripheral

    Entrance Conditions:
            
    Exit Conditions:
      -high and low clock prescalers are set
      -clocks are enabled to each peripheral.
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
static void SysCtrl_PeripheralClocksInit(void)
{
  EALLOW;

  SysCtrlRegs.HISPCP.all = 0x0003;                    // Hi-speed periph clock prescaler, HSPCLK=SYSCLKOUT/6 = 25MHz for ADC
  SysCtrlRegs.LOSPCP.all = 0x0003;                    // Lo-speed periph clock prescaler, LSPCLK=SYSCLKOUT/6 = 25MHz for SPI/SCI

  // XCLKOUT to SYSCLKOUT ratio.  By default XCLKOUT = 1/4 SYSCLKOUT
  XintfRegs.XINTCNF2.bit.XTIMCLK = 1;                 // XTIMCLK = SYSCLKOUT/2
  XintfRegs.XINTCNF2.bit.CLKMODE = 1;                 // XCLKOUT = XTIMCLK/2
  XintfRegs.XINTCNF2.bit.CLKOFF  = 0;                 // Enable XCLKOUT

  // Peripheral clock enables =1 for the selected peripherals.
  // If you are not using a peripheral leave =0 to save on power.
  SysCtrlRegs.PCLKCR0.bit.ADCENCLK    = 1;            // ADC
  SysCtrlRegs.PCLKCR0.bit.I2CAENCLK   = 1;            // I2C
  SysCtrlRegs.PCLKCR0.bit.SCIAENCLK   = 1;            // SCI-A
  SysCtrlRegs.PCLKCR0.bit.SCIBENCLK   = 0;            // SCI-B
  SysCtrlRegs.PCLKCR0.bit.SCICENCLK   = 0;            // SCI-C
  SysCtrlRegs.PCLKCR0.bit.SPIAENCLK   = 0;            // SPI-A
  SysCtrlRegs.PCLKCR0.bit.MCBSPAENCLK = 0;            // McBSP-A
  SysCtrlRegs.PCLKCR0.bit.MCBSPBENCLK = 0;            // McBSP-B
  SysCtrlRegs.PCLKCR0.bit.ECANAENCLK  = 0;            // eCAN-A
  SysCtrlRegs.PCLKCR0.bit.ECANBENCLK  = 1;            // eCAN-B

  // TBCLKSYNC bit is handled separately in EpwmInit() since it affects ePWM synchronization.
  SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK  = 1;            // ePWM1
  SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK  = 1;            // ePWM2
  SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK  = 1;            // ePWM3
  SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK  = 1;            // ePWM4
  SysCtrlRegs.PCLKCR1.bit.EPWM5ENCLK  = 1;            // ePWM5
  SysCtrlRegs.PCLKCR1.bit.EPWM6ENCLK  = 1;            // ePWM6

  SysCtrlRegs.PCLKCR1.bit.ECAP3ENCLK  = 0;            // eCAP3
  SysCtrlRegs.PCLKCR1.bit.ECAP4ENCLK  = 1;            // eCAP4
  SysCtrlRegs.PCLKCR1.bit.ECAP5ENCLK  = 0;            // eCAP5
  SysCtrlRegs.PCLKCR1.bit.ECAP6ENCLK  = 0;            // eCAP6
  SysCtrlRegs.PCLKCR1.bit.ECAP1ENCLK  = 0;            // eCAP1
  SysCtrlRegs.PCLKCR1.bit.ECAP2ENCLK  = 1;            // eCAP2
  SysCtrlRegs.PCLKCR1.bit.EQEP1ENCLK  = 0;            // eQEP1
  SysCtrlRegs.PCLKCR1.bit.EQEP2ENCLK  = 0;            // eQEP2

  SysCtrlRegs.PCLKCR3.bit.CPUTIMER0ENCLK = 1;         // CPU Timer 0
  SysCtrlRegs.PCLKCR3.bit.CPUTIMER1ENCLK = 0;         // CPU Timer 1
  SysCtrlRegs.PCLKCR3.bit.CPUTIMER2ENCLK = 0;         // CPU Timer 2

  SysCtrlRegs.PCLKCR3.bit.DMAENCLK    = 0;            // DMA Clock
  SysCtrlRegs.PCLKCR3.bit.XINTFENCLK  = 0;            // XTIMCLK
  SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK = 1;            // GPIO input clock

  EDIS;
}


/*****************************************************************************
End of file
******************************************************************************/
