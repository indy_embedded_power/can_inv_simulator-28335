/******************************************************************************
  FILENAME: Common_Led.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     3/2/2017
  Author:         S.Maduzia
  Target Device:  TMS302F28335

 ******************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_Led.h"
#include "Common_AppTimers.h"
#include "Common_Gpio.h"



//****************************************
//  global variables
//****************************************


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************
#define LED_SLOW_BLINK_TIME               (500)
#define LED_FAST_BLINK_TIME               (250)



//****************************************
//  local variables
//****************************************
static APPTMR_HANDLE_T Led_TimerHandle;
static Uint16 Led_OperationMode;
static Uint16 Led_Status;


//****************************************
//  local function prototypes
//****************************************





/******************************************************************************
    FUNCTION    LedInit()

    Description:
      Initialize LED driver

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void LedInit(void) 
{
  Led_TimerHandle = AppTmrStart(APPTMR_SUSPENDED);    // Start timer in suspended mode
  Led_OperationMode = LED_MODE_OFF;                   // Start in OFF mode
  Led_Status = 0;
  GPIO_LED_OFF();                                     // Make sure LED is OFF
}


/******************************************************************************
    FUNCTION    LedHandler()

    Description:
      Called from main to service LED driver

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void LedHandler(void) 
{
  switch(Led_OperationMode)
  {
    case LED_MODE_OFF:
      Led_Status = 0;
      GPIO_LED_OFF();
      break;

    case LED_MODE_BLINK_SLOW:
      if (AppTmrChkStatus(Led_TimerHandle) == APPTMR_SUSPENDED)
      {
        Led_Status = !(Led_Status);
        if (Led_Status)
          GPIO_LED_ON();
        else
          GPIO_LED_OFF();
          
        AppTmrRestart(Led_TimerHandle, LED_SLOW_BLINK_TIME);
      }
      break;
      
    case LED_MODE_BLINK_FAST:
      if (AppTmrChkStatus(Led_TimerHandle) == APPTMR_SUSPENDED)
      {
        Led_Status = !(Led_Status);
        if (Led_Status)
          GPIO_LED_ON();
        else
          GPIO_LED_OFF();
          
        AppTmrRestart(Led_TimerHandle, LED_FAST_BLINK_TIME);
      }
      break;
      
    case LED_MODE_ON:
      Led_Status = 1;
      GPIO_LED_ON();
      break;

    default:
      break;
  }
}


/******************************************************************************
    FUNCTION    LedSetMode()

    Description:
      Sets operation mode of LED

    Entrance Conditions:

    Exit Conditions:

    args:
      Uint16 mode       Operational mode to set to

    returns:
      void
*******************************************************************************/
void LedSetMode(Uint16 mode) 
{
  Led_OperationMode = mode;
}


/******************************************************************************
    FUNCTION    LedGetMode()

    Description:
      Gets operation mode of LED

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      Uint16 mode       Operational mode set to
*******************************************************************************/
Uint16 LedGetMode(void)
{
  return (Led_OperationMode);
}


/*******************************************************************************
  End of file
*******************************************************************************/
