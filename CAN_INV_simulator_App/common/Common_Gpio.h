/******************************************************************************
  FILENAME: Common_GPIO.h

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef GPIO_H
#define GPIO_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                                      // DSP2833x Header files


//****************************************
// structure definitions
//****************************************


//****************************************
// constant definitions
//****************************************
#define GPIO_QUAL_PERIOD                  25                    // # of Tsysclockout periods to use for sample window on digital inputs


//****************************************
// global variable definitions 
//****************************************


//****************************************
// function macro definitions
//****************************************
#define GPIO_IS_ESTOP()                   (GpioDataRegs.GPBDAT.bit.GPIO60 == 1)
#define GPIO_IS_FIRE_ALARM()              (GpioDataRegs.GPBDAT.bit.GPIO62 == 0)

#define GPIO_IS_GENERATOR_ON()            (GpioDataRegs.GPBDAT.bit.GPIO58 == 0)
#define GPIO_GENERATOR_START()            GpioDataRegs.GPASET.bit.GPIO19 = 1
#define GPIO_GENERATOR_STOP()             GpioDataRegs.GPACLEAR.bit.GPIO19 = 1

#define GPIO_IS_CB_OPEN()                 (GpioDataRegs.GPBDAT.bit.GPIO49 == 0)
#define GPIO_IS_CB_CLOSED()               (GpioDataRegs.GPBDAT.bit.GPIO49 == 1)
#define GPIO_CB_CLOSE_EN()                GpioDataRegs.GPCSET.bit.GPIO84 = 1
#define GPIO_CB_CLOSE_DIS()               GpioDataRegs.GPCCLEAR.bit.GPIO84 = 1 
#define GPIO_CB_OPEN_EN()                 GpioDataRegs.GPCSET.bit.GPIO86 = 1
#define GPIO_CB_OPEN_DIS()                GpioDataRegs.GPCCLEAR.bit.GPIO86 = 1 

#define GPIO_IS_CONTACTOR1_ON()           (GpioDataRegs.GPADAT.bit.GPIO15 == 0)
#define GPIO_CONTACTOR1_ON()              GpioDataRegs.GPCSET.bit.GPIO85 = 1
#define GPIO_CONTACTOR1_OFF()             GpioDataRegs.GPCCLEAR.bit.GPIO85 = 1
#define GPIO_IS_CONTACTOR2_ON()           (GpioDataRegs.GPADAT.bit.GPIO12 == 0)
#define GPIO_CONTACTOR2_ON()              GpioDataRegs.GPASET.bit.GPIO30 = 1
#define GPIO_CONTACTOR2_OFF()             GpioDataRegs.GPACLEAR.bit.GPIO30 = 1
#define GPIO_IS_CONTACTOR3_ON()           (GpioDataRegs.GPBDAT.bit.GPIO48 == 0)
#define GPIO_CONTACTOR3_ON()              GpioDataRegs.GPBSET.bit.GPIO32 = 1
#define GPIO_CONTACTOR3_OFF()             GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1

#define GPIO_PRECHARGE1_ON()              GpioDataRegs.GPASET.bit.GPIO24 = 1
#define GPIO_PRECHARGE1_OFF()             GpioDataRegs.GPACLEAR.bit.GPIO24 = 1
#define GPIO_PRECHARGE2_ON()              GpioDataRegs.GPASET.bit.GPIO26 = 1
#define GPIO_PRECHARGE2_OFF()             GpioDataRegs.GPACLEAR.bit.GPIO26 = 1
#define GPIO_PRECHARGE3_ON()              GpioDataRegs.GPCSET.bit.GPIO87 = 1
#define GPIO_PRECHARGE3_OFF()             GpioDataRegs.GPCCLEAR.bit.GPIO87 = 1

#define GPIO_MUX_SEL0_SET()               GpioDataRegs.GPASET.bit.GPIO18 = 1
#define GPIO_MUX_SEL0_CLR()               GpioDataRegs.GPACLEAR.bit.GPIO18 = 1
#define GPIO_MUX_SEL1_SET()               GpioDataRegs.GPASET.bit.GPIO20 = 1
#define GPIO_MUX_SEL1_CLR()               GpioDataRegs.GPACLEAR.bit.GPIO20 = 1
#define GPIO_MUX_SEL2_SET()               GpioDataRegs.GPASET.bit.GPIO22 = 1
#define GPIO_MUX_SEL2_CLR()               GpioDataRegs.GPACLEAR.bit.GPIO22 = 1

#define GPIO_LED_ON()                     GpioDataRegs.GPBSET.bit.GPIO34 = 1
#define GPIO_LED_OFF()                    GpioDataRegs.GPBCLEAR.bit.GPIO34 = 1

#define GPIO_IS_FUSE1 ALARM()             (GpioDataRegs.GPBDAT.bit.GPIO59 == 0)
#define GPIO_IS_FUSE2 ALARM()             (GpioDataRegs.GPBDAT.bit.GPIO61 == 0)
#define GPIO_IS_FUSE3 ALARM()             (GpioDataRegs.GPCDAT.bit.GPIO63 == 0)

#define GPIO_IS_FANTACH2()                (GpioDataRegs.GPADAT.bit.GPIO13 == 1)
#define GPIO_IS_FANTACH1()                (GpioDataRegs.GPADAT.bit.GPIO14 == 1)

#define GPIO_IS_FAULT2()                  (GpioDataRegs.GPADAT.bit.GPIO21 == 1)
#define GPIO_IS_FAULT1()                  (GpioDataRegs.GPADAT.bit.GPIO23 == 1)

#define GPIO_TEST33SET()                  GpioDataRegs.GPBSET.bit.GPIO33 = 1
#define GPIO_TEST33CLR()                  GpioDataRegs.GPBCLEAR.bit.GPIO33 = 1

// Added for CANbus Timing Test
#define GPIO_TEST31SET()                  GpioDataRegs.GPASET.bit.GPIO31 = 1
#define GPIO_TEST31CLR()                  GpioDataRegs.GPACLEAR.bit.GPIO31 = 1

// Added for INV CTRL Test
#define GPIO_TEST30SET()                  GpioDataRegs.GPASET.bit.GPIO30 = 1
#define GPIO_TEST30CLR()                  GpioDataRegs.GPACLEAR.bit.GPIO30 = 1

//****************************************
// function prototypes 
//****************************************
void GpioInit(void);
void GpioAppInit(void);


#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  End of file
*******************************************************************************/
