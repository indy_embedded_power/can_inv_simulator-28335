/******************************************************************************
  FILENAME: Common_AppTimers.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_AppTimers.h"                         // Application Timer prototypes
#include "Common_CpuTimers.h"                         // prototypes for CpuTimers
#include "Common_BitMacro.h"                          // Bit manipulation macros


//****************************************
//  global variables
//****************************************


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************
static Uint32 App_Timer[APPTMR_MAX];                      // allocate timers 
static Uint32 App_TimerAllocated;                         // bitmap of timers allocated (32 app timers max)

//****************************************
// local function prototypes
//****************************************


/*****************************************************************************
    FUNCTION    AppTmrInit()

    Description:
      initialize application timer stuff

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
 *****************************************************************************/
void AppTmrInit(void)
{
  Uint16 index;

  CpuTimer0Init(NULL, 1000);                          // initialize for 1mS Period, no isr

  for (index = 0; index < APPTMR_MAX; index++)
  {
    App_Timer[index] = 0;                             // clear all timers
  }

  App_TimerAllocated = 0;                             // no timers allocated 
}


/*****************************************************************************
    FUNCTION    AppTmrStart()

    Description:
      Allocates and starts a software timer.
      usage:  AppTmrHandle = AppTmrStart(Time)
      calling fcn needs to allocate a varible of type APPTMR_HANDLE_T to hold the timer id

    Entrance Conditions:

    Exit Conditions:

    args:
      Uint32 timeout                  Timeout value in milliseconds

    returns:
      APPTMR_HANDLE_T index           Timer index (1 to APPTMR_MAX), if timer allocated
                                      NULL, if no more timers available
 *****************************************************************************/
APPTMR_HANDLE_T AppTmrStart(Uint32 timeout)
{
  APPTMR_HANDLE_T  index;

  for (index = 0; index < APPTMR_MAX; index++)        // cycle through to find a free timer
  {
    if (ISBITCLR(App_TimerAllocated,index))           // Found a free timer
    {
      App_Timer[index] = timeout;                     // initialize it
      BITSET(App_TimerAllocated,index);               // claim it
      break;                                          // done - break from for loop
    }
  }

  if (index == APPTMR_MAX)
  {
    index = NULL;                                     // no more timers available
  }
  else
  {
    index++;                                          // make timer index 1 based before returning result to caller
  }

  return(index);
}


/*****************************************************************************
    FUNCTION    AppTmrChkStatus()

    Description:
      Determines if timer is allocated, running or stopped.

    Entrance Conditions:

    Exit Conditions:

    args:
      APPTMR_HANDLE_T tmr_id      timer id (1 to APPTMR_MAX)

    returns:
      APPTMR_STATUS_T status      TMR_RUNNING,   if timer allocated/running
                                  TMR_SUSPENDED, if timer allocated/not running
                                  TMR_NOT_FOUND, if timer not allocated
 *****************************************************************************/
APPTMR_STATUS_T AppTmrChkStatus(APPTMR_HANDLE_T tmr_id)
{
  APPTMR_STATUS_T status;
  
  status = APPTMR_NOT_FOUND;                          // Assume timer not allocated 

  if ((tmr_id > 0) && (tmr_id <= APPTMR_MAX))         // ID in valid range?
  {
    tmr_id--;                                         // make id 0 based
    if (ISBITSET(App_TimerAllocated, tmr_id))         // Timer allocated?
    {
      if (App_Timer[tmr_id])                          // Timer running?
      {
        status = APPTMR_RUNNING;                      // timer allocated & running
      }
      else
      {
        status = APPTMR_SUSPENDED;                    // timer allocated but not running
      }
    }
  }

  return(status);
}


/*****************************************************************************
    FUNCTION    AppTmrDealloc()

    Description:
      de-allocates and stops a software timer.

    Entrance Conditions:

    Exit Conditions:
      timer is stopped and returned to the timer pool

    args:
      APPTMR_HANDLE_T tmr_id       Timer id (1 to APPTMR_MAX)

    returns:
      void
 *****************************************************************************/
void AppTmrDealloc(APPTMR_HANDLE_T tmr_id)
{
  if ((tmr_id > 0) && (tmr_id <= APPTMR_MAX))         // ID in valid range?
  {
    tmr_id--;                                         // make id 0 based
    if (ISBITSET(App_TimerAllocated, tmr_id))
    {
      App_Timer[tmr_id] = 0;                          // Stop timer
      BITCLR(App_TimerAllocated, tmr_id);             // de-allocate timer
    }
  }
}


/*****************************************************************************
    FUNCTION    AppTmrStop()

    Description:
      stop an allocated software timer.

    Entrance Conditions:

    Exit Conditions:
      timer is stopped but still allocated

    args:
      APPTMR_HANDLE_T tmr_id       Timer id (1 to APPTMR_MAX)

    returns:
      void
 *****************************************************************************/
void AppTmrStop(APPTMR_HANDLE_T tmr_id)
{
  if ((tmr_id > 0) && (tmr_id <= APPTMR_MAX))         // ID in valid range?
  {
    tmr_id--;                                         // make id 0 based
    if (ISBITSET(App_TimerAllocated, tmr_id))
    {
      App_Timer[tmr_id] = 0;                          // Stop timer
    }
  }
}


/*****************************************************************************
    FUNCTION    AppTmrRestart()

    Description:
      Restart the timer with the timeout specified
      
      
    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      Uint16 tmr_id       Timer id (1 to MAX_APPL_TIMERS)
      Uint32 timeout      Timeout value in milliseconds
      
    returns:
      void
 *****************************************************************************/

void AppTmrRestart(APPTMR_HANDLE_T tmr_id, Uint32 timeout)
{
  App_Timer[--tmr_id] = timeout;
}


/*****************************************************************************
    FUNCTION    AppTmrHandler()

    Description:
      update the application timers
      Polled from main loop

    args:
      void
      
    returns:
      void
 *****************************************************************************/
void AppTmrHandler(void)
{
  Uint16 index;     

  if (CpuTimer0Regs.TCR.bit.TIF == 1)
  {
    CpuTimer0Regs.TCR.bit.TIF = 1;                    // clear CpuTimer Flag

    for (index = 0; index < APPTMR_MAX; index++)      // update all App timers
    {  
      if (App_Timer[index])
      { 
        --App_Timer[index];
      } 
    }
  }
}


/*****************************************************************************
End of file
******************************************************************************/
