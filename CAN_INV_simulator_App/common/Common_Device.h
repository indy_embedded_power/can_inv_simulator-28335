/******************************************************************************
   FILENAME: Common_Device.h

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_DEVICE_H
#define COMMON_DEVICE_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "DSP2833x_Device.h"                          // DSP2833x Header files



//****************************************
// structure definitions
//****************************************
typedef unsigned int Boolean;
typedef struct BYTE8_S
{
  Uint16 BYTE0  : 8;
  Uint16 BYTE1  : 8;
  Uint16 BYTE2  : 8;
  Uint16 BYTE3  : 8;
  Uint16 BYTE4  : 8;
  Uint16 BYTE5  : 8;
  Uint16 BYTE6  : 8;
  Uint16 BYTE7  : 8;
} BYTE8_T;
typedef union LLBYTE8_U
{
  BYTE8_T byte;
  Uint64  all;
} LLBYTE8_T;

//****************************************
// constant definitions
//****************************************
#ifndef EOF
#define EOF                 (-1)                      // end of file marker
#endif
#ifndef TRUE
#define TRUE                1
#endif
#ifndef FALSE
#define FALSE               0
#endif
#ifndef NULL
#define NULL                0
#endif

#ifndef CPU_FREQ
#define CPU_FREQ            150e6
#endif

#ifndef CPU_FREQ_MHZ
#define CPU_FREQ_MHZ        (CPU_FREQ/1e6)
#endif

//****************************************
// global variable definitions 
//****************************************


//****************************************
// function macro definitions
//****************************************


//****************************************
// function prototypes 
//****************************************




#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  End of file
*******************************************************************************/
