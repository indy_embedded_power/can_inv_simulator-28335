/******************************************************************************
  FILENAME: App_Faults.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     3/3/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "App_Faults.h"                               // prototypes for Faults
#include "App_Main.h"                                 // CHAN info
#include "App_InvCtrl.h"                              // prototypes for InvCtrlStatus[]
#include "App_System.h"                               // prototypes for SystemStatus
#include "Common_Gpio.h"                              // Macros for reading GPIO
#include "Common_NvVars.h"                            // prototypes for NvVars
#include "Common_SysCtrl.h"                           // prototypes for SysCtrl


//****************************************
//  global variables
//****************************************
FAULT_STATUS_T FaultStatus;
FAULT_WARNING_STATUS_T FaultWarningStatus;
APPTMR_HANDLE_T FaultMasterHeartbeatTimerHandle;


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
//  local function prototypes
//****************************************





/******************************************************************************
    FUNCTION    FaultInit()

    Description:
      Initialize fault handler

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void FaultInit(void) 
{
  //  clear all faults
  FaultStatus.all        = 0x00;
  FaultWarningStatus.all = 0x00;
  FaultMasterHeartbeatTimerHandle  = AppTmrStart(FAULT_HEARTBEAT_TIME);
}


/******************************************************************************
    FUNCTION    FaultHandler()

    Description:
      Handle fault conditions and update FaultStatus

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void FaultHandler(void) 
{
#if 0
  float32 localSetPoint;
  Uint16  localFlags;

  //--- CB Control
  //--- DC Link UVLO
  //--- DC Link OVLO
  //--- Output Undervoltage - test for undervoltage only when channels are enabled and not in Test mode or UVLO
    // Undervoltage CHAN1
    // Undervoltage CHAN2
  //--- Output Overvoltage
    // Overvoltage CHAN1
    // Overvoltage CHAN2
  //--- Output OverCurrent
    // Overcurrent CHAN1
    // Overcurrent CHAN2
  //--- Peak OverCurrent CHAN1 & CHAN2
  //--- Peak OverVoltage CHAN1 & CHAN2
  //--- H-Bridge Fault
    // H-Bridge CHAN1
    // H-bridge CHAN2
  //--- E-Stop Fault
  //--- CAN Master Communication Fault
  // TODO: Fan Faults
  // TODO: Temperature Faults


  // for test - dummy data
  FaultWarningStatus.byte.BYTE0  = 0xAA;
  FaultWarningStatus.byte.BYTE1  = 0xDE;
  FaultWarningStatus.byte.BYTE2  = 0xAD;
  FaultWarningStatus.byte.BYTE3  = 0xBE;
  FaultWarningStatus.byte.BYTE4  = 0xEF;
#endif
}


/*******************************************************************************
  End of file
*******************************************************************************/
