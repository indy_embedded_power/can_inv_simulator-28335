/******************************************************************************
  FILENAME: App_LINKER.cmd

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     5/15/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:
    This file defines the memory map for the Application

    In addition to this memory linker command file, add the file
    /DSP2833x_headers/cmd/DSP2833x_Headers_nonBIOS.cmd directly to the project.
    This file is required to link the peripheral structures to the proper locations
    within the memory map

    Define the memory block start/length for the F28335
    PAGE 0 = program memory sections
    PAGE 1 = data memory sections

    Since the Bootloader is not implemented yet the following sections are set to BOOT_
    when Bootloader is implemented, these need to be set to APP_:
      codestart         : > BOOT_BEGIN      // change to APP_BEGIN
      csm_passwords     : > BOOT_CSM_PWL    // change to APP_CSM_PWL
      csm_reserved      : > BOOT_CSM_RSVD   // change to APP_CSM_RSVD
      

*******************************************************************************/
MEMORY
{
  /*** Program Memory ***/
  PAGE 0:
  RAML0_3       : origin = 0x008000,  length = 0x004000         // 16Kx16  on-chip RAM block L0,L1,L2,L3
  APP_FLASH     : origin = 0x300000,  length = 0x02FF70         // 192Kx16 on-chip FLASH-CDEFGH (minus 144 words for header)
  APP_DATA      : origin = 0x32FF70,  length = 0x000010         // part of FLASH-C. Critical application data (checksum, etc.)
  APP_CSM_RSVD  : origin = 0x32FF80,  length = 0x000076         // Part of FLASH-C. CSM - not used, set to 0x0000
  APP_BEGIN     : origin = 0x32FFF6,  length = 0x000002         // Part of FLASH-C. Branch to start of Application program
  APP_CSM_PWL   : origin = 0x32FFF8,  length = 0x000008         // Part of FLASH-C. CSM - not used, set to 0xFFFF
  
  NV_VARS       : origin = 0x330000,  length = 0x008000         // 32Kx16  on-chip FLASH-B. Used for nonvolatile variable storage
  
  BOOT_FLASH    : origin = 0x338000,  length = 0x007F70         // 32Kx16  on-chip FLASH-A (minus 144 words for header)
  BOOT_DATA     : origin = 0x33FF70,  length = 0x000010         // Part of FLASH-A. Critical bootloader data (checksum, etc.)
  BOOT_CSM_RSVD : origin = 0x33FF80,  length = 0x000076         // Part of FLASH-A. CSM - not used, set to 0x0000
  BOOT_BEGIN    : origin = 0x33FFF6,  length = 0x000002         // Part of FLASH-A. Branch to start of Bootloader program
  BOOT_CSM_PWL  : origin = 0x33FFF8,  length = 0x000008         // Part of FLASH-A. CSM - not used, set to 0xFFFF

  OTP           : origin = 0x380400,  length = 0x000400         // OTPROM - not used
  ADC_CAL       : origin = 0x380080,  length = 0x000009         // ADC_cal function in Reserved memory

  IQTABLES      : origin = 0x3FE000,  length = 0x000b50         // IQ Math Tables in Boot ROM
  IQTABLES2     : origin = 0x3FEB50,  length = 0x00008c         // IQ Math Tables in Boot ROM
  FPUTABLES     : origin = 0x3FEBDC,  length = 0x0006A0         // FPU Tables in Boot ROM
  ROM           : origin = 0x3FF27C,  length = 0x000D44         // Boot ROM
  RESET         : origin = 0x3FFFC0,  length = 0x000002         // Boot ROM - part starts here out of reset
  VECTORS       : origin = 0x3FFFC2,  length = 0x00003E         // Boot ROM

  /*** Data Memory ***/
  PAGE 1 :
  ROM_BOOT_RSVD : origin = 0x000000,  length = 0x000050         // Part of M0, BOOT ROM will use this for stack
  HANDSHAKE     : origin = 0x000050,  length = 0x000002         // Part of M0, Applicaton and Bootloader use this for handshake
  RAMM0         : origin = 0x000052,  length = 0x0003AE         // 1Kx16  on-chip RAM block M0 (minus 80 words for boot stack & handshake)
  RAMM1         : origin = 0x000400,  length = 0x000400         // 1Kx16  on-chip RAM block M1
  RAML4_7       : origin = 0x00C000,  length = 0x004000         // 16Kx16 on-chip RAM block L4,L5,L6,L7
}


SECTIONS
{
  /*** Compiler Required Sections ***/
  // Program Memory (Page 0) sections
  .text             : > APP_FLASH,        PAGE = 0              // executable code and constants
  .cinit            : > APP_FLASH,        PAGE = 0              // string constants, initialization of global and static variables
  .econst           : > APP_FLASH,        PAGE = 0              // far mem string constants, initialization of global and static variables
  .pinit            : > APP_FLASH,        PAGE = 0              // tables for initializing variables and constants
  .switch           : > APP_FLASH,        PAGE = 0              // tables for switch statements
  .reset            : > RESET,            PAGE = 0, TYPE = DSECT// not using this section
  vectors           : > VECTORS,          PAGE = 0, TYPE = DSECT// not using this section

  // Data Memory (PAGE 1) sections
  .bss              : > RAML4_7,          PAGE = 1              // global and static variables loaded from .cinit at startup
  .ebss             : > RAML4_7,          PAGE = 1              // far mem global and static variables loaded from .cinit at startup
  .sysmem           : > RAML4_7,          PAGE = 1              // uninitialized section dynamic memory allocation - not used
  .esysmem          : > RAML4_7,          PAGE = 1              // uninitialized section far dynamic memory allocation - not used
  .cio              : > RAML4_7,          PAGE = 1              // required by C compiler - not used
  .stack            : > RAMM1             PAGE = 1              // uninitialized section C system stack

  /*** User Defined Sections ***/
  codestart         : > BOOT_BEGIN        PAGE = 0              // used by asm function code_start in CodeStart.asm
  csm_passwords     : > BOOT_CSM_PWL      PAGE = 0              // Fill with 0xFFFF in CodeStart.asm
  csm_reserved      : > BOOT_CSM_RSVD     PAGE = 0              // Fill with 0x0000 in CodeStart.asm
  handshake         : > HANDSHAKE         PAGE = 1              // Used by Application to tell Bootloader to download new app
  app_data          : > APP_DATA          PAGE = 0              // Used by App to store critical Application const data
  boot_data         : > BOOT_DATA         PAGE = 0              // Used by Boot to store critical Bootloader const data
  m0_data_ram       : > RAMM0             PAGE = 1              // Used to put data into M0 SARAM
  nv_vars           : > NV_VARS           PAGE = 0              // Used for non-volatile configuration storage

  // Used by functions that run from RAM
  ramfuncs          : LOAD = APP_FLASH,                         // RAM functions in Flash
                      RUN  = RAML0_3,                           // RAM functions in RAM
                      LOAD_START(_RamfuncsLoadStart),
                      LOAD_END(_RamfuncsLoadEnd),
                      RUN_START(_RamfuncsRunStart),
                      PAGE = 0

  // floating point library run from RAM
  rtslib            : LOAD = APP_FLASH,                         // RTS floating point libs in Flash
                      RUN  = RAML0_3,                           // RTS floating point libs in RAM
                      LOAD_START(_rtsLibLoadStart),
                      LOAD_END(_rtsLibLoadEnd),
                      RUN_START(_rtsLibRunStart),
                      PAGE = 0
                      {
                        -l rts2800_fpu32_fast_supplement.lib < *.obj > (.text)
                        -l rts2800_fpu32.lib < sin.obj cos.obj tan.obj atan.obj fd_add.obj fd_sub.obj fs_div.obj sqrt.obj i_div.obj u_div.obj > (.text)
                      }
  // Flash functions to run from RAM
  Flash28_API       : {
                        -l Flash28335_API_V210.lib (.econst)
                        -l Flash28335_API_V210.lib (.text)
                      }
                      LOAD = APP_FLASH,
                      RUN = RAML0_3,
                      LOAD_START(_Flash28_API_LoadStart),
                      LOAD_END(_Flash28_API_LoadEnd),
                      RUN_START(_Flash28_API_RunStart),
                      PAGE = 0

  /*** IQmath Sections: */
  IQmath            : > APP_FLASH,        PAGE = 0
  IQmathTables      : > IQTABLES,         PAGE = 0, TYPE = NOLOAD
  FPUmathTables     : > FPUTABLES,        PAGE = 0, TYPE = NOLOAD

  /*** ADC_cal function (pre-programmed by factory into TI reserved memory) ***/
  .adc_cal          : load = ADC_CAL,     PAGE = 0, TYPE = NOLOAD

}


/*******************************************************************************
  End of file
*******************************************************************************/
