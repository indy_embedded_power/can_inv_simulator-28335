/******************************************************************************
  FILENAME: App_CmdMenu.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/30/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

 *****************************************************************************/
//****************************************
//  include files
//****************************************
#include <stdio.h>                                    // define sscanf() & sprintf()
#include <string.h>                                   // define strncmp()
#include "Common_Sci.h"                               // prototypes for SCI functions
#include "Common_Version.h"                           // prototypes for Version info
#include "Common_NvVars.h"                            // prototypes for NvVars
#include "Common_BitMacro.h"                          // prototypes for bit manipulation macros
#include "Common_SysCtrl.h"                           // prototypes for SysCtrl
#include "Common_Led.h"                               // Prototypes for LED
#include "App_CmdMenu.h"                              // prototypes for CMD_MENU_T
#include "App_InvCtrl.h"                              // prototypes for INVCTRL
#include "App_Main.h"                                 // defines for CHANNEL
#include "App_System.h"                               // defines for SystemStatus
#include "App_Faults.h"                               // prototypes for FaultStatus
#include "App_IpsCanDevB.h"                           // defines for CAN


//****************************************
//  local function prototypes
//****************************************
// Command functions prototypes
// These prototypes are at the top of this file so that they can be used in CmdMenuTable[] definition below
static void Cmd_Help(char *pArgs);
static void Cmd_Version(char *pArgs);
static void Cmd_Boot(char *pArgs);
static void Cmd_MemRead(char *pArgs);

static void Cmd_SinGet(char *pArgs);
static void Cmd_SinSet(char *pArgs);

static void Cmd_NvRevert(char *pArgs);
static void Cmd_NvGet(char *pArgs);
static void Cmd_NvSet(char *pArgs);

static void Cmd_SpGet(char *pArgs);
static void Cmd_SpSet(char *pArgs);

static void Cmd_MeasGet(char *pArgs);
static void Cmd_MeasSet(char *pArgs);

static void Cmd_LedGet(char *pArgs);
static void Cmd_LedSet(char *pArgs);

static void Cmd_SysGet(char *pArgs);
static void Cmd_SysSet(char *pArgs);
static void Cmd_SysClr(char *pArgs);

static void Cmd_FaultGet(char *pArgs);
static void Cmd_FaultSet(char *pArgs);
static void Cmd_FaultClr(char *pArgs);

static void Cmd_WarnGet(char *pArgs);
static void Cmd_WarnSet(char *pArgs);
static void Cmd_WarnClr(char *pArgs);

static void Cmd_SimCmdGet(char *pArgs);
static void Cmd_SimCmdSet(char *pArgs);
static void Cmd_SimCmdClr(char *pArgs);


// Helper functions prototypes 
static Uint32 Cmd_UtilScanXtoL(char **p);
static char *Cmd_UtilLtoX(char *ps, Uint32 val);


//****************************************
//  global variables
//****************************************
const CMD_LINE_T CmdMenuTable[] =
{
  {"?"        , 1, Cmd_Help,        "Display this menu"},
  {"help"     , 4, Cmd_Help,        "Display this menu"},
  {"version"  , 7, Cmd_Version,     "Software version"},
  {"boot"     , 4, Cmd_Boot,        "Go back to bootloader"},
  {"read"     , 4, Cmd_MemRead,     "Read memory: read addr"},

  {"SinGet"   , 6, Cmd_SinGet,      "Get PWM sin amplitude in pu"},
  {"SinSet"   , 6, Cmd_SinSet,      "Set PWM sin amplitude in pu: SinSet [1,2,A] [0.0...1.0]"},

  {"NvRevert" , 8, Cmd_NvRevert,    "Revert NvVars to NvDefaults"},
  {"NvGet"    , 5, Cmd_NvGet,       "Get NvVar values"},
  {"NvSet"    , 5, Cmd_NvSet,       "Set NvVar: NvSet [variable] [value]"},

  {"SpGet"    , 5, Cmd_SpGet,       "Get Setpoint values"},
  {"SpSet"    , 5, Cmd_SpSet,       "Set Setpoint value: SpSet [variable] [value]"},

  {"MeasGet"  , 7, Cmd_MeasGet,     "Get Measured values"},
  {"MeasSet"  , 7, Cmd_MeasSet,     "Set Measured value: MeasSet [variable] [value]"},

  {"LedGet"   , 6, Cmd_LedGet,      "Get Led Mode"},
  {"LedSet"   , 6, Cmd_LedSet,      "Set Led Mode: LedSet [Off,Slow,Fast,On]"},

  {"SysGet"   , 6, Cmd_SysGet,      "Get SystemStatus[] bits"},
  {"SysSet"   , 6, Cmd_SysSet,      "Set SystemStatus bit: SysSet [byte 0..7] [bit 0..7] [value 0,1]"},
  {"SysClr"   , 6, Cmd_SysClr,      "Clear SystemStatus[] bits"},

  {"FaultGet" , 8, Cmd_FaultGet,    "Get FaultStatus[] bits"},
  {"FaultSet" , 8, Cmd_FaultSet,    "Set FaultStatus bit: FaultSet [byte 0..7] [bit 0..7] [value 0,1]"},
  {"FaultClr" , 8, Cmd_FaultClr,    "Clear FaultStatus[] bits"},

  {"WarnGet"  , 7, Cmd_WarnGet,     "Get WarningStatus[] bits"},
  {"WarnSet"  , 7, Cmd_WarnSet,     "Set WarningStatus bit: WarnSet [byte 0..7] [bit 0..7] [value 0,1]"},
  {"WarnClr"  , 7, Cmd_WarnClr,     "Clear WarningStatus[] bits"},

  {"CmdGet"   , 6, Cmd_SimCmdGet,   "Get IpsCanSimCmd bits"},
  {"CmdSet"   , 6, Cmd_SimCmdSet,   "Set IpsCanSimCmd bit: CmdSet [name] [value 0,1]"},
  {"CmdClr"   , 6, Cmd_SimCmdClr,   "Clear IpsCanSimCmd bits"},

  {NULL, 0, NULL, NULL}
};


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************
#define TAB_COL              12
#define CTL_LF               0x0A                     // LineFeed control character
#define CTL_CR               0x0D                     // CarrageReturn control character

//****************************************
//  local variables
//****************************************
static char Cmd_Buffer[80];
static const char Cmd_NewLine[] = "\r\n";

static const char *Cmd_NvSetVariableDescriptors[] =
{
  "CanAdr",
  "Model",
  "Serial1",
  "Serial2",
  "Series",
  "HwVer",
  NULL
};

static const char *Cmd_SpSetVariableDescriptors[] =
{
  "Vout1",
  "Vout2",
  "UVLO",
  "OVLO",
  "OVPK",
  "UV",
  "OV",
  "OC",
  NULL
};

static const char *Cmd_MeasSetVariableDescriptors[] =
{
  "Vdc",
  "Vout1",
  "Vout2",
  "Iout1",
  "Iout2",
  "Pout1",
  "Pout2",
  "Freq1",
  "Freq2",
  "Tstk1",
  "Tstk2",
  "Text1",
  "Text2",
  NULL
};

static const char *Cmd_LedSetVariableDescriptors[] =
{
  "Off",
  "Slow",
  "Fast",
  "On",
  NULL
};

static const char *Cmd_SimCmdBitDescriptors[] =
{
  "PwmPh1",
  "PwmPh2",
  "PwmPh1and2",
  "Cb",
  "Led",
  "LedBlinkSlow",
  "LedBlinkFast",
  "ClrFaults",
  "DbugMode",
  "Rsvd9",
  "Rsvd10",
  "Rsvd11",
  "Rsvd12",
  "Rsvd13",
  "Rsvd14",
  "Rsvd15",
  NULL
};


/*****************************************************************************
    FUNCTION    Cmd_Help()

    Description:
      list available commands

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      char *pArgs     pointer to argument list from command line parser
      
    returns:
      void
 *****************************************************************************/
static void Cmd_Help(char *pArgs)
{
  const CMD_LINE_T *pCommand;
  Uint16 idx;

  SciAPuts(Cmd_NewLine);
  for (pCommand = CmdMenuTable; pCommand->cmd != NULL; ++pCommand)
  {
    SciAPuts(pCommand->cmd);
    for (idx = pCommand->len; idx < TAB_COL; ++idx)
    {
      SciAPutChar(' ');
    }
    SciAPuts(pCommand->descr);
    SciAPuts(Cmd_NewLine);
  }
  SciAPuts(Cmd_NewLine);
}


/*****************************************************************************
    FUNCTION    Cmd_Version()

    Description:
      print application version information

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      char *pArgs     pointer to argument list from command line parser
      
    returns:
      void
 *****************************************************************************/
static void Cmd_Version(char *pArgs)
{
  Uint16 checksum;

  SciAPuts(Cmd_NewLine);
  SciAPuts("Boot - ");
  checksum = VersionCheck(&BootVersion);
  if(checksum == 0xFFFF)
  {
    SciAPuts("ERROR!!! Boot FLASH is blank");
  }  
  else
  {
    sprintf(Cmd_Buffer, "FW Version: %02d.%02d.%02d%1c   Checksum: %04X [%04X]",
            BootVersion.PartNo, BootVersion.Major, BootVersion.Minor, BootVersion.DbgRev, BootVersion.Checksum, checksum);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(Cmd_NewLine);

  SciAPuts(Cmd_NewLine);
  SciAPuts("App  - ");
  checksum = VersionCheck(&AppVersion);
  if(checksum == 0xFFFF)
  {
    SciAPuts("ERROR!!! App FLASH is blank");
  }  
  else
  {
    sprintf(Cmd_Buffer, "FW Version: %02d.%02d.%02d%1c   Checksum: %04X [%04X]",
            AppVersion.PartNo, AppVersion.Major, AppVersion.Minor, AppVersion.DbgRev, AppVersion.Checksum, checksum);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(Cmd_NewLine);
}


/*****************************************************************************
    FUNCTION    Cmd_Boot()

    Description:
      Return to boot

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      char *pArgs     pointer to argument list from command line parser
      
    returns:
      void
 *****************************************************************************/
static void Cmd_Boot(char *pArgs)
{
  SciAPuts(Cmd_NewLine);
  SciAPuts("Returning to Bootloader (if we had one)...");
  SciAPuts(Cmd_NewLine);
}


/*****************************************************************************
    FUNCTION    Cmd_MemRead()

    Description:
      display values of memory locations

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
 *****************************************************************************/
static void Cmd_MemRead(char *pArgs)
{
  Uint16 word_idx,line_idx;
  Uint32 addr;
  Uint16 *pAddr;

  addr = Cmd_UtilScanXtoL(&pArgs);                    // get address from command line
  addr &= 0xFFFFFFF8;                                 // mask off lsb's
  pAddr = (Uint16 *) addr;                            // convert to pointer
  SciAPuts(Cmd_NewLine);  

  for (line_idx = 0; line_idx <50; line_idx++)        // print a line of '*'
  {
    SciAPutChar('*');
  }
  SciAPuts(Cmd_NewLine);

  for (line_idx = 0; line_idx < 8; line_idx++)        // line counter
  {
    Cmd_UtilLtoX(Cmd_Buffer, (Uint32) pAddr);
    SciAPuts(Cmd_Buffer);                             // print address
    SciAPuts("  ");

    for (word_idx = 0; word_idx < 8; word_idx++)      // word counter
    {
      sprintf(Cmd_Buffer, "%04X ", *pAddr++);
      SciAPuts(Cmd_Buffer);                           // print word
    }

    SciAPuts(Cmd_NewLine);                            // next line  
  }

  SciAPuts(Cmd_NewLine);  
}


/******************************************************************************
    FUNCTION    Cmd_SinGet()

    Description:
      Get PWM sin amplitude in pu

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SinGet(char *pArgs)
{

  SciAPuts(Cmd_NewLine);
  sprintf(Cmd_Buffer, "InvCtrlSinAmpPu[0] = %g\r\n", InvCtrlSinAmpPu[0]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer, "InvCtrlSinAmpPu[1] = %g\r\n", InvCtrlSinAmpPu[1]);
  SciAPuts(Cmd_Buffer);
  SciAPuts(Cmd_NewLine);

}


/******************************************************************************
    FUNCTION    Cmd_SinSet()

    Description:
      Set PWM sin amplitude in pu

    Entrance Conditions:
      argument list has the following:
      Uint16  channel     PWM channel to change sin amplitude on 1/2/A
      float32 pu          value to set sin amplitude to 0.0..1.0

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SinSet(char *pArgs)
{
  Uint16 channel;
  float32 pu;

  sscanf(pArgs, "%X %g", &channel, &pu);

  SciAPuts(Cmd_NewLine);
  sprintf(Cmd_Buffer, "PWM channel = %X\r\n", channel);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer, "PWM sin pu  = %g\r\n", pu);
  SciAPuts(Cmd_Buffer);
  SciAPuts(Cmd_NewLine);

  if ((pu > 1.0f) || (pu < 0.0f))
  {
    SciAPuts("Sin pu out of range [0.0...1.0], abort...\r\n");
    return;
  }

  switch (channel)
  {
    case 0x0A:
      SciAPuts("SinAmpPu all requested\r\n");
      InvCtrlSinAmpPu[CHAN1] = pu;
      InvCtrlSinAmpPu[CHAN2] = pu;
      NvVars.Var.SetPointVout[CHAN1] = (pu * VPK_MAX) / SQRT2;
      NvVars.Var.SetPointVout[CHAN2] = (pu * VPK_MAX) / SQRT2;
      NV_UPDATE_SET();
      break;

    case 0x01:
      SciAPuts("SinAmpPu PWM1/2 requested\r\n");
      InvCtrlSinAmpPu[CHAN1] = pu;
      NvVars.Var.SetPointVout[CHAN1] = (pu * VPK_MAX) / SQRT2;
      NV_UPDATE_SET();
      break;

   case 0x02:
      SciAPuts("SinAmpPu PWM4/5 requested\r\n");
      InvCtrlSinAmpPu[CHAN2] = pu;
      NvVars.Var.SetPointVout[CHAN2] = (pu * VPK_MAX) / SQRT2;
      NV_UPDATE_SET();
      break;

    default:
      SciAPuts("Invalid request\r\n");
      break;
  }
}


/******************************************************************************
    FUNCTION    Cmd_NvRevert()

    Description:
      Revert all NvVars to defaults

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_NvRevert(char *pArgs)
{
  Uint16 indx;
  Uint16 chksum;
  Uint16 pagegood;
  
  SciAPuts(Cmd_NewLine);
  SciAPuts("Reverting all NvVars to Defaults...\r\n");

  pagegood = NvVars.Var.PageGood;
  chksum = 0;
  for (indx = 1; indx < NVMEM_COPY_SIZE; indx++)
  {
    NvVars.Copy[indx] = NvDefault.Copy[indx];         // copy default data from Flash
    chksum += NvVars.Copy[indx];                      // calculate checksum
  }
  NvVars.Var.PageGood = pagegood;                     // replace PageGood
  NvVars.Var.Checksum = chksum;                       // assign Checksum
  NV_UPDATE_SET();                                    // set flag to update Nv_Rom
  SciAPuts(Cmd_NewLine);
  
}


/******************************************************************************
    FUNCTION    Cmd_NvGet()

    Description:
      Get values of NvVars elements

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_NvGet(char *pArgs)
{
  char buf[9];

  sprintf(Cmd_Buffer, "CanAdr  = %02X\r\n", NvVars.Var.CanAddrB);
  SciAPuts(Cmd_Buffer);
  
  memcpy(buf,NvVars.Var.ModelNumber,8);               // copy var to buffer
  buf[8] = 0;                                         // null terminate buffer
  sprintf(Cmd_Buffer, "Model   = %s\r\n", buf);  // print var
  SciAPuts(Cmd_Buffer);

  memcpy(buf,NvVars.Var.SerialNumber1,8);             // copy var to buffer
  buf[8] = 0;                                         // null terminate buffer
  sprintf(Cmd_Buffer, "Serial1 = %s\r\n", buf); // print var
  SciAPuts(Cmd_Buffer);

  memcpy(buf,NvVars.Var.SerialNumber2,8);             // copy var to buffer
  buf[8] = 0;                                         // null terminate buffer
  sprintf(Cmd_Buffer, "Serial2 = %s\r\n", buf); // print var
  SciAPuts(Cmd_Buffer);

  memcpy(buf,NvVars.Var.SeriesNumber,8);              // copy var to buffer
  buf[8] = 0;                                         // null terminate buffer
  sprintf(Cmd_Buffer, "Series  = %s\r\n", buf); // print var
  SciAPuts(Cmd_Buffer);

  memcpy(buf,NvVars.Var.HwVersion,8);                 // copy var to buffer
  buf[8] = 0;                                         // null terminate buffer
  sprintf(Cmd_Buffer, "HwVer   = %s\r\n", buf); // print var
  SciAPuts(Cmd_Buffer);
  
}


/******************************************************************************
    FUNCTION    Cmd_NvSet()

    Description:
      Set NvVar and update

    Entrance Conditions:
      argument list has the following:
      char  name[]     name of variable to change
      char  val[]      char string value to set to

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_NvSet(char *pArgs)
{
  char name_buf [10], val_buf[10];
  Uint16 offset, val, idx;
  CAN_HANDLE_T *pCanHandle;
  IPS_MSG_ID_T type, addr, mask;

  for (idx = 0; idx < 10; idx++)                      // clear buffers
  {
    name_buf[idx] = 0;
    val_buf[idx]  = 0;
  }

  sscanf(pArgs, "%s %s", name_buf, val_buf);          // get parameters from command line

  SciAPuts(Cmd_NewLine);

  // find variable name's offset in descriptors list
  for (offset = 0; Cmd_NvSetVariableDescriptors[offset] != NULL; offset++)
  {
    if (strcmp(name_buf, Cmd_NvSetVariableDescriptors[offset]) == 0)
    {
      break;                                          // found it!
    }
  }

  if (Cmd_NvSetVariableDescriptors[offset] == NULL)
  {
    SciAPuts("\r\nBad Variable Name...\r\n");         // didn't find it
    return;
  }

  switch (offset)
  {
    case 0: //CanAdr
      sscanf(val_buf, "%x", &val);                    // convert string to Uint16
      NvVars.Var.CanAddrB = val;                      // update NvVar

      pCanHandle = &CanB;                             // write data to NodeIdx and update
      pCanHandle->NodeIdx = val;                      // copy new Node Address to handle

      type.MsgId = IPS_NONE_MASK;                     // start with msg cleared
      addr.MsgId = IPS_NONE_MASK;                     // start with msg cleared
      mask.MsgId = IPS_ALL_MASK;                      // start with allow all
                                                      // set up MBOX filters according to FMTB ID
      type.MsgIdB.rxNodeType = pCanHandle->NodeType;  // MSGID with rxNodeType set for type B format msg
      addr.MsgIdB.rxNodeAddr = pCanHandle->NodeIdx;   // MSGID with rxNodeAddr set for type B format msg
      mask.MsgIdB.rxNodeType = 0;                     // set mask to match Type for type B format msg
      mask.MsgIdB.rxNodeAddr = 0;                     // set mask to match Addr for type B format msg
      // reset CAN acceptance filters with new Node Address
      CanSetAccpMask(pCanHandle, type.MsgId, addr.MsgId, mask.MsgId);
      break;

    case 1: //Model
      for (idx = 0; idx < 8; idx++)                   // copy buffer to NvVar
      {
        NvVars.Var.ModelNumber[idx] = val_buf[idx];
      }
      break;

    case 2: //Serial1
      for (idx = 0; idx < 8; idx++)                   // copy buffer to NvVar
      {
        NvVars.Var.SerialNumber1[idx] = val_buf[idx];
      }
      break;

    case 3: //Serial2
      for (idx = 0; idx < 8; idx++)                   // copy buffer to NvVar
      {
        NvVars.Var.SerialNumber2[idx] = val_buf[idx];
      }
      break;

    case 4: //Series
      for (idx = 0; idx < 8; idx++)                   // copy buffer to NvVar
      {
        NvVars.Var.SeriesNumber[idx] = val_buf[idx];
      }
      break;

    case 5: //HwVer
      for (idx = 0; idx < 8; idx++)                   // copy buffer to NvVar
      {
        NvVars.Var.HwVersion[idx] = val_buf[idx];
      }
      break;

    default:
      break;
  }

  NV_UPDATE_SET();                                    // set flag to update Nv_Rom

}


/******************************************************************************
    FUNCTION    Cmd_SpGet()

    Description:
      Get values of NvVar Setpoint elements

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SpGet(char *pArgs)
{

  sprintf(Cmd_Buffer, "Vout1  = %9.6g\r\n", NvVars.Var.SetPointVout[0]);
  SciAPuts(Cmd_Buffer);

  sprintf(Cmd_Buffer, "Vout2  = %9.6g\r\n", NvVars.Var.SetPointVout[1]);
  SciAPuts(Cmd_Buffer);

  sprintf(Cmd_Buffer, "UVLO   = %9.6g\r\n", NvVars.Var.SetPointUVLO);
  SciAPuts(Cmd_Buffer);

  sprintf(Cmd_Buffer, "OVLO   = %9.6g\r\n", NvVars.Var.SetPointOVLO);
  SciAPuts(Cmd_Buffer);

  sprintf(Cmd_Buffer, "OVPK   = %9.6g\r\n", NvVars.Var.SetPointOVPK);
  SciAPuts(Cmd_Buffer);

  sprintf(Cmd_Buffer, "UV     = %9.6g\r\n", NvVars.Var.SetPointUV);
  SciAPuts(Cmd_Buffer);

  sprintf(Cmd_Buffer, "OV     = %9.6g\r\n", NvVars.Var.SetPointOV);
  SciAPuts(Cmd_Buffer);

  sprintf(Cmd_Buffer, "OC     = %9.6g\r\n", NvVars.Var.SetPointOC);
  SciAPuts(Cmd_Buffer);

}


/******************************************************************************
    FUNCTION    Cmd_SpSet()

    Description:
      Set NvVar Setpoints and update

    Entrance Conditions:
      argument list has the following:
      char    name[]     name of variable to change
      float32 val[]      value to set to

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SpSet(char *pArgs)
{
  char name_buf [10];
  float32 val;
  Uint16 offset;

  sscanf(pArgs, "%s %g", name_buf, &val);             // get parameters from command line

  SciAPuts(Cmd_NewLine);

  // find variable name's offset in descriptors list
  for (offset = 0; Cmd_SpSetVariableDescriptors[offset] != NULL; offset++)
  {
    if (strcmp(name_buf, Cmd_SpSetVariableDescriptors[offset]) == 0)
    {
      break;                                          // found it!
    }
  }

  if (Cmd_SpSetVariableDescriptors[offset] == NULL)
  {
    SciAPuts("\r\nBad Variable Name...\r\n");         // didn't find it
    return;
  }

  switch (offset)
  {
    case 0: //Vout1
      NvVars.Var.SetPointVout[0] = val;
      break;

    case 1: //Vout2
      NvVars.Var.SetPointVout[1] = val;
      break;

    case 2: //UVLO
      NvVars.Var.SetPointUVLO = val;
      break;

    case 3: //OVLO
      NvVars.Var.SetPointOVLO = val;
      break;

    case 4: //OVPK
      NvVars.Var.SetPointOVPK = val;
      break;

    case 5: //UV
      NvVars.Var.SetPointUV = val;
      break;

    case 6: //OV
      NvVars.Var.SetPointOV = val;
      break;

    case 7: //OC
      NvVars.Var.SetPointOC = val;
      break;

    default:
      break;
  }

  NV_UPDATE_SET();                                    // set flag to update Nv_Rom

}


/******************************************************************************
    FUNCTION    Cmd_MeasGet()

    Description:
      Get values of InvCtrlMeas structure

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_MeasGet(char *pArgs)
{
  SciAPuts(Cmd_NewLine);

  sprintf(Cmd_Buffer,"Vdc   = %9.6g\r\n", InvCtrlMeas.VdcLink);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Vout1 = %9.6g\r\n", InvCtrlMeas.Vout[CHAN1]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Vout2 = %9.6g\r\n", InvCtrlMeas.Vout[CHAN2]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Iout1 = %9.6g\r\n", InvCtrlMeas.Iout[CHAN1]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Iout2 = %9.6g\r\n", InvCtrlMeas.Iout[CHAN2]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Pout1 = %9.6g\r\n", InvCtrlMeas.Pout[CHAN1]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Pout2 = %9.6g\r\n", InvCtrlMeas.Pout[CHAN2]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Freq1 = %9.6g\r\n", InvCtrlMeas.Freq[CHAN1]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Freq2 = %9.6g\r\n", InvCtrlMeas.Freq[CHAN2]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Tstk1 = %9.6g\r\n", InvCtrlMeas.Tstk[CHAN1]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Tstk2 = %9.6g\r\n", InvCtrlMeas.Tstk[CHAN2]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Text1 = %9.6g\r\n", InvCtrlMeas.Text[CHAN1]);
  SciAPuts(Cmd_Buffer);
  sprintf(Cmd_Buffer,"Text2 = %9.6g\r\n", InvCtrlMeas.Text[CHAN2]);
  SciAPuts(Cmd_Buffer);
  SciAPuts(Cmd_NewLine);
}


/******************************************************************************
    FUNCTION    Cmd_MeasSet()

    Description:
      Set values of InvCtrlMeas structure

    Entrance Conditions:
      argument list has the following:
      char    name[]     name of variable to change
      float32 val[]      value to set to

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_MeasSet(char *pArgs)
{
  char name_buf [10];
  float32 val;
  Uint16 offset;

  sscanf(pArgs, "%s %g", name_buf, &val);             // get parameters from command line

  SciAPuts(Cmd_NewLine);

  // find variable name's offset in descriptors list
  for (offset = 0; Cmd_MeasSetVariableDescriptors[offset] != NULL; offset++)
  {
    if (strcmp(name_buf, Cmd_MeasSetVariableDescriptors[offset]) == 0)
    {
      break;                                          // found it!
    }
  }

  if (Cmd_MeasSetVariableDescriptors[offset] == NULL)
  {
    SciAPuts("\r\nBad Variable Name...\r\n");         // didn't find it
    return;
  }

  switch (offset)
  {
    case 0: //Vdc
      InvCtrlMeas.VdcLink = val;
      break;

    case 1: //Vout1
      InvCtrlMeas.Vout[CHAN1] = val;
      break;

    case 2: //Vout2
      InvCtrlMeas.Vout[CHAN2] = val;
      break;

    case 3: //Iout1
      InvCtrlMeas.Iout[CHAN1] = val;
      break;

    case 4: //Iout2
      InvCtrlMeas.Iout[CHAN2] = val;
      break;

    case 5: //Pout1
      InvCtrlMeas.Pout[CHAN1] = val;
      break;

    case 6: //Pout2
      InvCtrlMeas.Pout[CHAN2] = val;
      break;

    case 7: //Freq1
      InvCtrlMeas.Freq[CHAN1] = val;
      break;

    case 8: //Freq2
      InvCtrlMeas.Freq[CHAN2] = val;
      break;

    case 9: //Tstk1
      InvCtrlMeas.Tstk[CHAN1] = val;
      break;

    case 10: //Tstk2
      InvCtrlMeas.Tstk[CHAN2] = val;
      break;

    case 11: //Text1
      InvCtrlMeas.Text[CHAN1] = val;
      break;

    case 12: //Text2
      InvCtrlMeas.Text[CHAN2] = val;
      break;

    default:
      break;
  }

}


/******************************************************************************
    FUNCTION    Cmd_LedGet()

    Description:
      Get mode of Led

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_LedGet(char *pArgs)
{
  Uint16 mode;

  SciAPuts(Cmd_NewLine);
  mode = LedGetMode();
  sprintf(Cmd_Buffer,"Led mode = %s\r\n", Cmd_LedSetVariableDescriptors[mode]);
  SciAPuts(Cmd_Buffer);
  SciAPuts(Cmd_NewLine);
}


/******************************************************************************
    FUNCTION    Cmd_LedSet()

    Description:
      Set mode of Led

    Entrance Conditions:
      argument list has the following:
      char    name[]     name of mode to set to

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_LedSet(char *pArgs)
{
  char name_buf [10];
  Uint16 mode;

  sscanf(pArgs, "%s", name_buf);                      // get parameters from command line

  SciAPuts(Cmd_NewLine);

  // find variable name's offset in descriptors list
  for (mode = 0; Cmd_LedSetVariableDescriptors[mode] != NULL; mode++)
  {
    if (strcmp(name_buf, Cmd_LedSetVariableDescriptors[mode]) == 0)
    {
      break;                                          // found it!
    }
  }

  if (Cmd_LedSetVariableDescriptors[mode] == NULL)
  {
    SciAPuts("\r\nBad Mode...\r\n");                  // didn't find it
    return;
  }

  LedSetMode(mode);

}


/******************************************************************************
    FUNCTION    Cmd_SysGet()

    Description:
      print out SystemStatus[] bits
      This just prints out 1/0 without description since there are 8 bytes of bits

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SysGet(char *pArgs)
{
  Uint16 b;
  int16  bit;
  
  SciAPuts(Cmd_NewLine);
  SciAPuts("SystemStatus:\r\n");

  SciAPuts("bit # ->  7 6 5 4 3 2 1 0\r\n");
  SciAPuts("BYTE0 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (SystemStatus.byte.BYTE0 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE1 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (SystemStatus.byte.BYTE1 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE2 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (SystemStatus.byte.BYTE2 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE3 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (SystemStatus.byte.BYTE3 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE4 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (SystemStatus.byte.BYTE4 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE5 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (SystemStatus.byte.BYTE5 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE6 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (SystemStatus.byte.BYTE6 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE7 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (SystemStatus.byte.BYTE7 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts(Cmd_NewLine);

}


/******************************************************************************
    FUNCTION    Cmd_SysSet()

    Description:
      Set value of a SystemStatus flag

    Entrance Conditions:
      argument list has the following:
      Uint16  byte    BYTE of SystemStatus to modify [0..7]
      Uint16  bit     BIT in BYTE to modify [0..7]
      Uint16  val     VALUE to set BIT to [0,1]

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SysSet(char *pArgs)
{
  Uint16 byte, bit, val, temp;

  sscanf(pArgs,"%x %x %x",&byte, &bit, &val);
  SciAPuts(Cmd_NewLine);

  byte &= 0x07;
  bit  &= 0x07;
  val  &= 0x01;

  sprintf(Cmd_Buffer,"Setting SystemStatus BYTE%1d, BIT%1d to value %1d\r\n",byte, bit, val);
  SciAPuts(Cmd_Buffer);

  switch (byte)
  {
    case 0:
      temp = SystemStatus.byte.BYTE0;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      SystemStatus.byte.BYTE0 = temp;
      break;

    case 1:
      temp = SystemStatus.byte.BYTE1;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      SystemStatus.byte.BYTE1 = temp;
      break;

    case 2:
      temp = SystemStatus.byte.BYTE2;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      SystemStatus.byte.BYTE2 = temp;
      break;

    case 3:
      temp = SystemStatus.byte.BYTE3;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      SystemStatus.byte.BYTE3 = temp;
      break;

    case 4:
      temp = SystemStatus.byte.BYTE4;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      SystemStatus.byte.BYTE4 = temp;
      break;

    case 5:
      temp = SystemStatus.byte.BYTE5;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      SystemStatus.byte.BYTE5 = temp;
      break;

    case 6:
      temp = SystemStatus.byte.BYTE6;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      SystemStatus.byte.BYTE6 = temp;
      break;

    case 7:
      temp = SystemStatus.byte.BYTE7;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      SystemStatus.byte.BYTE7 = temp;
      break;

    default:
      break;
  }
}


/******************************************************************************
    FUNCTION    Cmd_SysClr()

    Description:
      Clear all SystemStatus[] bits to 0

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SysClr(char *pArgs)
{
  SystemStatus.all = 0x00;

  SciAPuts(Cmd_NewLine);
  SciAPuts("SystemStatus bits cleared...");
  SciAPuts(Cmd_NewLine);
}


/******************************************************************************
    FUNCTION    Cmd_FaultGet()

    Description:
      print out FaultStatus[] bits
      This just prints out 1/0 without description since there are 8 bytes of bits

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_FaultGet(char *pArgs)
{
  Uint16 b;
  int16  bit;

  SciAPuts(Cmd_NewLine);
  SciAPuts("FaultStatus:\r\n");

  SciAPuts("bit # ->  7 6 5 4 3 2 1 0\r\n");
  SciAPuts("BYTE0 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultStatus.byte.BYTE0 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE1 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultStatus.byte.BYTE1 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE2 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultStatus.byte.BYTE2 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE3 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultStatus.byte.BYTE3 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE4 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultStatus.byte.BYTE4 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE5 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultStatus.byte.BYTE5 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE6 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultStatus.byte.BYTE6 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE7 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultStatus.byte.BYTE7 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts(Cmd_NewLine);
}


/******************************************************************************
    FUNCTION    Cmd_FaultSet()

    Description:
      Set value of a Fault flag

    Entrance Conditions:
      argument list has the following:
      Uint16  byte    BYTE of Fault to modify [0..7]
      Uint16  bit     BIT in BYTE to modify [0..7]
      Uint16  val     VALUE to set BIT to [0,1]

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_FaultSet(char *pArgs)
{
  Uint16 byte, bit, val, temp;

  sscanf(pArgs,"%x %x %x",&byte, &bit, &val);
  SciAPuts(Cmd_NewLine);

  byte &= 0x07;
  bit  &= 0x07;
  val  &= 0x01;

  sprintf(Cmd_Buffer,"Setting FaultStatus BYTE%1d, BIT%1d to value %1d\r\n",byte, bit, val);
  SciAPuts(Cmd_Buffer);

  switch (byte)
  {
    case 0:
      temp = FaultStatus.byte.BYTE0;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultStatus.byte.BYTE0 = temp;
      break;

    case 1:
      temp = FaultStatus.byte.BYTE1;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultStatus.byte.BYTE1 = temp;
      break;

    case 2:
      temp = FaultStatus.byte.BYTE2;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultStatus.byte.BYTE2 = temp;
      break;

    case 3:
      temp = FaultStatus.byte.BYTE3;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultStatus.byte.BYTE3 = temp;
      break;

    case 4:
      temp = FaultStatus.byte.BYTE4;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultStatus.byte.BYTE4 = temp;
      break;

    case 5:
      temp = FaultStatus.byte.BYTE5;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultStatus.byte.BYTE5 = temp;
      break;

    case 6:
      temp = FaultStatus.byte.BYTE6;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultStatus.byte.BYTE6 = temp;
      break;

    case 7:
      temp = FaultStatus.byte.BYTE7;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultStatus.byte.BYTE7 = temp;
      break;

    default:
      break;
  }
}


/******************************************************************************
    FUNCTION    Cmd_FaultClr()

    Description:
      Clear all FaultStatus[] bits to 0

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_FaultClr(char *pArgs)
{
  FaultStatus.all = 0x00;

  SciAPuts(Cmd_NewLine);
  SciAPuts("FaultStatus bits cleared...");
  SciAPuts(Cmd_NewLine);
}


/******************************************************************************
    FUNCTION    Cmd_WarnGet()

    Description:
      print out FaultWarningStatus[] bits
      This just prints out 1/0 without description since there are 8 bytes of bits

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_WarnGet(char *pArgs)
{
  Uint16 b;
  int16  bit;

  SciAPuts(Cmd_NewLine);
  SciAPuts("WarningStatus:\r\n");

  SciAPuts("bit # ->  7 6 5 4 3 2 1 0\r\n");
  SciAPuts("BYTE0 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultWarningStatus.byte.BYTE0 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE1 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultWarningStatus.byte.BYTE1 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE2 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultWarningStatus.byte.BYTE2 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE3 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultWarningStatus.byte.BYTE3 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE4 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultWarningStatus.byte.BYTE4 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE5 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultWarningStatus.byte.BYTE5 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE6 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultWarningStatus.byte.BYTE6 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts("BYTE7 = {");
  for (bit = 7 ; bit >= 0 ; bit--)
  {
    b = (FaultWarningStatus.byte.BYTE7 & (1<<bit)) ? 1 : 0;
    sprintf(Cmd_Buffer, " %1d", b);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(" }\r\n");

  SciAPuts(Cmd_NewLine);
}


/******************************************************************************
    FUNCTION    Cmd_WarnSet()

    Description:
      Set value of a FaultWarningStatus flag

    Entrance Conditions:
      argument list has the following:
      Uint16  byte    BYTE of FaultWarningStatus to modify [0..7]
      Uint16  bit     BIT in BYTE to modify [0..7]
      Uint16  val     VALUE to set BIT to [0,1]

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_WarnSet(char *pArgs)
{
  Uint16 byte, bit, val, temp;

  sscanf(pArgs,"%x %x %x",&byte, &bit, &val);
  SciAPuts(Cmd_NewLine);

  byte &= 0x07;
  bit  &= 0x07;
  val  &= 0x01;

  sprintf(Cmd_Buffer,"Setting WarningStatus BYTE%1d, BIT%1d to value %1d\r\n",byte, bit, val);
  SciAPuts(Cmd_Buffer);

  switch (byte)
  {
    case 0:
      temp = FaultWarningStatus.byte.BYTE0;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultWarningStatus.byte.BYTE0 = temp;
      break;

    case 1:
      temp = FaultWarningStatus.byte.BYTE1;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultWarningStatus.byte.BYTE1 = temp;
      break;

    case 2:
      temp = FaultWarningStatus.byte.BYTE2;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultWarningStatus.byte.BYTE2 = temp;
      break;

    case 3:
      temp = FaultWarningStatus.byte.BYTE3;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultWarningStatus.byte.BYTE3 = temp;
      break;

    case 4:
      temp = FaultWarningStatus.byte.BYTE4;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultWarningStatus.byte.BYTE4 = temp;
      break;

    case 5:
      temp = FaultWarningStatus.byte.BYTE5;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultWarningStatus.byte.BYTE5 = temp;
      break;

    case 6:
      temp = FaultWarningStatus.byte.BYTE6;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultWarningStatus.byte.BYTE6 = temp;
      break;

    case 7:
      temp = FaultWarningStatus.byte.BYTE7;
      if (val)
      {
        BITSET(temp,bit);
      }
      else
      {
        BITCLR(temp,bit);
      }
      FaultWarningStatus.byte.BYTE7 = temp;
      break;

    default:
      break;
  }
}


/******************************************************************************
    FUNCTION    Cmd_WarnClr()

    Description:
      Clear all FaultWarningStatus[] bits to 0

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_WarnClr(char *pArgs)
{
  FaultWarningStatus.all = 0x00;

  SciAPuts(Cmd_NewLine);
  SciAPuts("WarningStatus bits cleared...");
  SciAPuts(Cmd_NewLine);
}


/******************************************************************************
    FUNCTION    Cmd_SimCmdGet()

    Description:
      print out IpsCanSimCmd bits

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SimCmdGet(char *pArgs)
{
  Uint16 bit, val;

  SciAPuts(Cmd_NewLine);
  SciAPuts("IpsCanSimCmd bits:\r\n");
  for (bit = 0; bit < 16; bit++)
  {
    val = (ISBITSET(IpsCanSimCmd.all, bit) ? 1 : 0);

    sprintf(Cmd_Buffer,"%-12s [%1d]\r\n", Cmd_SimCmdBitDescriptors[bit], val);
    SciAPuts(Cmd_Buffer);
  }
  SciAPuts(Cmd_NewLine);

}


/******************************************************************************
    FUNCTION    Cmd_SemCmdSet()

    Description:
      Set value of a IpsCanSimCmd flag

    Entrance Conditions:
      argument list has the following:
      Uint16  bit     BIT in BYTE to modify [0..7]
      Uint16  val     VALUE to set BIT to [0,1]

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SimCmdSet(char *pArgs)
{
  Uint16 val, bit;
  char cmd[14];

  sscanf(pArgs,"%s %x",&cmd[0], &val);
  SciAPuts(Cmd_NewLine);

  val  &= 0x01;                                       // mask to 1 bit [0/1]

  // find bit number
  for (bit = 0; Cmd_SimCmdBitDescriptors[bit] != NULL; bit++)
  {
    if (strcmp(cmd, Cmd_SimCmdBitDescriptors[bit]) == 0)
    {
      break;                                          // found it!
    }
  }

  if (Cmd_SimCmdBitDescriptors[bit] == NULL)
  {
    SciAPuts("\r\nBad Command Name...\r\n");          // didn't find it
    return;
  }

  sprintf(Cmd_Buffer,"Setting IpsCanSimCmd.%s to value %1d\r\n",Cmd_SimCmdBitDescriptors[bit], val);
  SciAPuts(Cmd_Buffer);

  if (val)
  {
    BITSET(IpsCanSimCmd.all,bit);
  }
  else
  {
    BITCLR(IpsCanSimCmd.all,bit);
  }

}


/******************************************************************************
    FUNCTION    Cmd_SimCmdClr()

    Description:
      Clear all IpsCanSimCmd bits to 0

    Entrance Conditions:

    Exit Conditions:

    args:
      char *pArgs     pointer to argument list from command line parser

    returns:
      void
*******************************************************************************/
static void Cmd_SimCmdClr(char *pArgs)
{
  IpsCanSimCmd.all = 0x0000;

  SciAPuts(Cmd_NewLine);
  SciAPuts("IpsCanSimCmd bits cleared...");
  SciAPuts(Cmd_NewLine);
}


/*****************************************************************************
    FUNCTION    Cmd_UtilScanXtoL()

    Description:
      Scan ASCII hex characters in a string and convert them to long integer.
      Move pointer 1 position past end of hex characters in the source string
      Used for parsing a hex number off an argument list
      You have to use this for long ints because sscanf %X only works for ints, not longs

    Entrance Conditions:
      ps points to pointer to string that contains ASCII hex characters

    Exit Conditions:
      ps positioned in the string 1 past the hex characters that were converted

    args:
      char **ps     pointer to pointer to string
    returns:
      Uint32 val    value of number converted from string
 *****************************************************************************/
static Uint32 Cmd_UtilScanXtoL(char **ps)
{
  char c;
  Uint32 val;

  val = 0;
  do
  {
    c = *((*ps)++);
  } while (c == ' ');                                 // ignore beginning spaces
  for(;;)
  {
    if ((c >= '0') && (c <= '9'))
    {
      val = (val << 4) + (c - '0');
    }
    else if ((c >= 'A') && (c <= 'F'))
    {
      val = (val << 4) + (c - 'A') + 10;
    }
    else if ((c >= 'a') && (c <= 'f'))
    {
      val = (val << 4) + (c - 'a') + 10;
    }
    else
    {
      if (c == '\0')                                  // don't move past end of string
        (*ps)--;
      return (val);
    }
    c = *((*ps)++);
  }
}


/*****************************************************************************
    FUNCTION    Cmd_UtilLtoX()

    Description:
      Converts Uint32 to a 8 digit hex character string.
      You have to use this for long ints because sprintf %X only works for ints, not longs

    Entrance Conditions:

    Exit Conditions:

    args:
      char   *ps    buffer must have enough space for string
      Uint32 val    value to be converted to a hex string
    returns:
      char *      pointer to null terminator of converted string
 *****************************************************************************/
static char *Cmd_UtilLtoX(char *ps, Uint32 val)
{
  const char hexdigit[] = "0123456789ABCDEF";

  *(ps++) = hexdigit[(val >> 28) & 0x0F];
  *(ps++) = hexdigit[(val >> 24) & 0x0F];
  *(ps++) = hexdigit[(val >> 20) & 0x0F];
  *(ps++) = hexdigit[(val >> 16) & 0x0F];
  *(ps++) = hexdigit[(val >> 12) & 0x0F];
  *(ps++) = hexdigit[(val >>  8) & 0x0F];
  *(ps++) = hexdigit[(val >>  4) & 0x0F];
  *(ps++) = hexdigit[val & 0x0F];
  *ps     = '\0';                                     // NULL terminate string
  return ps;
}
 

/*******************************************************************************
  End of file
*******************************************************************************/
