/******************************************************************************
  FILENAME: Common_CpuTimers.h

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_CPU_TIMERS_H
#define COMMON_CPU_TIMERS_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP2833x Header files


//****************************************
// structure definitions
//****************************************


//****************************************
// constant definitions
//****************************************
#define CPUTIMER_SYSCLKOUT        150L                 // SYSCLOCK in MHz

//****************************************
// global variable definitions 
//****************************************


//****************************************
// function macro definitions
//****************************************
// Start Timer:
#define CpuTimer0Start()        CpuTimer0Regs.TCR.bit.TSS = 0
#define CpuTimer1Start()        CpuTimer1Regs.TCR.bit.TSS = 0
#define CpuTimer2Start()        CpuTimer2Regs.TCR.bit.TSS = 0
// Stop Timer:
#define CpuTimer0Stop()         CpuTimer0Regs.TCR.bit.TSS = 1
#define CpuTimer1Stop()         CpuTimer1Regs.TCR.bit.TSS = 1
#define CpuTimer2Stop()         CpuTimer2Regs.TCR.bit.TSS = 1
// Reload Timer With period Value:
#define CpuTimer0Reload()       CpuTimer0Regs.TCR.bit.TRB = 1
#define CpuTimer1Reload()       CpuTimer1Regs.TCR.bit.TRB = 1
#define CpuTimer2Reload()       CpuTimer2Regs.TCR.bit.TRB = 1
// Read 32-Bit Timer Value:
#define CpuTimer0ReadCounter()  CpuTimer0Regs.TIM.all
#define CpuTimer1ReadCounter()  CpuTimer1Regs.TIM.all
#define CpuTimer2ReadCounter()  CpuTimer2Regs.TIM.all
// Read 32-Bit Period Value:
#define CpuTimer0ReadPeriod()   CpuTimer0Regs.PRD.all
#define CpuTimer1ReadPeriod()   CpuTimer1Regs.PRD.all
#define CpuTimer2ReadPeriod()   CpuTimer2Regs.PRD.all


//****************************************
// function prototypes 
//****************************************
void CpuTimer0Init(PINT TimerIsr, Uint32 Period);
void CpuTimer1Init(PINT TimerIsr, Uint32 Period);
void CpuTimer2Init(PINT TimerIsr, Uint32 Period);


interrupt void CpuTimer0Isr(void);
interrupt void CpuTimer1Isr(void);
interrupt void CpuTimer2Isr(void);



#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  End of file
*******************************************************************************/
