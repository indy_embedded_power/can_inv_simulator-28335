/******************************************************************************
  FILENAME: App_System.h

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     3/4/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/
#ifndef APP_SYSTEM_H        
#define APP_SYSTEM_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP2833x Header files


//****************************************
//  structure definitions
//****************************************
typedef struct SYSTEM_STATUS_BITS_S
{
  Uint16 CB             : 1;                          // BYTE0
  Uint16 Reserved01     : 1;
  Uint16 Reserved02     : 1;
  Uint16 Reserved03     : 1;
  Uint16 Reserved04     : 1;
  Uint16 Reserved05     : 1;
  Uint16 Reserved06     : 1;
  Uint16 Test           : 1;

  Uint16 Reserved10     : 1;                          // BYTE1
  Uint16 Reserved11     : 1;
  Uint16 Reserved12     : 1;
  Uint16 Reserved13     : 1;
  Uint16 Reserved14     : 1;
  Uint16 Reserved15     : 1;
  Uint16 Reserved16     : 1;
  Uint16 Reserved17     : 1;

  Uint16 Phase1Op       : 1;                          // BYTE2
  Uint16 Phase1En       : 1;
  Uint16 Reserved22     : 1;
  Uint16 Reserved23     : 1;
  Uint16 Reserved24     : 1;
  Uint16 Reserved25     : 1;
  Uint16 Reserved26     : 1;
  Uint16 Reserved27     : 1;

  Uint16 Reserved30     : 1;                          // BYTE3
  Uint16 Reserved31     : 1;
  Uint16 Reserved32     : 1;
  Uint16 Reserved33     : 1;
  Uint16 Reserved34     : 1;
  Uint16 Reserved35     : 1;
  Uint16 Reserved36     : 1;
  Uint16 Reserved37     : 1;

  Uint16 Phase2Op       : 1;                          // BYTE4
  Uint16 Phase2En       : 1;
  Uint16 Reserved42     : 1;
  Uint16 Reserved43     : 1;
  Uint16 Reserved44     : 1;
  Uint16 Reserved45     : 1;
  Uint16 Reserved46     : 1;
  Uint16 Reserved47     : 1;

  Uint16 Reserved50     : 1;                          // BYTE5
  Uint16 Reserved51     : 1;
  Uint16 Reserved52     : 1;
  Uint16 Reserved53     : 1;
  Uint16 Reserved54     : 1;
  Uint16 Reserved55     : 1;
  Uint16 Reserved56     : 1;
  Uint16 Reserved57     : 1;

  Uint16 Reserved60     : 1;                          // BYTE6
  Uint16 Reserved61     : 1;
  Uint16 Reserved62     : 1;
  Uint16 Reserved63     : 1;
  Uint16 Reserved64     : 1;
  Uint16 Reserved65     : 1;
  Uint16 Reserved66     : 1;
  Uint16 Reserved67     : 1;

  Uint16 Reserved70     : 1;                          // BYTE7
  Uint16 Reserved71     : 1;
  Uint16 Reserved72     : 1;
  Uint16 Reserved73     : 1;
  Uint16 Reserved74     : 1;
  Uint16 Reserved75     : 1;
  Uint16 Reserved76     : 1;
  Uint16 Reserved77     : 1;
} SYSTEM_STATUS_BITS_T;

typedef union SYSTEM_STATUS_U
{
  Uint64 all;
  SYSTEM_STATUS_BITS_T bit;
  BYTE8_T byte;
} SYSTEM_STATUS_T;


//****************************************
//  constant definitions
//****************************************


//****************************************
//  global variable definitions 
//****************************************
extern SYSTEM_STATUS_T SystemStatus;


//****************************************
//  function macro definitions
//****************************************


//****************************************
//  function prototypes 
//****************************************
void SystemInit(void);
void SystemHandler(void);


#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
