/******************************************************************************
  FILENAME: App_main.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "DSP2833x_MemCopy.h"                         // prototype for MemCopy
#include "Flash2833x_API_Library.h"                   // prototypes for TI Flash API
#include "Common_Device.h"                            // DSP2833x Header files
#include "Common_CodeStart.h"                         // prototypes for asm functions and startup stuff
#include "Common_SysCtrl.h"                           // prototypes for SysCtrl
#include "Common_PieCtrl.h"                           // prototypes for PieCtrl
#include "Common_Gpio.h"                              // prototypes for GPIO
#include "Common_AppTimers.h"                         // prototypes for Application Timers
#include "Common_CmdLine.h"                           // prototypes for command line interface
#include "Common_NvVars.h"                            // prototypes for NvVars
#include "Common_Led.h"                               // Prototypes for LED
#include "Common_IpsCan.h"                            // prototypes for IPS CAN
#include "App_CmdMenu.h"                              // prototype for CmdMenuTable[]
#include "App_InvCtrl.h"                              // prototypes for InvCtrl
#include "App_Faults.h"                               // prototypes for Fault handler
#include "App_System.h"                               // prototypes for System Handler
#include "App_IpsCanDevB.h"                           // prototypes for IPS CAN tables

//****************************************
//  global variables
//****************************************


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
//  local function prototypes
//****************************************





/******************************************************************************
    FUNCTION    main()

    Description:
      C program starts here

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void main(void)
{

  //--- Disable global interrupts
  DINT;                                               // Disable global interrupts
  DRTM;                                               // Disable real time debug

  //--- CPU Initialization
  SysCtrlInit();                                      // Initialize the CPU
  GpioInit();                                         // Initialize the GPIO pins to default state (input, PU enabled)
  PieCtrlInit();                                      // Initialize the PIE control registers to their default state
//x don't enable watchdog during development debug. when re-enabling it here, also have to in NvVarsHandler()
  SysCtrlWatchdogInit();                              // Initialize the Watchdog

  //--- Copy all Flash sections that need to run from RAM
  MemCopy(&RamfuncsLoadStart, &RamfuncsLoadEnd, &RamfuncsRunStart);
  MemCopy(&Flash28_API_LoadStart, &Flash28_API_LoadEnd, &Flash28_API_RunStart);
  MemCopy(&rtsLibLoadStart, &rtsLibLoadEnd, &rtsLibRunStart);
  SysCtrlFlashInit();                                 // Initialize the Flash and OTP ROM

  //--- Peripheral Initialization
  AppTmrInit();                                       // Initialize Application timers (and CpuTimer0) [MUST BE FIRST]
  NvVarsInit();                                       // Initialize NV config data from Flash
  SciAInit(115200);                                   // Initialize Console RS-232 port for 115200 BAUD

  //--- Enable global interrupts
  EINT;                                               // Enable Global interrupt INTM
  ERTM;                                               // Enable Global realtime interrupt DBGM

  //--- Initialize system components
  SystemInit();                                       // Initialize system status
  GpioAppInit();                                      // Initialize inverter specific GPIO
  CmdLineInit();                                      // Initialize Console Command Line Interface
  InvCtrlInit();                                      // Initialize inverter control
  LedInit();                                          // Initialize LED driver
  FaultInit();                                        // Initialize Fault handler
  CanHandleInit();                                    // initialize CAN handles first before IpsCanInit
  IpsCanInit(&CanB, IPS_ID_FMTA, IPS_CAN_DEVB_TYPE, NvVars.Var.CanAddrB); // initialize eCANB for IPS CANA protocol
  IpsCanSimCmd.all = 0x0000;                          // initialize CAN simulator cmd variable

  //--- main loop
  for (;;)
  {
    SysCtrlServiceDog();
    AppTmrHandler();
    NvVarsHandler();
    CmdLineHandler(CmdMenuTable);
//x    LedHandler();
//x    SystemHandler();
//x    FaultHandler();
    IpsCanHandler(&CanB, IpsCanDevBTable);
  }

}


/*******************************************************************************
  End of file
*******************************************************************************/
