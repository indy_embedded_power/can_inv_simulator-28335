/******************************************************************************
  FILENAME: Common_Sci.h

 *****************************************************************************
  Copyright 2016 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/30/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_SCI_H
#define COMMON_SCI_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP280xx Header files

//****************************************
// constant definitions
//****************************************


//****************************************
// structure definitions
//****************************************


//****************************************
// global variable definitions
//****************************************


//****************************************
// function macro definitions
//****************************************


//****************************************
// function prototypes
//****************************************
extern void SciAInit(Uint32 Baud);
extern Uint16 SciAKbHit(void);
extern void SciAFlush(void);
extern int16 SciAGetChar(void);
extern void SciAPutChar(const char c);
extern void putch(char c);
extern void SciAPuts(const char *ps);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  End of file
*******************************************************************************/
