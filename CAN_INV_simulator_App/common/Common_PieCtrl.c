/******************************************************************************
  FILENAME: Common_PieCtrl.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_PieCtrl.h"                           // function prototype definitions


//****************************************
//  global variables
//****************************************


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
// local function prototypes
//****************************************
interrupt void PieCtrlDummy_ISR(void);



/*****************************************************************************
    FUNCTION    PieCtrlInit()

    Description:
      Initializes the PIE vector table and enables the PIE interrupts
      
    Entrance Conditions:
      global interrupts are disabled
                  
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
void PieCtrlInit(void)
{
  int16   i;
  Uint32 *pSource        = (void *) &PieCtrlDummy_ISR;// source is address of dummy ISR
  volatile Uint32 *pDest = (void *) &PieVectTable;    // destination is the start of the PIE vector table

  PieCtrlRegs.PIECTRL.bit.ENPIE = 0;                  // Disable the PIE

  EALLOW;
  for(i=0; i < 128; i++)
  {
    *pDest++ = *pSource;                              // copy dummy interrupt to each vector in the table
  }
  EDIS;
  
  PieCtrlRegs.PIEIER1.all   = 0x0000;                 // Disable all PIE interrupts
  PieCtrlRegs.PIEIER2.all   = 0x0000;
  PieCtrlRegs.PIEIER3.all   = 0x0000;
  PieCtrlRegs.PIEIER4.all   = 0x0000;
  PieCtrlRegs.PIEIER5.all   = 0x0000;
  PieCtrlRegs.PIEIER6.all   = 0x0000;
  PieCtrlRegs.PIEIER7.all   = 0x0000;
  PieCtrlRegs.PIEIER8.all   = 0x0000;
  PieCtrlRegs.PIEIER9.all   = 0x0000;
  PieCtrlRegs.PIEIER10.all  = 0x0000;
  PieCtrlRegs.PIEIER11.all  = 0x0000;
  PieCtrlRegs.PIEIER12.all  = 0x0000;

  PieCtrlRegs.PIEIFR1.all   = 0x0000;                 // Clear any potentially pending PIEIFR flags
  PieCtrlRegs.PIEIFR2.all   = 0x0000;
  PieCtrlRegs.PIEIFR3.all   = 0x0000;
  PieCtrlRegs.PIEIFR4.all   = 0x0000;
  PieCtrlRegs.PIEIFR5.all   = 0x0000;
  PieCtrlRegs.PIEIFR6.all   = 0x0000;
  PieCtrlRegs.PIEIFR7.all   = 0x0000;
  PieCtrlRegs.PIEIFR8.all   = 0x0000;
  PieCtrlRegs.PIEIFR9.all   = 0x0000;
  PieCtrlRegs.PIEIFR10.all  = 0x0000;
  PieCtrlRegs.PIEIFR11.all  = 0x0000;
  PieCtrlRegs.PIEIFR12.all  = 0x0000;

  PieCtrlRegs.PIEACK.all    = 0xFFFF;                 // Acknowlege all PIE interrupt groups

  PieCtrlRegs.PIECTRL.bit.ENPIE = 1;                  // Enable the PIE

  IER = 0x0000;                                       // Disable CPU interrupts
  IFR = 0x0000;                                       // Clear all CPU interrupt flags
}


/*****************************************************************************
    FUNCTION    PieCtrlDummy_ISR()

    Description:
      This ISR is for reserved PIE vectors.  It is used to initialize the table.  
      It should never be reached by properly executing code.
      If you get here, it means something is wrong.
      
    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
interrupt void PieCtrlDummy_ISR(void)
{
  ESTOP0;                                             // Emulator Halt instruction
  while(1);                                           // wait here for watchdog reset
}



/*****************************************************************************
End of file
******************************************************************************/
