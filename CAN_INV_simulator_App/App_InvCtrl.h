/******************************************************************************
  FILENAME: App_InvCtrl.h

 ******************************************************************************
  Copyright 2016 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     Aug 15, 2014
  Author:         Drew, S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/
#ifndef APP_INVCTRL_H
#define APP_INVCTRL_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                                      // DSP2833x Header files
#include "App_Main.h"                                           // CHANNEL defines

//****************************************
//  structure definitions
//****************************************
typedef struct INVCTRL_STATUS_BITS_S
{
  Uint16 Fault      : 1;
  Uint16 OutEn      : 1;
  Uint16 OutTest    : 1;
  Uint16 OutSin     : 1;
  Uint16 ITest      : 1;
  Uint16 VTest      : 1;
  Uint16 IlimDis    : 1;
  Uint16 TzFault    : 1;
  Uint16 OvPkFault  : 1;
  Uint16 CmpReset   : 1;
  Uint16 Rsvd10     : 1;
  Uint16 Rsvd11     : 1;
  Uint16 Rsvd12     : 1;
  Uint16 Rsvd13     : 1;
  Uint16 Rsvd14     : 1;
  Uint16 Ilim_q     : 1;
} INVCTRL_STATUS_BITS_T;

typedef union INVCTRL_STATUS_U
{
  Uint16                all;
  INVCTRL_STATUS_BITS_T bit;
} INVCTRL_STATUS_T;

typedef struct INVCTRL_MEAS_S
{
  float32 VdcLink;
  float32 Vout[NUM_CHANNELS];
  float32 Iout[NUM_CHANNELS];
  float32 Pout[NUM_CHANNELS];
  float32 Freq[NUM_CHANNELS];
  float32 Iswitch[NUM_CHANNELS];
  float32 Tstk[NUM_CHANNELS];
  float32 Text[NUM_CHANNELS];
} INVCTRL_MEAS_T;


//****************************************
//  constant definitions
//****************************************
#define INVCTRL_GRID_FREQ                 (60.0f)

#define INVCTRL_NUM_COMP                  6
#define INVCTRL_OFFSET_COMP_I1            0
#define INVCTRL_OFFSET_COMP_IL1           1
#define INVCTRL_OFFSET_COMP_V1            2
#define INVCTRL_OFFSET_COMP_I2            3
#define INVCTRL_OFFSET_COMP_IL2           4
#define INVCTRL_OFFSET_COMP_V2            5


//****************************************
//  global variable definitions
//****************************************
extern volatile INVCTRL_MEAS_T InvCtrlMeas;
extern volatile float32 InvCtrlSinAmpPu[NUM_CHANNELS];
extern volatile INVCTRL_STATUS_T InvCtrlStatus[NUM_CHANNELS];


//****************************************
//  function macro definitions
//****************************************


//****************************************
//  function prototypes
//****************************************
void InvCtrlInit(void);
void InvCtrlPwmOn(void);
void InvCtrlPwmOff(void);



#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
