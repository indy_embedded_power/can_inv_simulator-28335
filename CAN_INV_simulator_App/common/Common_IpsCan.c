/******************************************************************************
  FILENAME: Common_IpsCan.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     5/7/2017
  Author:         S,Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include <stdio.h>                                    // define sscanf() & sprintf()
#include "Common_IpsCan.h"                            // defines for IpsCan
#include "Common_Version.h"                           // defines for Version info
#include "Common_NvVars.h"                            // defines for NvVars
#include "App_Faults.h"                               // prototypes for FaultMasterHeartbeatTimerHandle
#include "App_System.h"                               // prototypes for SystemStatus

//****************************************
//  global variables
//****************************************
IPS_VERSTR_T IpsVerStr;                               // Ips Version Strings for module


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
//  local function prototypes
//****************************************
static void IpsCan_ProcessMsg(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg, const IPS_TABLE_T *pParam);
static Uint16 IpsCan_CreateMsgTI(CAN_HANDLE_T *pCanHandle, Uint32 canMsgId);
static void IpsCan_ReInit(CAN_HANDLE_T *pCanHandle);




/******************************************************************************
    FUNCTION    IpsCanInit()

    Description:
      initialize eCAN peripheral for IPS CAN protocol

    Entrance Conditions:
      CAN handle structures initialized using CanHandleInit()

    Exit Conditions:
      eCAN peripheral initialized for given IPS format

    args:
      CAN_HANDLE_T    *pCanHandle - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      IPS_MSG_ID_FMT_E format     - IPS msg ID format specifier (FMT_A/FMT_B)
      Uint16           NodeType   - Device IPS node type
      Uint16           NodeAddr   - Device IPD node address

    returns:
      void
*******************************************************************************/
void IpsCanInit(CAN_HANDLE_T *pCanHandle, IPS_MSG_ID_FMT_E format, Uint16 NodeType, Uint16 NodeAddr)
{
  Uint16 checksum, indx;
  char bootVersion[9] = "xx.xx.xx";
  char appVersion[9]  = "xx.xx.xx";
  IPS_MSG_ID_T type, addr, mask;
  
  type.MsgId = IPS_NONE_MASK;                         // start with msg cleared
  addr.MsgId = IPS_NONE_MASK;                         // start with msg cleared
  mask.MsgId = IPS_ALL_MASK;                          // start with allow all
  if (format == IPS_ID_FMTA)                          // if FMTA, set up MBOX filters according to FMTA ID
  {
    type.MsgIdA.rxNodeType = NodeType;                // MSGID with rxNodeType set for type A format msg
    addr.MsgIdA.rxNodeAddr = NodeAddr;                // MSGID with rxNodeAddr set for type A format msg
    mask.MsgIdA.rxNodeType = 0;                       // set mask to match Type for type A format msg
    mask.MsgIdA.rxNodeAddr = 0;                       // set mask to match Addr for type A format msg
  }
  else  if (format == IPS_ID_FMTB)                    // if FMTB, set up MBOX filters according to FMTB ID
  {
    type.MsgIdB.rxNodeType = NodeType;                // MSGID with rxNodeType set for type B format msg
    addr.MsgIdB.rxNodeAddr = NodeAddr;                // MSGID with rxNodeAddr set for type B format msg
    mask.MsgIdB.rxNodeType = 0;                       // set mask to match Type for type B format msg
    mask.MsgIdB.rxNodeAddr = 0;                       // set mask to match Addr for type B format msg
  }
  
  pCanHandle->NodeType = NodeType;                    // save node info into handle
  pCanHandle->NodeIdx  = NodeAddr;
  pCanHandle->NodeFmt  = format;

  pCanHandle->ResetTries = 0;                         // setup for CAN reset if hung up
  pCanHandle->ResetTimerHandle = AppTmrStart(IPS_RESET_TIMEOUT);

  CanDriverInit(pCanHandle, type.MsgId, addr.MsgId, mask.MsgId);
  
  // initialize FW version strings
  checksum = VersionCheck(&BootVersion);
  if(checksum != 0xFFFF)
  {
    sprintf(bootVersion,"%2d.%2d.%1c",BootVersion.Major,BootVersion.Minor,BootVersion.DbgRev);
  }  
  checksum = VersionCheck(&AppVersion);
  if(checksum != 0xFFFF)
  {
    sprintf(appVersion, "%2d.%2d.%1c",AppVersion.Major,AppVersion.Minor,AppVersion.DbgRev);
  }
  for (indx = 0; indx < 8 ; indx++)
  {
    IpsVerStr.BootFwVer[indx] = bootVersion[indx];
    IpsVerStr.AppFwVer[indx]  = appVersion[indx];
  }
  
}


/******************************************************************************
    FUNCTION    IpsCanHandler()

    Description:
      handle processing of IPS messages

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T    *pCanHandle - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      IPS_TABLE_T     *pDevTable  - pointer to IPS device parameter table

    returns:
      void
*******************************************************************************/
void IpsCanHandler(CAN_HANDLE_T *pCanHandle, const IPS_TABLE_T *pDevTable)
{
  const IPS_TABLE_T *pTbl;
  struct MBOX msg;
  Uint16 msgTI;

  // check the Receive Queue for pending messages
  if (CanRxMessage(pCanHandle, &msg))
  {
    // reset master heartbeat timer whenever we get a msg
    AppTmrRestart(FaultMasterHeartbeatTimerHandle,FAULT_HEARTBEAT_TIME);

    // reset CAN stuck timer whenever we get a message
    AppTmrRestart(pCanHandle->ResetTimerHandle, IPS_RESET_TIMEOUT);
    pCanHandle->ResetTries = 0;

    // create msgTI key from Type/Index info
    msgTI = IpsCan_CreateMsgTI(pCanHandle, msg.MSGID.all);

    // find msgTI in table - if not in table pointer is NULL
    pTbl = pDevTable;
    for (;;)
    {
      if (pTbl->msgTI == msgTI)
      {
        break;                                        // found it, pTbl points to parameter
      }
      if (pTbl->msgTI == NULL)
      {
        pTbl = NULL;
        break;                                        // reached the end, not in table
      }
      pTbl++;
    }

    // process request if found in table
    if (pTbl)
    {
      // check data for proper size, don't process if size is wrong
      if ((msg.MSGCTRL.bit.DLC == 0)||(msg.MSGCTRL.bit.DLC == pTbl->size))
      {
        if (pTbl->flag & IPS_FUNC)                    // check if parameter requires special handling
        {
          pTbl->pIPS.pF(pCanHandle,&msg);             // parameter handling thorugh special function
        }
        else
        {
          IpsCan_ProcessMsg(pCanHandle, &msg, pTbl);  // parameter handling through std function
        }
      }
    }
  }

  // Check for stuck CAN peripheral
  if (AppTmrChkStatus(pCanHandle->ResetTimerHandle) == APPTMR_SUSPENDED)  // if we havent had a message for a long time
  {
    if (pCanHandle->ResetTries < IPS_RESET_MAX_TRIES)                     // if we haven't exceeded our retry limit
    {
      AppTmrRestart(pCanHandle->ResetTimerHandle, IPS_RESET_TIMEOUT);     // reset the timer
      pCanHandle->ResetTries++;                                           // increment retry counter
      IpsCan_ReInit(pCanHandle);                                          // reset the CAN peripheral
    }
  }

}


/******************************************************************************
    FUNCTION    IpsCanBuildTxMsgId()

    Description:
      Create CAN message ID, to transmit, using IPS message format.
      Swaps the txNode information from pMsg into the rxNode infor and adds txNode info 
      from pCanHandle Node info

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T  *pCanHandle - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      IPS_MSG_ID_T  *pMsg       - pointer to source msg with node type and addr to send to in txNodeType/txNodeAddr

    returns:
      IPS_MSG_ID_T msgId          - formatted CAN message ID
*******************************************************************************/
IPS_MSG_ID_T IpsCanBuildTxMsgId(CAN_HANDLE_T *pCanHandle, IPS_MSG_ID_T *pMsg)
{
  IPS_MSG_ID_T msgId;
  
  msgId.MsgId = CAN_IDE_MASK;
  if (pCanHandle->NodeFmt == IPS_ID_FMTA)             // set up TX msg using CANID FMTA
  {
    msgId.MsgIdA.msgType    = pMsg->MsgIdA.msgType;
    msgId.MsgIdA.msgIndex   = pMsg->MsgIdA.msgIndex;
    msgId.MsgIdA.rxNodeType = pMsg->MsgIdA.txNodeType;
    msgId.MsgIdA.rxNodeAddr = pMsg->MsgIdA.txNodeAddr;
    msgId.MsgIdA.txNodeType = pCanHandle->NodeType;
    msgId.MsgIdA.txNodeAddr = pCanHandle->NodeIdx;
  }
  else  // NodeFmt == IPS_ID_FMTB                     // set up TX msg using CANID FMTB
  {
    msgId.MsgIdB.msgType    = pMsg->MsgIdB.msgType;
    msgId.MsgIdB.msgIndex   = pMsg->MsgIdB.msgIndex;
    msgId.MsgIdB.rxNodeType = pMsg->MsgIdB.txNodeType;
    msgId.MsgIdB.rxNodeAddr = pMsg->MsgIdB.txNodeAddr;
    msgId.MsgIdB.txNodeType = pCanHandle->NodeType;
    msgId.MsgIdB.txNodeAddr = pCanHandle->NodeIdx;
  }
  return msgId;
}


/******************************************************************************
    FUNCTION    IpsCan_ProcessMsg()

    Description:
      handle standard processing of IPS messages

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T    *pCanHandle - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX     *pMsg       - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all
      IPS_TABLE_T     *pParam     - pointer to IPS parameter table entry

    returns:
      void
*******************************************************************************/
static void IpsCan_ProcessMsg(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg, const IPS_TABLE_T *pParam)
{
  struct MBOX msg;
  IPS_MSG_ID_T txMsgId,rxMsgId;
  char byte[8] = {0,0,0,0,0,0,0,0};
  Uint16 indx;
  IPS_DATA_FLOAT32_T tempFloat32;
  IPS_DATA_UINT32_T  tempUint32;
  LLBYTE8_T          tempUint64;

  rxMsgId.MsgId = pMsg->MSGID.all;

  switch (pParam->type)
  {
    case IPS_BYTE:
    case IPS_STRING:  
      if (pMsg->MSGCTRL.bit.DLC == 0)
      {
        // Get Data
        txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID
        for (indx = 0; indx < pParam->size; indx++)         // copy data from variable to temp array
        {
          byte[indx] = pParam->pIPS.pB[indx];
        }
        
        msg.MSGID.all       = txMsgId.MsgId;                // load up the tx message mailbox
        msg.MSGCTRL.bit.DLC = pParam->size;
        msg.MDL.byte.BYTE0  = byte[0];
        msg.MDL.byte.BYTE1  = byte[1];
        msg.MDL.byte.BYTE2  = byte[2];
        msg.MDL.byte.BYTE3  = byte[3];
        msg.MDH.byte.BYTE4  = byte[4];
        msg.MDH.byte.BYTE5  = byte[5];
        msg.MDH.byte.BYTE6  = byte[6];
        msg.MDH.byte.BYTE7  = byte[7];

        CanTxMessage(pCanHandle, &msg);                     // transmit the response
      }
      else
      {
        // Set Data
        if ((pParam->flag & IPS_RO) == 0)                   // don't update if data is read only
        {
          byte[0] = pMsg->MDL.byte.BYTE0;
          byte[1] = pMsg->MDL.byte.BYTE1;
          byte[2] = pMsg->MDL.byte.BYTE2;
          byte[3] = pMsg->MDL.byte.BYTE3;
          byte[4] = pMsg->MDH.byte.BYTE4;
          byte[5] = pMsg->MDH.byte.BYTE5;
          byte[6] = pMsg->MDH.byte.BYTE6;
          byte[7] = pMsg->MDH.byte.BYTE7;

          for (indx = 0; indx < pParam->size; indx++)       // copy data from temp array to variable
          {
            pParam->pIPS.pB[indx] = byte[indx];
          }

          if (pParam->flag & IPS_NV)
          {
            NV_UPDATE_SET();                                // schedule NvVars flash update
          }
        }
      }
      break;
    
    case IPS_WORD16:
      if (pMsg->MSGCTRL.bit.DLC == 0)
      {
        // Get Data
        txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID

        msg.MSGID.all         = txMsgId.MsgId;              // load up the tx message mailbox
        msg.MSGCTRL.bit.DLC   = pParam->size;
        msg.MDL.word.HI_WORD  = *pParam->pIPS.pW;           // put word in bytes 0,1
        msg.MDL.word.LOW_WORD = 0x0000;
        msg.MDH.word.HI_WORD  = 0x0000;
        msg.MDH.word.LOW_WORD = 0x0000;
 
        CanTxMessage(pCanHandle, &msg);                     // transmit the response
      }
      else
      {
        // Set Data
        if ((pParam->flag & IPS_RO) == 0)                   // don't update if data is read only
        {
          *pParam->pIPS.pW = pMsg->MDL.word.HI_WORD;

          if (pParam->flag & IPS_NV)
          {
            NV_UPDATE_SET();                                // schedule NvVars flash update
          }
        }
      }
      break;
      
    case IPS_WORD32:
      if (pMsg->MSGCTRL.bit.DLC == 0)
      {
        // Get Data
        txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID

        tempUint32.all = *pParam->pIPS.pL;                  // get 32-bit parameter into union
        
        msg.MSGID.all         = txMsgId.MsgId;              // load up the message mailbox
        msg.MSGCTRL.bit.DLC   = pParam->size;
        msg.MDL.word.HI_WORD  = tempUint32.word[1];
        msg.MDL.word.LOW_WORD = tempUint32.word[0];
        msg.MDH.word.HI_WORD  = 0x0000;
        msg.MDH.word.LOW_WORD = 0x0000;

        CanTxMessage(pCanHandle, &msg);                     // transmit the response
      }
      else
      {
        // Set Data
        if ((pParam->flag & IPS_RO) == 0)                   // don't update if data is read only
        {
          tempUint32.word[1] = pMsg->MDL.word.HI_WORD;
          tempUint32.word[0] = pMsg->MDL.word.LOW_WORD;
          *pParam->pIPS.pL   = tempUint32.all;

          if (pParam->flag & IPS_NV)
          {
            NV_UPDATE_SET();                                // schedule NvVars flash update
          }
        }
      }
      break;
      
    case IPS_WORD64:
      if (pMsg->MSGCTRL.bit.DLC == 0)
      {
        // Get Data
        txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID

        tempUint64.all = *pParam->pIPS.pLL;                 // get 64-bit parameter into union

        msg.MSGID.all       = txMsgId.MsgId;                // load up the tx message mailbox
        msg.MSGCTRL.bit.DLC = pParam->size;
        msg.MDL.byte.BYTE0  = tempUint64.byte.BYTE0;
        msg.MDL.byte.BYTE1  = tempUint64.byte.BYTE1;
        msg.MDL.byte.BYTE2  = tempUint64.byte.BYTE2;
        msg.MDL.byte.BYTE3  = tempUint64.byte.BYTE3;
        msg.MDH.byte.BYTE4  = tempUint64.byte.BYTE4;
        msg.MDH.byte.BYTE5  = tempUint64.byte.BYTE5;
        msg.MDH.byte.BYTE6  = tempUint64.byte.BYTE6;
        msg.MDH.byte.BYTE7  = tempUint64.byte.BYTE7;

        CanTxMessage(pCanHandle, &msg);                     // transmit the response
      }
      else
      {
        // Set Data
        if ((pParam->flag & IPS_RO) == 0)                   // don't update if data is read only
        {
          tempUint64.byte.BYTE0 = pMsg->MDL.byte.BYTE0;
          tempUint64.byte.BYTE1 = pMsg->MDL.byte.BYTE1;
          tempUint64.byte.BYTE2 = pMsg->MDL.byte.BYTE2;
          tempUint64.byte.BYTE3 = pMsg->MDL.byte.BYTE3;
          tempUint64.byte.BYTE4 = pMsg->MDH.byte.BYTE4;
          tempUint64.byte.BYTE5 = pMsg->MDH.byte.BYTE5;
          tempUint64.byte.BYTE6 = pMsg->MDH.byte.BYTE6;
          tempUint64.byte.BYTE7 = pMsg->MDH.byte.BYTE7;
          *pParam->pIPS.pLL     = tempUint64.all;           // copy data from temp union to variable

          if (pParam->flag & IPS_NV)
          {
            NV_UPDATE_SET();                                // schedule NvVars flash update
          }
        }
      }
      break;

    case IPS_FLOAT32:
      if (pMsg->MSGCTRL.bit.DLC == 0)
      {
        // Get Data
        txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID

        tempFloat32.all = *pParam->pIPS.pFl;                // get 32-bit float parameter into union

        msg.MSGID.all         = txMsgId.MsgId;              // load up the message mailbox
        msg.MSGCTRL.bit.DLC   = pParam->size;
        msg.MDL.word.HI_WORD  = tempFloat32.word[1];
        msg.MDL.word.LOW_WORD = tempFloat32.word[0];
        msg.MDH.word.HI_WORD  = 0x0000;
        msg.MDH.word.LOW_WORD = 0x0000;

        CanTxMessage(pCanHandle, &msg);                     // transmit the response
      }
      else
      {
        // Set Data
        if ((pParam->flag & IPS_RO) == 0)                   // don't update if data is read only
        {
          tempFloat32.word[1] = pMsg->MDL.word.HI_WORD;
          tempFloat32.word[0] = pMsg->MDL.word.LOW_WORD;
          *pParam->pIPS.pFl   = tempFloat32.all;

          if (pParam->flag & IPS_NV)
          {
            NV_UPDATE_SET();                                // schedule NvVars flash update
          }
        }
      }
      break;
    
    default:
      break;
  }
}


/******************************************************************************
    FUNCTION    IpsCan_CreateMsgTI()

    Description:
      Create msgTI from IPS 32-bit CAN identifier

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T    *pCanHandle - pointer to handle for CAN peripheral used (&CanA/&CanB)
      Uint32          canMsgId    - 29-bit CAN identifier

    returns:
      Uint16 msgTI      combined msgType and msgIndex into 16-bit number
*******************************************************************************/
static Uint16 IpsCan_CreateMsgTI(CAN_HANDLE_T *pCanHandle, Uint32 canMsgId)
{
  IPS_MSG_ID_T msgId;
  Uint16 msgTI;

  // get MSGID into format union
  msgId.MsgId = canMsgId;

  // create msgTI key from Type/Index info
  if (pCanHandle->NodeFmt == IPS_ID_FMTA)             // have to check NodeFmt to figure out which MSGID format to use
  {
    msgTI = msgId.MsgIdA.msgType;
    msgTI <<= 8;                                      // Type shifted to MSB
    msgTI += msgId.MsgIdA.msgIndex;                   // Index is LSB
  }
  else  // NodeFmt == IPS_ID_FMTB
  {
    msgTI = msgId.MsgIdB.msgType;
    msgTI <<= 8;                                      // Type shifted to MSB
    msgTI += msgId.MsgIdB.msgIndex;                   // Index is LSB
  }

  return msgTI;
}


/******************************************************************************
    FUNCTION    IpsCan_ReInit()

    Description:
      Re-initialize eCAN peripheral for IPS CAN protocol

    Entrance Conditions:
      CAN handle structures initialized using CanHandleInit()

    Exit Conditions:
      eCAN peripheral re-initialized for given IPS format

    args:
      CAN_HANDLE_T    *pCanHandle - pointer to handle for CAN peripheral used (&CanA/&CanB)

    returns:
      void
*******************************************************************************/
static void IpsCan_ReInit(CAN_HANDLE_T *pCanHandle)
{
  IPS_MSG_ID_T type, addr, mask;

  type.MsgId = IPS_NONE_MASK;                         // start with msg cleared
  addr.MsgId = IPS_NONE_MASK;                         // start with msg cleared
  mask.MsgId = IPS_ALL_MASK;                          // start with allow all
  if (pCanHandle->NodeFmt == IPS_ID_FMTA)             // if FMTA, set up MBOX filters according to FMTA ID
  {
    type.MsgIdA.rxNodeType = pCanHandle->NodeType;    // MSGID with rxNodeType set for type A format msg
    addr.MsgIdA.rxNodeAddr = pCanHandle->NodeIdx;     // MSGID with rxNodeAddr set for type A format msg
    mask.MsgIdA.rxNodeType = 0;                       // set mask to match Type for type A format msg
    mask.MsgIdA.rxNodeAddr = 0;                       // set mask to match Addr for type A format msg
  }
  else  if (pCanHandle->NodeFmt == IPS_ID_FMTB)       // if FMTB, set up MBOX filters according to FMTB ID
  {
    type.MsgIdB.rxNodeType = pCanHandle->NodeType;    // MSGID with rxNodeType set for type B format msg
    addr.MsgIdB.rxNodeAddr = pCanHandle->NodeIdx;     // MSGID with rxNodeAddr set for type B format msg
    mask.MsgIdB.rxNodeType = 0;                       // set mask to match Type for type B format msg
    mask.MsgIdB.rxNodeAddr = 0;                       // set mask to match Addr for type B format msg
  }

  pCanHandle->pECanRegs->CANMC.all = 0x00000020;      // set software reset bit in master control register
  CanDriverInit(pCanHandle, type.MsgId, addr.MsgId, mask.MsgId);// re-initialize CAN peripheral

}



/*******************************************************************************
  End of file
*******************************************************************************/
