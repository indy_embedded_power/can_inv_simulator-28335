/******************************************************************************
  FILENAME: Common_CanDriver.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     4/28/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_CanDriver.h"                         // prototypes for CAN driver

//****************************************
//  global variables
//****************************************
// Only allocate variables if peripheral is selected as active in .h
#if ECANA
CAN_HANDLE_T CanA;
#endif
#if ECANB
CAN_HANDLE_T CanB;
#endif

//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************
#define CANGIF_MIV_MSK       0x0000001FUL
#define CANGIF_GMIF_MSK      (1UL << 15)
#define CANGIF_BOIF_MSK      (1UL << 10)
#define CANGIF_EPIF_MSK      (1UL <<  9)
#define CANGIF_WLIF_MSK      (1UL <<  8)
#define CANGIF_BO_EP_WL_MSK  (CANGIF_BOIF_MSK | CANGIF_EPIF_MSK | CANGIF_WLIF_MSK)

#define CANES_EW_MSK         (1UL << 16)


//****************************************
//  local variables
//****************************************


//****************************************
//  local function prototypes
//****************************************
#if ECANA
interrupt void CanA_RcvISR(void);
#endif
#if ECANB
interrupt void CanB_RcvISR(void);
#endif
inline void Can_RcvISR(CAN_HANDLE_T *pCanHandle);


/******************************************************************************
    FUNCTION    CanHandleInit()

    Description:
      Initialization of CAN handles for all CAN peripherals

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
*******************************************************************************/
void CanHandleInit(void)
{
#if ECANA
  // initialize CanA handle pointers
  CanA.pECanRegs      = &ECanaRegs;
  CanA.pECanMboxes    = &ECanaMboxes;
  CanA.pECanLAMRegs   = &ECanaLAMRegs;
  canA.pECanInterrupt = &CanA_RcvISR;

  // setup CanA handle receive queue
  for (CanA.RxNdx = 0; CanA.RxNdx < CAN_RX_QUEUE_SIZE; ++CanA.RxNdx)
  {
    CanA.RxQueue[CanA.RxNdx].MSGCTRL.all  = 0;        // rsvd1=FALSE and DLC=0
    CanA.RxQueue[CanA.RxNdx].MSGID.all    = 0;
    CanA.RxQueue[CanA.RxNdx].MDL.all      = 0;
    CanA.RxQueue[CanA.RxNdx].MDH.all      = 0;
  }
  CanA.RxOdx  = 0;                                    // start the out-dex at zero
  CanA.RxNdx  = 0;                                    // start the in-dex at zero
  CanA.BrpReg = CAN_BRPREG_A;                         // Baud Rate Prescale
#endif
#if ECANB
  // initialize CanB handle pointers
  CanB.pECanRegs      = &ECanbRegs;
  CanB.pECanMboxes    = &ECanbMboxes;
  CanB.pECanLAMRegs   = &ECanbLAMRegs;
  CanB.pECanInterrupt = &CanB_RcvISR;

  // setup CanB handle receive queue
  for (CanB.RxNdx = 0; CanB.RxNdx < CAN_RX_QUEUE_SIZE; ++CanB.RxNdx)
  {
    CanB.RxQueue[CanB.RxNdx].MSGCTRL.all  = 0;        // rsvd1=FALSE and DLC=0
    CanB.RxQueue[CanB.RxNdx].MSGID.all    = 0;
    CanB.RxQueue[CanB.RxNdx].MDL.all      = 0;
    CanB.RxQueue[CanB.RxNdx].MDH.all      = 0;
  }
  CanB.RxOdx  = 0;                                    // start the out-dex at zero
  CanB.RxNdx  = 0;                                    // start the in-dex at zero
  CanB.BrpReg = CAN_BRPREG_B;                         // Baud Rate Prescale
#endif
}


/******************************************************************************
    FUNCTION    CanDriverInit()

    Description:
      Initialization of CAN driver parameters

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      CAN_HANDLE_T *pCanHandle   - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      Uint32 rcvNodeType         - this device Can RXtype Id based on IPC Can msgId
      Uint32 rcvNodeIdx          - this device Can RXidx  Id based on IPC Can msgId
      Uint32 rcvMask             - acceptance mask to setup
      
    returns:
      void
*******************************************************************************/
void CanDriverInit(CAN_HANDLE_T *pCanHandle, Uint32 rcvNodeType, Uint32 rcvNodeIdx, Uint32 rcvMask)
{
  struct ECAN_REGS ECanShadow;                        // shadow registers to guarantee 32bit access

  EALLOW;
  //--- Configure IO & clock
  if (pCanHandle->pECanRegs == &ECanaRegs)            // if initializing ECANA
  {
    SysCtrlRegs.PCLKCR0.bit.ECANAENCLK = 1;           // enable clock to ECANA module
    GpioCtrlRegs.GPAPUD.bit.GPIO30     = 0;           // Enable pull-up for GPIO30 (CANRXA)
    GpioCtrlRegs.GPAPUD.bit.GPIO31     = 0;           // Enable pull-up for GPIO31 (CANTXA)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO30   = 3;           // Asynch qual for GPIO30 (CANRXA)
    GpioCtrlRegs.GPAMUX2.bit.GPIO30    = 1;           // GPIO30 set to CANRXA
    GpioCtrlRegs.GPAMUX2.bit.GPIO31    = 1;           // GPIO31 set to CANTXA
  }
  else if (pCanHandle->pECanRegs == &ECanbRegs)       // if initializing ECANB
  {
    SysCtrlRegs.PCLKCR0.bit.ECANBENCLK = 1;           // enable clock to ECANB module
    GpioCtrlRegs.GPAPUD.bit.GPIO17     = 0;           // Enable pull-up for GPIO17 (CANRXB)
    GpioCtrlRegs.GPAPUD.bit.GPIO16     = 0;           // Enable pull-up for GPIO16 (CANTXB)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO17   = 3;           // Asynch qual for GPIO17 (CANRXB)
    GpioCtrlRegs.GPAMUX2.bit.GPIO17    = 2;           // GPIO17 set to CANRXB
    GpioCtrlRegs.GPAMUX2.bit.GPIO16    = 2;           // GPIO16 set to CANTXB
  }

  //--- Configure eCAN RX and TX pins for CAN operation using eCAN shadow regs
  ECanShadow.CANTIOC.all = pCanHandle->pECanRegs->CANTIOC.all;
  ECanShadow.CANTIOC.bit.TXFUNC = 1;                  // IO pin used for CANTX
  pCanHandle->pECanRegs->CANTIOC.all = ECanShadow.CANTIOC.all;

  ECanShadow.CANRIOC.all = pCanHandle->pECanRegs->CANRIOC.all;
  ECanShadow.CANRIOC.bit.RXFUNC = 1;                  // IO pin used for CANRX
  pCanHandle->pECanRegs->CANRIOC.all = ECanShadow.CANRIOC.all;

  //--- Clear MSGCTRL registers for proper operation as some bits come up in an unknown state
  pCanHandle->pECanMboxes->MBOX0.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX1.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX2.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX3.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX4.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX5.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX6.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX7.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX8.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX9.MSGCTRL.all  = 0x00000000;
  pCanHandle->pECanMboxes->MBOX10.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX11.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX12.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX13.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX14.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX15.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX16.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX17.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX18.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX19.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX20.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX21.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX22.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX23.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX24.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX25.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX26.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX27.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX28.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX29.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX30.MSGCTRL.all = 0x00000000;
  pCanHandle->pECanMboxes->MBOX31.MSGCTRL.all = 0x00000000;

  //--- TAn, RMPn, GIFn bits are all zero upon reset and are cleared here as a matter of precaution
  pCanHandle->pECanRegs->CANTA.all   = 0xFFFFFFFF;    // Clear all TAn bits
  pCanHandle->pECanRegs->CANRMP.all  = 0xFFFFFFFF;    // Clear all RMPn bits
  pCanHandle->pECanRegs->CANGIF0.all = 0xFFFFFFFF;    // Clear all interrupt flag bits
  pCanHandle->pECanRegs->CANGIF1.all = 0xFFFFFFFF;

  //--- Enter initialization mode to configure bit timing parameters for eCANx
  ECanShadow.CANMC.all     = pCanHandle->pECanRegs->CANMC.all;
  ECanShadow.CANMC.bit.SCB = 1;                       // Enable eCan enhanced mode
  ECanShadow.CANMC.bit.DBO = 0;                       // Most significant byte first (MDL = B0,B1,B2,B3  MDH = B4,B5,B6,B7)
  ECanShadow.CANMC.bit.CCR = 1;                       // Set Change Configuration Request
  pCanHandle->pECanRegs->CANMC.all = ECanShadow.CANMC.all;

  //--- Wait until the CPU has been granted permission to change the configuration registers
  do
  {
    ECanShadow.CANES.all = pCanHandle->pECanRegs->CANES.all;
  } while (ECanShadow.CANES.bit.CCE != 1);            // Wait for set Change Configuration Enable bit

  //--- CAN Module clock (Time Quanta) : 15 TQ per bit
  ECanShadow.CANBTC.all          = 0;
  ECanShadow.CANBTC.bit.BRPREG   = pCanHandle->BrpReg;// BAUD rate prescaler
  ECanShadow.CANBTC.bit.TSEG1REG = CAN_TSEG1;
  ECanShadow.CANBTC.bit.TSEG2REG = CAN_TSEG2;
  ECanShadow.CANBTC.bit.SAM      = 1;                 // Sample 3 times
  ECanShadow.CANBTC.bit.SJWREG   = 1;                 // sync jump width 2TQ
  pCanHandle->pECanRegs->CANBTC.all = ECanShadow.CANBTC.all;

  ECanShadow.CANMC.all     = pCanHandle->pECanRegs->CANMC.all;
  ECanShadow.CANMC.bit.CCR = 0;                       // Clear Change Configuration Request
  pCanHandle->pECanRegs->CANMC.all = ECanShadow.CANMC.all;

  //--- Wait until the CPU no longer has permission to change the configuration registers
  do
  {
    ECanShadow.CANES.all = pCanHandle->pECanRegs->CANES.all;
  } while (ECanShadow.CANES.bit.CCE != 0);            // Wait for cleared Change Configuration Enable bit

  //--- Setup the Global interrupt mask, and associate eCanx interrupt functions to PIE and enable INT
  pCanHandle->pECanRegs->CANGIM.all = (1UL << 10) +   // BOI - Bus off
                                      (1UL << 9)  +   // EPI - Error passive 
                                      (1UL << 8)  +   // WLI - Warning 
                                      (0UL << 2)  +   // GIL mapped to ECAN0INT
                                      (0UL << 1)  +   // ECAN1INT [I1EN] = disabled [Xmt ISR]
                                      (1UL << 0);     // ECAN0INT [I0EN] = enabled [Err & Rcv ISR]

  pCanHandle->pECanRegs->CANMIM.all = 0x00000000UL;   // but, disable mailbox interrupts

  //--- Associate eCanx interrupt functions to PIE and enable INT
  if (pCanHandle->pECanRegs == &ECanaRegs)
  {
    PieVectTable.ECAN0INTA        = pCanHandle->pECanInterrupt;
    PieCtrlRegs.PIEIER9.bit.INTx5 = 1;                // Enable  INT9.5 CanA Interrupt 0 [I0EN] (Rx & Err Int)
    PieCtrlRegs.PIEIER9.bit.INTx6 = 0;                // Disable INT9.6 CanA Interrupt 1 [I1EN] (Tx Int)
  }
  else if (pCanHandle->pECanRegs == &ECanbRegs)
  {
    PieVectTable.ECAN0INTB        = pCanHandle->pECanInterrupt;
    PieCtrlRegs.PIEIER9.bit.INTx7 = 1;                // Enable  INT9.7 CanB Interrupt 0 [I0EN] (Rx & Err Int)
    PieCtrlRegs.PIEIER9.bit.INTx8 = 0;                // Disable INT9.8 CanB Interrupt 1 [I1EN] (Tx Int)
  }

  //--- Enable interrupt for the eCAN module (both modules use same PIE block)
  PieCtrlRegs.PIECTRL.bit.ENPIE = 1;                  // make sure PIE block is enabled
  PieCtrlRegs.PIEACK.bit.ACK9   = 1;                  // Acknowledge Group9 PIE (clear interrupt)
  IER |= M_INT9;                                      // Enable CPU Int9 (includes both eCan peripherals)

  pCanHandle->pECanRegs->CANES.all = 0x01BF0000UL;    // Clear FE,BE,CRCE,SE,ACKE,BO,EP,EW error status bits
  EDIS;

  CanSetAccpMask(pCanHandle, rcvNodeType, rcvNodeIdx, rcvMask);
}


/******************************************************************************
    FUNCTION    CanSetAccpMask()

    Description:
      Setup CAN Acceptance Mask Filter Configuration for receive mailboxes

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      Uint32 rcvNodeType          - this device Can RXtype Id based on IPC Can msgId
      Uint32 rcvNodeIdx           - this device Can RXidx  Id based on IPC Can msgId
      Uint32 rcvMask              - acceptance mask to setup
      
    returns:
      void
*******************************************************************************/
void CanSetAccpMask(CAN_HANDLE_T *pCanHandle, Uint32 rcvNodeType, Uint32 rcvNodeIdx, Uint32 rcvMask)
{
  Uint32 nodeMask;

  pCanHandle->pECanRegs->CANME.all  = 0x00000000UL;   // Disable all Mailboxes

  // Configure Mailboxes Direction: 31-24 as Tx(0), 23-0 as Rx(1)
  pCanHandle->pECanRegs->CANMD.all  = 0x00FFFFFFUL;

  // Setup MBOX 0 as special RECEIVE mailboxes with broadcast command address
  pCanHandle->pECanMboxes->MBOX0.MSGID.all  = CAN_IDE_MASK | CAN_AME_MASK;

  // Setup the MSGID field of RECEIVE mailboxes MBOX 1-23
  // cccs ssss ssss ssee eeee eeee eeee eeee [c=ctrl-bits, s=std-addr, e=ext-addr]
  nodeMask = CAN_IDE_MASK | CAN_AME_MASK | rcvNodeType | rcvNodeIdx;
  pCanHandle->pECanMboxes->MBOX1.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX2.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX3.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX4.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX5.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX6.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX7.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX8.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX9.MSGID.all  = nodeMask;
  pCanHandle->pECanMboxes->MBOX10.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX11.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX12.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX13.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX14.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX15.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX16.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX17.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX18.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX19.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX20.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX21.MSGID.all = nodeMask;
  pCanHandle->pECanMboxes->MBOX22.MSGID.all = nodeMask;

  // setup special mailbox for group broadcast command address
  nodeMask = CAN_IDE_MASK | CAN_AME_MASK | rcvNodeType;
  pCanHandle->pECanMboxes->MBOX23.MSGID.all = nodeMask;

  // setup to filter on EXT message -- LAMI=0
  pCanHandle->pECanLAMRegs->LAM0.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM1.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM2.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM3.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM4.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM5.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM6.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM7.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM8.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM9.all  = rcvMask;
  pCanHandle->pECanLAMRegs->LAM10.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM11.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM12.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM13.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM14.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM15.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM16.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM17.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM18.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM19.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM20.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM21.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM22.all = rcvMask;
  pCanHandle->pECanLAMRegs->LAM23.all = rcvMask;

  
  pCanHandle->pECanRegs->CANOPC.all   = 0x00FFFFFFUL;              // setup to queue up to 24 payloads (Overflow Protection Control)

  pCanHandle->pECanRegs->CANMIL.all   = 0xFF000000UL;              // Mailbox Interrupt Level (map 1=Tx; 0=Rx)
  EALLOW;
  pCanHandle->pECanRegs->CANMIM.all   = 0x00FFFFFFUL;              // Enable RX interrupts on mailbox 23-0
  EDIS;

  pCanHandle->pECanRegs->CANTA.all    = 0xFFFFFFFFUL;              // clear all TX Ack
  pCanHandle->pECanRegs->CANRMP.all   = 0xFFFFFFFFUL;              // clear all Receive Msg Pending (RMP) flags
  pCanHandle->pECanRegs->CANGIF0.all  = 0xFFFFFFFFUL;              // clear all Global INT0 flags 
  pCanHandle->pECanRegs->CANGIF1.all  = 0xFFFFFFFFUL;              // clear all Global INT1 flags

  pCanHandle->pECanRegs->CANME.all    = 0xFFFFFFFFUL;              // Enable all mailboxes
}


/******************************************************************************
    FUNCTION    CanTxMessage()

    Description:
      Schedule a Can packet to be sent via CAN bus

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to message being sent out
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all
    returns:
      Uint16 return - TRUE  -> message sent successfully
                      FALSE -> CAN bus is busy and message could not be sent

*******************************************************************************/
Uint16 CanTxMessage(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  volatile struct MBOX *pMailbox;
  union CANMSGCTRL_REG msgCtrl;
  Uint16 sendRequested;
  Uint32 mask, mbox;

  sendRequested = FALSE;
  mask = 0x80000000L;
  pMailbox = &pCanHandle->pECanMboxes->MBOX31;
  for (mbox = 31; mbox > 23; --mbox)
  {
    if ((pCanHandle->pECanRegs->CANTRS.all & mask) == 0)  // look for an open transmit mailbox (31-23)
    {
      pCanHandle->pECanRegs->CANME.all &= ~mask;          // disable selected mailbox (using 32-bit write)
      pMailbox->MSGID.all = pMsg->MSGID.all;
      msgCtrl.all = pMailbox->MSGCTRL.all;
      msgCtrl.bit.DLC = pMsg->MSGCTRL.bit.DLC;
      pMailbox->MSGCTRL.all = msgCtrl.all;
      pCanHandle->pECanRegs->CANME.all = 0xFFFFFFFF;      // enable all mailboxes (using 32-bit write)

      pMailbox->MDL.all = pMsg->MDL.all;
      pMailbox->MDH.all = pMsg->MDH.all;
      pCanHandle->pECanRegs->CANTRS.all = mask;           // transmit request set
      sendRequested = TRUE;
      break;
    }

    mask >>= 1;                                           // move to next mailbox
    --pMailbox;
  }

  return sendRequested;
}


/******************************************************************************
    FUNCTION    CanA_RcvISR()

    Description:
        The CAN Interrupt routine for ECANA: processes receive buffers

    Entrance Conditions:
            
    Exit Conditions:
            
    args:
      void
      
    returns:
      void
*******************************************************************************/
#if ECANA
interrupt void CanA_RcvISR(void)
{
  EINT;                                               // re-enable interrupts for higher priority
  Can_RcvISR(&CanA);
  PieCtrlRegs.PIEACK.bit.ACK9 = 1;                    // Acknowledge group9 (eCANx) interrupt to PIE
}
#endif


/******************************************************************************
    FUNCTION    CanB_RcvISR()

    Description:
        The CAN Interrupt routine for ECANB: processes receive buffers

    Entrance Conditions:
            
    Exit Conditions:
            
    args:
      void
      
    returns:
      void
*******************************************************************************/
#if ECANB
interrupt void CanB_RcvISR(void)
{
  EINT;                                               // re-enable interrupts for higher priority
  Can_RcvISR(&CanB);
  PieCtrlRegs.PIEACK.bit.ACK9 = 1;                    // Acknowledge group9 (eCANx) interrupt to PIE
}
#endif


/******************************************************************************
    FUNCTION    Can_RcvISR()

    Description:
        The CAN Interrupt routine: processes receive buffers

    Entrance Conditions:
            
    Exit Conditions:
            
    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      
    returns:
      void
*******************************************************************************/
#pragma FUNC_ALWAYS_INLINE (Can_RcvISR)
inline void Can_RcvISR(CAN_HANDLE_T *pCanHandle)
{
  volatile struct MBOX *pMailbox;
  Uint32  regShadow, mask, mbox;

  regShadow = pCanHandle->pECanRegs->CANGIF0.all;                 // get copy of CANGIF0: Global Interrupt Flag 0
  if (regShadow & CANGIF_BO_EP_WL_MSK)                            // Process Error interrupt:
  {                                                               //   warning level, error passive or bus off interrupt
    regShadow = pCanHandle->pECanRegs->CANES.all;                 // get copy of CANES register
    pCanHandle->pECanRegs->CANES.all   = regShadow & 0xFFFF0000UL;// Clear the flags in CANES
    pCanHandle->pECanRegs->CANGIF0.all = CANGIF_BO_EP_WL_MSK;     // Clear BOIF,EPIF,WLIF flags
  }
  else if (regShadow & CANGIF_GMIF_MSK)                           // process rx mailbox interrupt
  {
    mbox = regShadow & CANGIF_MIV_MSK;                            // setup mailbox based on interrupt vector
    mask = 1UL << mbox;

    if ((pCanHandle->pECanRegs->CANRMP.all & mask) == 0)          // if mbox int vector not pending, search
    {
      for (mbox = 23; mbox; --mbox)                               // check receive mailboxs for pending
      {
        mask = 1UL << mbox;
        if (pCanHandle->pECanRegs->CANRMP.all & mask)
          break;                                                  // have new mbox & mask
      }
    }
    pMailbox = &pCanHandle->pECanMboxes->MBOX0 + mbox;            // setup pointer based on mailbox index

    // make sure we have space for the new payload
    if (pCanHandle->RxQueue[pCanHandle->RxNdx].MSGCTRL.bit.rsvd1 && pCanHandle->RxNdx == pCanHandle->RxOdx)
    {
      if (++pCanHandle->RxOdx >= CAN_RX_QUEUE_SIZE)
      {
        pCanHandle->RxOdx = 0;
      }
    }
    pCanHandle->RxQueue[pCanHandle->RxNdx].MSGCTRL.bit.rsvd1  = TRUE;
    pCanHandle->RxQueue[pCanHandle->RxNdx].MSGCTRL.bit.DLC    = pMailbox->MSGCTRL.bit.DLC;
    pCanHandle->RxQueue[pCanHandle->RxNdx].MSGID.all          = pMailbox->MSGID.all;
    pCanHandle->RxQueue[pCanHandle->RxNdx].MDL.all            = pMailbox->MDL.all;
    pCanHandle->RxQueue[pCanHandle->RxNdx].MDH.all            = pMailbox->MDH.all;
    if (++pCanHandle->RxNdx >= CAN_RX_QUEUE_SIZE)
    {
      pCanHandle->RxNdx = 0;
    }

    pCanHandle->pECanRegs->CANRMP.all  = mask;                     // Clear the Rcv Mailbox Pending flag
    pCanHandle->pECanRegs->CANGIF0.all = CANGIF_GMIF_MSK;          // Clear global mailbox interrupt flag
  }

}


/******************************************************************************
    FUNCTION    CanRxMessage()

    Description:
        Dequeue receive payload buffers

    Entrance Conditions:
            
    Exit Conditions:
            
    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all
      
    returns:
      TRUE if payload is available, otherwise FALSE
*******************************************************************************/
Uint16 CanRxMessage(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  Uint16 rc;

  if (pCanHandle->RxQueue[pCanHandle->RxOdx].MSGCTRL.bit.rsvd1)                     // check pending flag
  {
    pMsg->MSGID.all       = pCanHandle->RxQueue[pCanHandle->RxOdx].MSGID.all;
    pMsg->MSGCTRL.bit.DLC = pCanHandle->RxQueue[pCanHandle->RxOdx].MSGCTRL.bit.DLC;
    pMsg->MDL.all         = pCanHandle->RxQueue[pCanHandle->RxOdx].MDL.all;
    pMsg->MDH.all         = pCanHandle->RxQueue[pCanHandle->RxOdx].MDH.all;
    pCanHandle->RxQueue[pCanHandle->RxOdx].MSGCTRL.bit.rsvd1 = FALSE;
    if (++pCanHandle->RxOdx >= CAN_RX_QUEUE_SIZE)                                   // move out-dex to next element
    {
      pCanHandle->RxOdx = 0;
    }
    rc = TRUE;
  }
  else
  {
    rc = FALSE;
  }

  return rc;
}


/******************************************************************************
  End of file
*******************************************************************************/
