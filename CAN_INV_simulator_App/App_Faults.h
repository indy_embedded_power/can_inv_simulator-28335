/******************************************************************************
  FILENAME: App_Faults.h

 ******************************************************************************
  Copyright 2016 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     6/23/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/
#ifndef APP_FAULTS_H
#define APP_FAULTS_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP2833x Header files
#include "Common_AppTimers.h"                         // prototypes for Application Timers


//****************************************
//  structure definitions
//****************************************
typedef struct FAULT_STATUS_BITS_S
{
  Uint16 DcLinkOV       : 1;                          // BYTE0
  Uint16 DcLinkUV       : 1;
  Uint16 Estop          : 1;
  Uint16 SysShutdown    : 1;
  Uint16 OverTemp       : 1;
  Uint16 Fan            : 1;
  Uint16 HbeatTimeout   : 1;
  Uint16 OutputCB       : 1;

  Uint16 Reserved10     : 1;                          // BYTE1
  Uint16 Reserved11     : 1;
  Uint16 Reserved12     : 1;
  Uint16 Reserved13     : 1;
  Uint16 Reserved14     : 1;
  Uint16 Reserved15     : 1;
  Uint16 Reserved16     : 1;
  Uint16 Reserved17     : 1;

  Uint16 Contactor1     : 1;                          // BYTE2
  Uint16 Fuse1_Fail     : 1;
  Uint16 Powerex1       : 1;
  Uint16 Output1_OV     : 1;
  Uint16 Output1_UV     : 1;
  Uint16 Output1_OC     : 1;
  Uint16 Output1_PKOV   : 1;
  Uint16 Output1_PKOC   : 1;

  Uint16 Reserved30     : 1;                          // BYTE3
  Uint16 Reserved31     : 1;
  Uint16 Reserved32     : 1;
  Uint16 Reserved33     : 1;
  Uint16 Reserved34     : 1;
  Uint16 Reserved35     : 1;
  Uint16 Reserved36     : 1;
  Uint16 Reserved37     : 1;

  Uint16 Contactor2     : 1;                          // BYTE4
  Uint16 Fuse2_Fail     : 1;
  Uint16 Powerex2       : 1;
  Uint16 Output2_OV     : 1;
  Uint16 Output2_UV     : 1;
  Uint16 Output2_OC     : 1;
  Uint16 Output2_PKOV   : 1;
  Uint16 Output2_PKOC   : 1;

  Uint16 Reserved50     : 1;                          // BYTE5
  Uint16 Reserved51     : 1;
  Uint16 Reserved52     : 1;
  Uint16 Reserved53     : 1;
  Uint16 Reserved54     : 1;
  Uint16 Reserved55     : 1;
  Uint16 Reserved56     : 1;
  Uint16 Reserved57     : 1;

  Uint16 Reserved60     : 1;                          // BYTE6
  Uint16 Reserved61     : 1;
  Uint16 Reserved62     : 1;
  Uint16 Reserved63     : 1;
  Uint16 Reserved64     : 1;
  Uint16 Reserved65     : 1;
  Uint16 Reserved66     : 1;
  Uint16 Reserved67     : 1;

  Uint16 Reserved70     : 1;                          // BYTE7
  Uint16 Reserved71     : 1;
  Uint16 Reserved72     : 1;
  Uint16 Reserved73     : 1;
  Uint16 Reserved74     : 1;
  Uint16 Reserved75     : 1;
  Uint16 Reserved76     : 1;
  Uint16 Reserved77     : 1;
} FAULT_STATUS_BITS_T;

typedef union FAULT_STATUS_U
{
  Uint64 all;
  FAULT_STATUS_BITS_T bit;
  BYTE8_T byte;
} FAULT_STATUS_T;

typedef struct FAULT_WARNING_STATUS_BITS_S
{
  Uint16 Reserved00     : 1;                          // BYTE0
  Uint16 Reserved01     : 1;
  Uint16 Reserved02     : 1;
  Uint16 Reserved03     : 1;
  Uint16 Reserved04     : 1;
  Uint16 Reserved05     : 1;
  Uint16 Reserved06     : 1;
  Uint16 Reserved07     : 1;

  Uint16 Reserved10     : 1;                          // BYTE1
  Uint16 Reserved11     : 1;
  Uint16 Reserved12     : 1;
  Uint16 Reserved13     : 1;
  Uint16 Reserved14     : 1;
  Uint16 Reserved15     : 1;
  Uint16 Reserved16     : 1;
  Uint16 Reserved17     : 1;

  Uint16 Reserved20     : 1;                          // BYTE2
  Uint16 Reserved21     : 1;
  Uint16 Reserved22     : 1;
  Uint16 Reserved23     : 1;
  Uint16 Reserved24     : 1;
  Uint16 Reserved25     : 1;
  Uint16 Reserved26     : 1;
  Uint16 Reserved27     : 1;

  Uint16 Reserved30     : 1;                          // BYTE3
  Uint16 Reserved31     : 1;
  Uint16 Reserved32     : 1;
  Uint16 Reserved33     : 1;
  Uint16 Reserved34     : 1;
  Uint16 Reserved35     : 1;
  Uint16 Reserved36     : 1;
  Uint16 Reserved37     : 1;

  Uint16 Reserved40     : 1;                          // BYTE4
  Uint16 Reserved41     : 1;
  Uint16 Reserved42     : 1;
  Uint16 Reserved43     : 1;
  Uint16 Reserved44     : 1;
  Uint16 Reserved45     : 1;
  Uint16 Reserved46     : 1;
  Uint16 Reserved47     : 1;

  Uint16 Reserved50     : 1;                          // BYTE5
  Uint16 Reserved51     : 1;
  Uint16 Reserved52     : 1;
  Uint16 Reserved53     : 1;
  Uint16 Reserved54     : 1;
  Uint16 Reserved55     : 1;
  Uint16 Reserved56     : 1;
  Uint16 Reserved57     : 1;

  Uint16 Reserved60     : 1;                          // BYTE6
  Uint16 Reserved61     : 1;
  Uint16 Reserved62     : 1;
  Uint16 Reserved63     : 1;
  Uint16 Reserved64     : 1;
  Uint16 Reserved65     : 1;
  Uint16 Reserved66     : 1;
  Uint16 Reserved67     : 1;

  Uint16 Reserved70     : 1;                          // BYTE7
  Uint16 Reserved71     : 1;
  Uint16 Reserved72     : 1;
  Uint16 Reserved73     : 1;
  Uint16 Reserved74     : 1;
  Uint16 Reserved75     : 1;
  Uint16 Reserved76     : 1;
  Uint16 Reserved77     : 1;
} FAULT_WARNING_STATUS_BITS_T;

typedef union FAULT_WARNING_STATUS_U
{
  Uint64 all;
  FAULT_WARNING_STATUS_BITS_T bit;
  BYTE8_T byte;
} FAULT_WARNING_STATUS_T;


//****************************************
//  constant definitions
//****************************************
#define FAULT_UV_TIME                         (5000)    // Output UV fault exists
#define FAULT_OV_TIME                         (80)      // Output OV fault exists
#define FAULT_OC_TIME                         (5000)    // Output OC fault exists
#define FAULT_ESTOP_TIME                      (100)     // ESTOP exists
#define FAULT_HEARTBEAT_TIME                  (40000)   // Times out if we haven't got a message
#define FAULT_OCPK_TIME                       (10000)   // Resets fault counter after no fault for this time
#define FAULT_OVPK_TIME                       (10000)   // Resets fault counter after no fault for this time

#define FAULT_OCPK_MAX_COUNT                  (3)       // Max # of tries before latch off
#define FAULT_OVPK_MAX_COUNT                  (3)       // Max # of tries before latch off
#define FAULT_OCPK_LEVEL_PCT                  (0.10)    // 10% of full scale current
#define FAULT_OVPK_LEVEL_PCT                  (0.90)    // 90% of setpoint voltage
#define FAULT_UVLO_LEVEL_PCT                  (0.975)   // 97.5% of setpoint voltage
#define FAULT_OVLO_LEVEL_PCT                  (0.975)   // 97.5% of setpoint voltage

//****************************************
//  global variable definitions
//****************************************
extern FAULT_STATUS_T FaultStatus;
extern FAULT_WARNING_STATUS_T FaultWarningStatus;
extern APPTMR_HANDLE_T FaultMasterHeartbeatTimerHandle;


//****************************************
//  function macro definitions
//****************************************


//****************************************
//  function prototypes
//****************************************
void FaultInit(void);
void FaultHandler(void);

#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
