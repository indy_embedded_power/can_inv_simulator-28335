/******************************************************************************
  FILENAME: Common_CpuTimers.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:
    based on example code in DSP2833x_CpuTimers.c

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_CpuTimers.h"


//****************************************
//  global variables
//****************************************


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
// local function prototypes
//****************************************




/*****************************************************************************
    FUNCTION    CpuTimer0Init()

    Description:
      Initialize CpuTimer0 to the period specified. 
      Enable CPU and PIE interrupts and initialize PIE vector if TimerIsr!=NULL 
    
    Entrance Conditions:
            
    Exit Conditions:
      CpuTimer0 initialized and started
      TINT0 vector initialized and interrupts enabled if TimerIsr!=NULL
      
    args:
      PINT  TimerISR    pointer to Isr function 
      Uint32 Period     Period in uS
      
    returns:
      void
 *****************************************************************************/
void CpuTimer0Init(PINT TimerIsr,Uint32 Period)
{
  CpuTimer0Regs.TCR.bit.TSS  = 1;                     // Make sure timer is stopped
  CpuTimer0Regs.PRD.all      = (CPUTIMER_SYSCLKOUT * Period);// Initialize timer period
  CpuTimer0Regs.TPR.all      = 0;                     // Initialize pre-scale counter to divide by 1 (SYSCLKOUT)
  CpuTimer0Regs.TPRH.all     = 0;
  CpuTimer0Regs.TCR.bit.TRB  = 1;                     // 1 = reload timer
  CpuTimer0Regs.TCR.all      = 0;                     // Enable Timer - Use write-only instruction to set ,TSS=0

  if (TimerIsr)                                       // Interrupts enabled only if TimerIsr function is provided
  {                                                   
    EALLOW;                                           
    PieVectTable.TINT0 = TimerIsr;                    // Map TimerIsr Function to TINT0
    EDIS;
                                                   
    CpuTimer0Regs.TCR.bit.TIE = 1;                    // Enable peripheral interrupt
    IER |= M_INT1;                                    // Enable CPU interrupt (INT1) 
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;                // Enable PIE interrupt (Group 1 interrupt 7)
  }
}


/*****************************************************************************
    FUNCTION    CpuTimer1Init()

    Description:
      Initialize CpuTimer1 to the period specified. 
      Enable CPU interrupt and initialize PIE vector if TimerIsr!=NULL 
    
    Entrance Conditions:
            
    Exit Conditions:
      CpuTimer1 initialized and started
      TINT1 vector initialized and interrupts enabled if TimerIsr!=NULL
      
    args:
      PINT  TimerISR    pointer to Isr function 
      Uint32 Period     Period in uS
      
    returns:
      void
 *****************************************************************************/
void CpuTimer1Init(PINT TimerIsr,Uint32 Period)
{
  CpuTimer1Regs.TCR.bit.TSS  = 1;                     // Make sure timer is stopped
  CpuTimer1Regs.PRD.all      = (CPUTIMER_SYSCLKOUT * Period);// Initialize timer period
  CpuTimer1Regs.TPR.all      = 0;                     // Initialize pre-scale counter to divide by 1 (SYSCLKOUT)
  CpuTimer1Regs.TPRH.all     = 0;
  CpuTimer1Regs.TCR.bit.TRB  = 1;                     // 1 = reload timer
  CpuTimer1Regs.TCR.all      = 0;                     // Enable Timer - Use write-only instruction to set ,TSS=0

  if (TimerIsr)                                       // Interrupts enabled only if TimerIsr function is provided
  {                                                   
    EALLOW;                                           
    PieVectTable.XINT13 = TimerIsr;                   // Map TimerIsr Function to XINT13/TINT1
    EDIS;
                                                   
    CpuTimer1Regs.TCR.bit.TIE = 1;                    // Enable peripheral interrupt
    IER |= M_INT13;                                   // Enable CPU interrupt (INT13) 
  }
}


/*****************************************************************************
    FUNCTION    CpuTimer2Init()

    Description:
      Initialize CpuTimer2 to the period specified. 
      Enable CPU interrupt and initialize PIE vector if TimerIsr!=NULL 
    
    Entrance Conditions:
            
    Exit Conditions:
      CpuTimer2 initialized and started
      TINT2 vector initialized and interrupts enabled if TimerIsr!=NULL
      
    args:
      PINT  TimerISR    pointer to Isr function 
      Uint32 Period     Period in uS
      
    returns:
      void
 *****************************************************************************/
void CpuTimer2Init(PINT TimerIsr,Uint32 Period)
{
  CpuTimer2Regs.TCR.bit.TSS  = 1;                     // Make sure timer is stopped
  CpuTimer2Regs.PRD.all      = (CPUTIMER_SYSCLKOUT * Period);// Initialize timer period
  CpuTimer2Regs.TPR.all      = 0;                     // Initialize pre-scale counter to divide by 1 (SYSCLKOUT)
  CpuTimer2Regs.TPRH.all     = 0;
  CpuTimer2Regs.TCR.bit.TRB  = 1;                     // 1 = reload timer
  CpuTimer2Regs.TCR.all      = 0;                     // Enable Timer - Use write-only instruction to set ,TSS=0

  if (TimerIsr)                                       // Interrupts enabled only if TimerIsr function is provided
  {                                                   
    EALLOW;                                           
    PieVectTable.TINT2 = TimerIsr;                    // Map TimerIsr Function to TINT2
    EDIS;
                                                   
    CpuTimer2Regs.TCR.bit.TIE = 1;                    // Enable peripheral interrupt
    IER |= M_INT14;                                   // Enable CPU interrupt (INT14) 
  }
}


/*****************************************************************************
    FUNCTION    CpuTimer0Isr()

    Description:
      example interrupt code
      this is conditional compiled out
      isr needs to run out of ram only if you need to reprogram the flash in system
    
    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
#if 0
#pragma CODE_SECTION(CpuTimer0Isr,"ramfuncs");
interrupt void CpuTimer0Isr(void)
{
  EINT;                                               // re-enable Global interrupt INTM
  // ISR code here...
  PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;             // Acknowledge this interrupt to receive more interrupts from group 1
}
#endif


/*****************************************************************************
    FUNCTION    CpuTimer1Isr()

    Description:
      example interrupt code
      this is conditional compiled out
      isr needs to run out of ram only if you need to reprogram the flash in system

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
#if 0
#pragma CODE_SECTION(CpuTimer1Isr,"ramfuncs");
interrupt void CpuTimer1Isr(void)
{
  EINT;                                               // re-enable Global interrupt INTM
  // ISR code here...
  EDIS;                                               // The CPU acknowledges the interrupt.
}
#endif


/*****************************************************************************
    FUNCTION    CpuTimer2Isr()

    Description:
      example interrupt code
      this is conditional compiled out
      isr needs to run out of ram only if you need to reprogram the flash in system

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
#if 0
#pragma CODE_SECTION(CpuTimer2Isr,"ramfuncs");
interrupt void CpuTimer2Isr(void)
{  
  EINT;                                               // re-enable Global interrupt INTM
  // ISR code here...
  EDIS;                                               // The CPU acknowledges the interrupt.
}
#endif





/*****************************************************************************
End of file
******************************************************************************/
