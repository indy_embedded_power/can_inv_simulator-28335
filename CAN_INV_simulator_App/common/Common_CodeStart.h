/******************************************************************************
  FILENAME: Common_CodeStart.h

 ******************************************************************************
  Copyright 2016 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/21/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:
  Based on TI file: DSP2833x_Examples.h
  Defines C macro DELAY_US() which is a wrapper for asm function DSP28x_usDelay
  found in Common_CodeStart.asm

*******************************************************************************/
#ifndef COMMON_CODESTART_H
#define COMMON_CODESTART_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "DSP2833x_Device.h"

//****************************************
//  structure definitions
//****************************************


//****************************************
//  constant definitions
//****************************************
// Specify the clock rate of the CPU (SYSCLKOUT) in nS.
#define CPU_RATE    6.667L   // for a 150MHz CPU clock speed (SYSCLKOUT)


//****************************************
//  global variable definitions 
//****************************************
// These variables created by the linker
extern Uint16 RamfuncsLoadStart;
extern Uint16 RamfuncsLoadEnd;
extern Uint16 RamfuncsRunStart;

extern Uint16 rtsLibLoadStart;
extern Uint16 rtsLibLoadEnd;
extern Uint16 rtsLibRunStart;

extern Uint16 psimLibLoadStart;
extern Uint16 psimLibLoadEnd;
extern Uint16 psimLibRunStart;


//****************************************
//  function macro definitions
//****************************************
#define DELAY_US(A)   DSP28x_usDelay(((((long double) A * 1000.0L) / \
        (long double)CPU_RATE) - 9.0L) / 5.0L)

#define pADC_Cal      (void (*) (void)) 0x380080


//****************************************
//  function prototypes 
//****************************************
extern void DSP28x_usDelay(Uint32);
extern void SetDBGIER(Uint16);

#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
