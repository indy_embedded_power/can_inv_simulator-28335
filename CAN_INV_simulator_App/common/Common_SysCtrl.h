/******************************************************************************
  FILENAME: Common_SysCtrl.h

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef SYSCTRL_H        
#define SYSCTRL_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                                      // DSP280xx Header files



//****************************************
// structure definitions
//****************************************


//****************************************
// constant definitions
//****************************************


//****************************************
// global variable definitions 
//****************************************


//****************************************
// function macro definitions
//****************************************
#define SYSCTRL_ENABLE_WATCHDOG()     SysCtrlRegs.WDCR |= 0x0028

//****************************************
// function prototypes 
//****************************************
extern void SysCtrlInit(void);
extern void SysCtrlFlashInit(void);
extern void SysCtrlWatchdogInit(void);
extern void SysCtrlServiceDog(void);
extern void SysCtrlDisableDog(void);
extern void SysCtrlIntOsc1Sel(void);
extern void SysCtrlIntOsc2Sel(void); 
extern void SysCtrlExtClkSel(void); 




#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  $Log: not supported by cvs2svn $
  Revision 1.2  2010/10/22 22:00:52  Stephen
  Updated with SCI re-factoring

  Revision 1.1  2010/06/30 20:08:37  Stephen
  Initial check in to CVS based on breadboard software.
  New software directory structure for production code.

 *****************************************************************************
  End of file
*******************************************************************************/
