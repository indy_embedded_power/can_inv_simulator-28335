/******************************************************************************
  FILENAME: Common_Version.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     2/15/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_Version.h"


//****************************************
//  global variables
//****************************************
#ifdef __cplusplus
#pragma DATA_SECTION("handshake")
#else
#pragma DATA_SECTION(HandshakeFlags,"handshake");
#endif
volatile HANDSHAKE_FLAGS_T HandshakeFlags;


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
// local function prototypes
//****************************************




/*****************************************************************************
    FUNCTION    VersionCheck()

    Description:
      compute checksum of Flash area based on VERSION_DATA struct

    Entrance Conditions:
                
    Exit Conditions:
      
    args:
      const VERSION_DATA_T *pVersion    pointer to version structure to verify 
      
    returns:
      Uint16 checksum     passed = 0x0000, 
                          failed = 0xFFFF=not programmed, 
                                   0x????=the calculated 2's compliment checksum
 *****************************************************************************/
Uint16 VersionCheck(const VERSION_DATA_T *pVersion)
{
  Uint16 checksum = 0;
  Uint16 *pFlash;

  if (pVersion->Major == 0xFFFF)
  {
    checksum = 0xFFFF;                                // flash is erased, don't bother with checksum
  }
  else
  {
    for (pFlash = pVersion->pStartAddr; pFlash <= pVersion->pEndAddr; pFlash++)
    {
      checksum += *pFlash;                            // calculate Flash checksum
    }
    if (checksum != 0)                                // check if valid (checksum=0)
    {
      // checksum without Version.Checksum, take 2's compliment for ref
      checksum = ~(checksum - pVersion->Checksum) + 1;
    }
  }

  return(checksum);
}


/*****************************************************************************
End of file
******************************************************************************/
