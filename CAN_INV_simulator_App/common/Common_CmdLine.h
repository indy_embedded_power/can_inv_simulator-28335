/******************************************************************************
  FILENAME: Common_CmdLine.h

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/30/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_CMD_LINE_HEADER
#define COMMON_CMD_LINE_HEADER

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
// include files 
//****************************************
#include "Common_Sci.h"


//****************************************
//  constant definitions
//****************************************
#define CmdLinePuts             SciAPuts
#define CmdLineGetChar          SciAGetChar
#define CmdLinePutChar          SciAPutChar
#define CmdLineKbHit            SciAKbHit
#define CMD_LINE_SIZE           63


//****************************************
//  structure definitions
//****************************************
// Command data structure
// Note: when initializing this structure the last entry must be:
//       {NULL, 0, NULL, NULL}
typedef struct
{
  const char *cmd;                    // command string
  const char len;                     // length of the command string
  void (*fn) ( char *);               // command service function
  const char *descr;                  // command description
} CMD_LINE_T;


//****************************************
//  global variable definitions
//****************************************


//****************************************
//  function macro definitions
//****************************************


//****************************************
// function prototypes 
//****************************************
void CmdLineInit(void);
void CmdLineHandler(const CMD_LINE_T *CmdTablePtr);


#ifdef __cplusplus
}
#endif

#endif
/*******************************************************************************
  End of file
*******************************************************************************/
