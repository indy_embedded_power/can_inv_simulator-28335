/******************************************************************************
  FILENAME: Common_BitMacro.h

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef BITMACRO_HEADER
#define BITMACRO_HEADER

#ifdef __cplusplus
extern "C" {
#endif
/**********************************************************************
 *  MACROS BITSET(),BITCLR(),ISBITSET(),ISBITCLR(),ISMSKSET(),ISMSKCLR()
 *
 *  Description:
 *  BITSET and BITCLR are used to set or clear individual bits in a data type
 *  ISBITSET and ISBITCLR are used for checking to see if a bit is set or clear
 *  ISMSKSET and ISMSKCLR are used for checking if a group of bits are set or clear
 *
 *  Usage:
 *  BITSET(var,bit);
 *  BITCLR(var,bit);
 *
 *  ISBITSET(var,bit)   returns non-zero if set, 0 if clear
 *  ISBITCLR(var,bit)   returns non-zero if clear, 0 if set
 *
 *  ISMSKSET(var,msk)   returns non-zero if all mask bits set, 0 if clear
 *  ISMSKCLR(var,msk)   returns non-zero if all mask bits clear, 0 if set
 *  ISANYSET(var,msk)   returns non-zero if any mask bits set, 0 if clear
 *
 ***********************************************************************/
#define BITSET(var,bit)     ((var)|=(1L<<(bit)))
#define BITCLR(var,bit)     ((var)&= ~(1L<<(bit)))
#define ISBITSET(var,bit)   ((var) & (1L<<(bit)))
#define ISBITCLR(var,bit)   (((var) & (1L<<(bit)))==0)
#define ISMSKSET(var,msk)   (((var) & (msk))==(msk))
#define ISMSKCLR(var,msk)   (((var) & (msk))==0)
#define ISANYSET(var,msk)   (((var) & (msk))!=0)

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  End of file
*******************************************************************************/
