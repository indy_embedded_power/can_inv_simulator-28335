/******************************************************************************
  FILENAME: App_Version.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     2/15/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_Version.h"


//****************************************
//  global variables
//****************************************
#ifdef __cplusplus
#pragma DATA_SECTION("app_data")
#else
#pragma DATA_SECTION(AppVersion,"app_data");
#endif
const VERSION_DATA_T AppVersion = 
{
  0xFFFF,                                             // Checksum -- get from [ ] in boot message
  99,                                                 // Version PartNo
  0,                                                  // Version Major
  0,                                                  // Version Minor
  (void*) APP_STARTING_ADDRESS,                       // Start Address
  (void*) APP_ENDING_ADDRESS,                         // End Address
  (void (*)(void)) APP_CODE_ENTRY,                    // Application Entry Address
  'a',                                                // DbgRev letter [' ' for release]
  0xFFFF,                                             // reserved[5]
  0xFFFF,
  0xFFFF,
  0xFFFF,
  0xFFFF
};


#ifdef __cplusplus
#pragma DATA_SECTION("boot_data")
#else
#pragma DATA_SECTION(BootVersion,"boot_data");
#endif
const VERSION_DATA_T BootVersion;


/*****************************************************************************
End of file
******************************************************************************/
