/******************************************************************************
  FILENAME: Common_Version.h

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     2/15/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_VERSION_H
#define COMMON_VERSION_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                                      // DSP280xx Header files



//****************************************
// structure definitions
//****************************************
typedef struct VERSION_DATA_S
{
  Uint16  Checksum;
  Uint16  PartNo;
  Uint16  Major;
  Uint16  Minor;
  Uint16  *pStartAddr;
  Uint16  *pEndAddr;
  void    (*pCodeStart)(void);
  char    DbgRev;
  Uint16  reserved[5];
} VERSION_DATA_T;

typedef struct HANDSHAKE_FLAGS_S
{
  Uint16 Boot;
  Uint16 App;
} HANDSHAKE_FLAGS_T;


//****************************************
// constant definitions
//****************************************
#define VERSION_CHK_OK              0x0000
#define VERSION_CHK_ERASED          0xFFFF
#define HANDSHAKE_APP_DNLD          0xAA01
#define HANDSHAKE_APP_START         0xAA02
#define HANDSHAKE_APP_WAIT          0xAA03
#define HANDSHAKE_BOOT_OK           0x5555
#define HANDSHAKE_CLR               0x0000
#define HANDSHAKE_ERR               0xDEAD

#define APP_STARTING_ADDRESS        0x00300000
#define APP_ENDING_ADDRESS          0x0032FFFF
//x put APP_CODE_ENTRY at same place as BOOT_CODE_ENTRY until bootloader implemented
//x #define APP_CODE_ENTRY              0x0032FFF6
#define APP_CODE_ENTRY              0x0033FFF6

#define BOOT_STARTING_ADDRESS       0x00338000
#define BOOT_ENDING_ADDRESS         0x0033FFFF
#define BOOT_CODE_ENTRY             0x0033FFF6

//****************************************
// global variable definitions 
//****************************************
extern const VERSION_DATA_T AppVersion;               // memory allocated in App_Version.c
extern const VERSION_DATA_T BootVersion;              // memory allocated in Boot_Version.c
extern volatile HANDSHAKE_FLAGS_T HandshakeFlags;     // memory allocated in Common_Version.c


//****************************************
// function macro definitions
//****************************************


//****************************************
// function prototypes 
//****************************************
extern Uint16 VersionCheck(const VERSION_DATA_T *pVersion);



#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  End of file
*******************************************************************************/
