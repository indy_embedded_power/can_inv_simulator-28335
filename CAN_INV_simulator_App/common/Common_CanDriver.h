/******************************************************************************
  FILENAME: Common_CanDriver.h

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     4/28/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_CAN_DRIVER_H        
#define COMMON_CAN_DRIVER_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP28335 Header files
#include "Common_AppTimers.h"                         // prototypes for Application Timers


//****************************************
//  constant definitions
//****************************************
#define ECANA               0                         // set to 1 if ECANA is used, 0 if not used 
#define ECANB               1                         // set to 1 if ECANB is used, 0 if not used

#define CAN_RX_QUEUE_SIZE   16                        // number of entries in receive queue

#define CAN_TSEG1           10
#define CAN_TSEG2           2
#define CAN_BT              ((CAN_TSEG1 + 1) + (CAN_TSEG2 + 1) + 1)

#define CAN_BAUDRATE_A      500000UL                  // 500kbit data rate ECANA
#define CAN_BRPREG_A        (((CPU_FREQ / 2) / (CAN_BT * CAN_BAUDRATE_A)) - 1)

#define CAN_BAUDRATE_B      500000UL                  // 500kbit data rate ECANB
#define CAN_BRPREG_B        (((CPU_FREQ / 2) / (CAN_BT * CAN_BAUDRATE_B)) - 1)

#define CAN_IDE_MASK         (1UL << 31)
#define CAN_AME_MASK         (1UL << 30)


//****************************************
//  structure definitions
//****************************************
typedef struct CAN_HANDLE_S                                     // CAN handle for selecting which CAN port to use
{
  volatile struct ECAN_REGS   *pECanRegs;                       // pointers to eCAN register structures
  volatile struct ECAN_MBOXES *pECanMboxes;
  volatile struct LAM_REGS    *pECanLAMRegs;
  PINT                        pECanInterrupt;                   // pointer to eCAN RX interrupt function
  struct MBOX                 RxQueue[CAN_RX_QUEUE_SIZE];       // RX queue variables
  Uint16                      RxNdx;
  Uint16                      RxOdx;
  Uint16                      BrpReg;                           // baud rate prescale register value (determines CAN bit rate)
  Uint16                      NodeType;                         // IPS protocol node device type
  Uint16                      NodeIdx;                          // IPS protocol node device index (address on CAN bus)
  Uint16                      NodeFmt;                          // IPS protocol format type for ID
  Uint16                      ResetTries;                       // IPS protocol CAN reset tries when CAN bus hung
  APPTMR_HANDLE_T             ResetTimerHandle;                 // IPS protocol timer handle for CAN reset tries
} CAN_HANDLE_T;

//****************************************
//  global variable definitions 
//****************************************
#if ECANA
extern CAN_HANDLE_T CanA;
#endif
#if ECANB
extern CAN_HANDLE_T CanB;
#endif


//****************************************
//  function macro definitions
//****************************************


//****************************************
//  function prototypes 
//****************************************
extern void CanHandleInit(void);
extern void CanDriverInit(CAN_HANDLE_T *pCanHandle, Uint32 rcvNodeType, Uint32 rcvNodeIdx, Uint32 rcvMask);
extern void CanSetAccpMask(CAN_HANDLE_T *pCanHandle, Uint32 rcvNodeType, Uint32 rcvNodeIdx, Uint32 rcvMask);
extern Uint16 CanTxMessage(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
extern Uint16 CanRxMessage(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);


#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  End of file
*******************************************************************************/
