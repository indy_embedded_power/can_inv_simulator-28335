/******************************************************************************
  FILENAME: App_InvCtrl.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     2/1/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:
    This code based on heavily modified inverter_control.c from original code base

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_device.h"                            // DSP2833x Header files
#include "Common_NvVars.h"                            // prototypes Compensator Coefficients
#include "App_InvCtrl.h"                              // prototypes for InvCtrl


//****************************************
//  global variables
//****************************************
volatile INVCTRL_MEAS_T InvCtrlMeas;                           // Measurement Outputs in Volts/Amps/deg C
volatile float32 InvCtrlSinAmpPu[NUM_CHANNELS];                // Amplitude of Sine reference for each output channel
volatile INVCTRL_STATUS_T InvCtrlStatus[NUM_CHANNELS];         // Status of output, fault & test


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
//  local function prototypes
//****************************************


/******************************************************************************
    FUNCTION    InvCtrlInit()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void InvCtrlInit(void)
{
  Uint16 indx;

  //--- Clear Status variables
  InvCtrlStatus[CHAN1].all = 0x0000;
  InvCtrlStatus[CHAN2].all = 0x0000;

  //--- References initialization
  InvCtrlSinAmpPu[CHAN1] = (NvVars.Var.SetPointVout[CHAN1] * SQRT2) / VPK_MAX;
  InvCtrlSinAmpPu[CHAN2] = (NvVars.Var.SetPointVout[CHAN2] * SQRT2) / VPK_MAX;

  //--- Initialize Measurement outputs
  InvCtrlMeas.VdcLink = 0.0;
  for (indx = 0; indx < NUM_CHANNELS; indx++)
  {
    InvCtrlMeas.Vout[indx] = 0.0f;
    InvCtrlMeas.Iout[indx] = 0.0f;
    InvCtrlMeas.Pout[indx] = 0.0f;
    InvCtrlMeas.Freq[indx] = 0.0f;
    InvCtrlMeas.Iswitch[indx] = 0.0f;
    InvCtrlMeas.Tstk[indx] = 0.0f;
    InvCtrlMeas.Text[indx] = 0.0f;
  }

}


/******************************************************************************
    FUNCTION    InvCtrlPwmOn()

    Description:
      Enable PWM outputs at a zero crossing

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void InvCtrlPwmOn(void)
{
//x need to add code here
}


/******************************************************************************
    FUNCTION    InvCtrlPwmOff()

    Description:
      Disable PWM outputs & reset Fault flags

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void InvCtrlPwmOff(void)
{
  //x need to add code here
}





/*******************************************************************************
  End of file
*******************************************************************************/

