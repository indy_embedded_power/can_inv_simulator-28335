/******************************************************************************
  FILENAME: Common_Gpio.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

 *****************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_Gpio.h"                              // prototypes and Peripheral address definitions


//****************************************
//  global variables
//****************************************


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
// local function prototypes
//****************************************



/*****************************************************************************
    FUNCTION    GpioInit()

    Description:
      initialize GPIO

    Entrance Conditions:
            
    Exit Conditions:
      
    args:
      void
      
    returns:
      void
 *****************************************************************************/
void GpioInit(void)
{
  EALLOW;                                             // Enable EALLOW protected register access

  //--- Group A pins
  GpioCtrlRegs.GPACTRL.all        = 0x00000000;       // QUALPRD = SYSCLKOUT for all group A GPIO
  GpioCtrlRegs.GPAQSEL1.all       = 0x00000000;       // No qualification for all group A GPIO 0-15
  GpioCtrlRegs.GPAQSEL2.all       = 0x00000000;       // No qualification for all group A GPIO 16-31
  GpioDataRegs.GPACLEAR.all       = 0xFFFFFFFF;       // All group A outputs cleared
  GpioCtrlRegs.GPADIR.all         = 0x00000000;       // All group A GPIO are inputs
  GpioCtrlRegs.GPAPUD.all         = 0x00000000;       // All group A Pullups enabled, including GPIO0-11 (EPWM1-6)
  GpioCtrlRegs.GPAMUX1.all        = 0x00000000;       // All Group A1 pins are GPIO
  GpioCtrlRegs.GPAMUX2.all        = 0x00000000;       // All Group A2 pins are GPIO

  //--- Group B pins
  GpioCtrlRegs.GPBCTRL.all        = 0x00000000;       // QUALPRD = SYSCLKOUT for all group B GPIO
  GpioCtrlRegs.GPBQSEL1.all       = 0x00000000;       // No qualification for all group B GPIO 32-47
  GpioCtrlRegs.GPBQSEL2.all       = 0x00000000;       // No qualification for all group B GPIO 48-63
  GpioDataRegs.GPBCLEAR.all       = 0xFFFFFFFF;       // All group B outputs cleared
  GpioCtrlRegs.GPBDIR.all         = 0x00000000;       // All group B GPIO are inputs
  GpioCtrlRegs.GPBPUD.all         = 0x00000000;       // All group B pullups enabled
  GpioCtrlRegs.GPBMUX1.all        = 0x00000000;       // All Group B1 pins are GPIO
  GpioCtrlRegs.GPBMUX2.all        = 0x00000000;       // All Group B2 pins are GPIO

  //--- Group C pins
  GpioDataRegs.GPCCLEAR.all       = 0xFFFFFFFF;       // All group C outputs cleared
  GpioCtrlRegs.GPCDIR.all         = 0x00000000;       // All group C GPIO are inputs
  GpioCtrlRegs.GPCPUD.all         = 0x00000000;       // All group C pullups enabled
  GpioCtrlRegs.GPCMUX1.all        = 0x00000000;       // All Group C1 pins are GPIO
  GpioCtrlRegs.GPCMUX2.all        = 0x00000000;       // All Group C2 pins are GPIO

  EDIS;                                               // Disable EALLOW protected register access
}


/*****************************************************************************
    FUNCTION    GpioAppInit()

    Description:
      Initialize GPIO to application specific digital outputs and set qualification period on inputs
      GpioInit() sets all pins to GPIO inputs, so we only need to initialize outputs
      and qualification period for inputs
      Peripheral port initialization is done in the initialization functions of
      the specific peripherals, so it is not addressed here.

    Entrance Conditions:
      All GPIO previously initialized to inputs with pull-ups enabled

    Exit Conditions:
      Application digital inputs and outputs configured

    args:
      void

    returns:
      void
 *****************************************************************************/
void GpioAppInit(void)
{
  EALLOW;                                             // Enable EALLOW protected register access

  // set QUALPRD for port A and port B digital inputs
  GpioCtrlRegs.GPACTRL.bit.QUALPRD0 = GPIO_QUAL_PERIOD;
  GpioCtrlRegs.GPACTRL.bit.QUALPRD1 = GPIO_QUAL_PERIOD;
  GpioCtrlRegs.GPACTRL.bit.QUALPRD2 = GPIO_QUAL_PERIOD;
  GpioCtrlRegs.GPACTRL.bit.QUALPRD3 = GPIO_QUAL_PERIOD;

  GpioCtrlRegs.GPBCTRL.bit.QUALPRD0 = GPIO_QUAL_PERIOD;
  GpioCtrlRegs.GPBCTRL.bit.QUALPRD1 = GPIO_QUAL_PERIOD;
  GpioCtrlRegs.GPBCTRL.bit.QUALPRD2 = GPIO_QUAL_PERIOD;
  GpioCtrlRegs.GPBCTRL.bit.QUALPRD3 = GPIO_QUAL_PERIOD;


  // GPIO 60 - IN   ESTOP status
  GpioCtrlRegs.GPBQSEL2.bit.GPIO60  = 2;              // Qualification using 6 samples
  // GPIO 62 - IN   FIRE_ALARM status
  GpioCtrlRegs.GPBQSEL2.bit.GPIO62  = 2;              // Qualification using 6 samples

  // GPIO 58 - IN   GENERATOR_ON status
  GpioCtrlRegs.GPBQSEL2.bit.GPIO58  = 2;              // Qualification using 6 samples
  // GPIO 19 - OUT  GENERATOR_START drive
  GpioCtrlRegs.GPAPUD.bit.GPIO19    = 1;              // disable pull-up
  GpioCtrlRegs.GPAMUX2.bit.GPIO19   = 0;              // pin is GPIO
  GpioCtrlRegs.GPADIR.bit.GPIO19    = 1;              // pin is output
  GpioDataRegs.GPACLEAR.bit.GPIO19  = 1;              // default = 0

  // GPIO 49 - IN   CB_POSITION status
  GpioCtrlRegs.GPBQSEL2.bit.GPIO49  = 2;              // Qualification using 6 samples
  // GPIO 84 - OUT  CB_CLOSE drive
  GpioCtrlRegs.GPCPUD.bit.GPIO84    = 1;              // disable pull-up
  GpioCtrlRegs.GPCMUX2.bit.GPIO84   = 0;              // pin is GPIO
  GpioCtrlRegs.GPCDIR.bit.GPIO84    = 1;              // pin is output
  GpioDataRegs.GPCCLEAR.bit.GPIO84  = 1;              // default = 0
  // GPIO 86 - OUT  CB_OPEN drive
  GpioCtrlRegs.GPCPUD.bit.GPIO86    = 1;              // disable pull-up
  GpioCtrlRegs.GPCMUX2.bit.GPIO86   = 0;              // pin is GPIO
  GpioCtrlRegs.GPCDIR.bit.GPIO86    = 1;              // pin is output
  GpioDataRegs.GPCSET.bit.GPIO86    = 1;              // default = 1

  // GPIO 15 - IN   CONTACTOR1 status
  GpioCtrlRegs.GPAQSEL1.bit.GPIO15  = 2;              // Qualification using 6 samples
  // GPIO 85 - OUT  CONTACTOR1 drive
  GpioCtrlRegs.GPCPUD.bit.GPIO85    = 1;              // disable pull-up
  GpioCtrlRegs.GPCMUX2.bit.GPIO85   = 0;              // pin is GPIO
  GpioCtrlRegs.GPCDIR.bit.GPIO85    = 1;              // pin is output
  GpioDataRegs.GPCCLEAR.bit.GPIO85  = 1;              // default = 0

  // GPIO 12 - IN   CONTACTOR2 status
  GpioCtrlRegs.GPAQSEL1.bit.GPIO12  = 2;              // Qualification using 6 samples
  // GPIO 30 - OUT  CONTACTOR2 drive
  GpioCtrlRegs.GPAPUD.bit.GPIO30    = 1;              // disable pull-up
  GpioCtrlRegs.GPAMUX2.bit.GPIO30   = 0;              // pin is GPIO
  GpioCtrlRegs.GPADIR.bit.GPIO30    = 1;              // pin is output
  GpioDataRegs.GPACLEAR.bit.GPIO30  = 1;              // default = 0

  // GPIO 48 - IN   CONTACTOR3 status
  GpioCtrlRegs.GPBQSEL2.bit.GPIO48  = 2;              // Qualification using 6 samples
  // GPIO 32 - OUT  CONTACTOR3 drive
  GpioCtrlRegs.GPBPUD.bit.GPIO32    = 1;              // disable pull-up
  GpioCtrlRegs.GPBMUX1.bit.GPIO32   = 0;              // pin is GPIO
  GpioCtrlRegs.GPBDIR.bit.GPIO32    = 1;              // pin is output
  GpioDataRegs.GPBCLEAR.bit.GPIO32  = 1;              // default = 0

  // GPIO 24 - OUT  PRECHARGE1 drive
  GpioCtrlRegs.GPAPUD.bit.GPIO24    = 1;              // disable pull-up
  GpioCtrlRegs.GPAMUX2.bit.GPIO24   = 0;              // pin is GPIO
  GpioCtrlRegs.GPADIR.bit.GPIO24    = 1;              // pin is output
  GpioDataRegs.GPACLEAR.bit.GPIO24  = 1;              // default = 0

  // GPIO 26 - OUT  PRECHARGE2 drive
  GpioCtrlRegs.GPAPUD.bit.GPIO26    = 1;              // disable pull-up
  GpioCtrlRegs.GPAMUX2.bit.GPIO26   = 0;              // pin is GPIO
  GpioCtrlRegs.GPADIR.bit.GPIO26    = 1;              // pin is output
  GpioDataRegs.GPACLEAR.bit.GPIO26  = 1;              // default = 0

  // GPIO 87 - OUT  PRECHARGE3 drive
  GpioCtrlRegs.GPCPUD.bit.GPIO87    = 1;              // disable pull-up
  GpioCtrlRegs.GPCMUX2.bit.GPIO87   = 0;              // pin is GPIO
  GpioCtrlRegs.GPCDIR.bit.GPIO87    = 1;              // pin is output
  GpioDataRegs.GPCCLEAR.bit.GPIO87  = 1;              // default = 0

  // GPIO 18 - OUT  MUX_SEL0 drive
  GpioCtrlRegs.GPAPUD.bit.GPIO18    = 1;              // disable pull-up
  GpioCtrlRegs.GPAMUX2.bit.GPIO18   = 0;              // pin is GPIO
  GpioCtrlRegs.GPADIR.bit.GPIO18    = 1;              // pin is output
  GpioDataRegs.GPACLEAR.bit.GPIO18  = 1;              // default = 0

  // GPIO 20 - OUT  MUX_SEL1 drive
  GpioCtrlRegs.GPAPUD.bit.GPIO20    = 1;              // disable pull-up
  GpioCtrlRegs.GPAMUX2.bit.GPIO20   = 0;              // pin is GPIO
  GpioCtrlRegs.GPADIR.bit.GPIO20    = 1;              // pin is output
  GpioDataRegs.GPACLEAR.bit.GPIO20  = 1;              // default = 0

  // GPIO 22 - OUT  MUX_SEL2 drive
  GpioCtrlRegs.GPAPUD.bit.GPIO22    = 1;              // disable pull-up
  GpioCtrlRegs.GPAMUX2.bit.GPIO22   = 0;              // pin is GPIO
  GpioCtrlRegs.GPADIR.bit.GPIO22    = 1;              // pin is output
  GpioDataRegs.GPACLEAR.bit.GPIO22  = 1;              // default = 0

  // GPIO 34 - OUT  LED drive
  GpioCtrlRegs.GPBPUD.bit.GPIO34    = 1;              // disable pull-up
  GpioCtrlRegs.GPBMUX1.bit.GPIO34   = 0;              // pin is GPIO
  GpioCtrlRegs.GPBDIR.bit.GPIO34    = 1;              // pin is output
  GpioDataRegs.GPBCLEAR.bit.GPIO34  = 1;              // default = 0

  // GPIO 59 - IN   FUSE1 status
  GpioCtrlRegs.GPBQSEL2.bit.GPIO59  = 2;              // Qualification using 6 samples
  // GPIO 61 - IN   FUSE2 status
  GpioCtrlRegs.GPBQSEL2.bit.GPIO61  = 2;              // Qualification using 6 samples
  // GPIO 63 - IN   FUSE3 status
  GpioCtrlRegs.GPBQSEL2.bit.GPIO63  = 2;              // Qualification using 6 samples

  // GPIO 13 - IN   FAN_TACH status
//x moved to EpwmInit() to use for TZ2
//x  GpioCtrlRegs.GPAQSEL1.bit.GPIO13  = 1;              // Qualification using 3 samples
  // GPIO 14 - IN   FAN_TACH status
//x moved to EpwmInit() to use for TZ3
//x  GpioCtrlRegs.GPAQSEL1.bit.GPIO14  = 1;              // Qualification using 3 samples

  // GPIO 21 - IN   FAULT2 status
  GpioCtrlRegs.GPAQSEL2.bit.GPIO21  = 1;              // Qualification using 3 samples
  // GPIO 23 - IN   FAULT1 status
  GpioCtrlRegs.GPAQSEL2.bit.GPIO23  = 1;              // Qualification using 3 samples

  // GPIO 33 - OUT  DEBUG_TEST drive
  GpioCtrlRegs.GPBPUD.bit.GPIO33    = 1;              // disable pull-up
  GpioCtrlRegs.GPBMUX1.bit.GPIO33   = 0;              // pin is GPIO
  GpioCtrlRegs.GPBDIR.bit.GPIO33    = 1;              // pin is output
  GpioDataRegs.GPBCLEAR.bit.GPIO33  = 1;              // default = 0

  // GPIO 31 - OUT CANbus TIMING Timing Debug Test
  GpioCtrlRegs.GPAPUD.bit.GPIO31    = 1;              // disable pull-up
  GpioCtrlRegs.GPAMUX2.bit.GPIO31   = 0;              // pin is GPIO
  GpioCtrlRegs.GPADIR.bit.GPIO31    = 1;              // pin is output
  GpioDataRegs.GPACLEAR.bit.GPIO31  = 1;              // default = 0

  EDIS;                                               // Disable EALLOW protected register access
}


/*****************************************************************************
End of file
******************************************************************************/
