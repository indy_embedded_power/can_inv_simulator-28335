/******************************************************************************
  FILENAME: Common_CmdLine.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/30/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Description:
  Simple command line interface.
  It uses a command table in cmdcmds.h. The last command pointer in the
  command table must be NULL.  The output device (usart) must be initialized
  prior to initializing this interface.

  The following implementation specific items must be defined in Common_CmdLine.h:

    CmdLinePuts
    CmdLineGetChar
    CmdLinePutChar
    CmdLineKbHit
    CMD_LINE_SIZE - command line length (max 255)
*******************************************************************************/

//****************************************
//  include files
//****************************************
#include <string.h>
#include <stddef.h>
#include "Common_CmdLine.h"

//****************************************
//  global variables
//****************************************


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************
static unsigned char Cmd_Index;
static char Cmd_Line[CMD_LINE_SIZE + 1];
static const char Cmd_Prompt[] = "\r\nInv Sim App> ";

//****************************************
//  local function prototypes
//****************************************




/*****************************************************************************
    FUNCTION    CmdLineInit()

    Description:
      Initializes the command line interface by doing the following:
      prints the command line Cmd_Prompt
      initializes the command line index      

    Entrance Conditions:

    Exit Conditions:

    args:
      void
    returns:
      void
 *****************************************************************************/
void CmdLineInit(void)
{
  Cmd_Index = CMD_LINE_SIZE+1;
  while (Cmd_Index > 0)
    Cmd_Line[--Cmd_Index] = ' ';

  CmdLinePuts(Cmd_Prompt);
}


/*****************************************************************************
    FUNCTION    CmdLinehandler()

    Description:
      Handles command line input.
      Commands are terminated by a CR or LF.
      Backspace are handled and command line overflows are signaled by BELL character.
      This routine looks up commands in the command line table passed to the 
      routine and calls the service function for that command.  
      If the command is not found in the table a ? is printed on the user interface.      

    Entrance Conditions:

    Exit Conditions:

    args:
      const CMD_LINE_T *cmd_table     table of command names and functions
    returns:
      void
 *****************************************************************************/
void CmdLineHandler(const CMD_LINE_T *cmd_table)
{
  unsigned char tmp;
  const CMD_LINE_T *cptr;

  if (!CmdLineKbHit())
    return;

  tmp = CmdLineGetChar();
  if ((tmp == 0x0a) || (tmp == 0x0d))
  {
    // Got a complete command line
    Cmd_Line[Cmd_Index] = '\0';
    CmdLinePuts("\r\n");

    // Look up the command in the table
    cptr = cmd_table;
    for(;;)
    {
      if (cptr->cmd == NULL)
      {
        // got to end without finding it
        CmdLinePuts("?");
        break;
      }
      else if (strncmp(Cmd_Line, cptr->cmd, cptr->len) == 0)
      {
        // found the command, execute its service function
        // pass a pointer to the arguments
        cptr->fn(&Cmd_Line[cptr->len + 1]);
        break;
      }
      cptr++;
    }
    // clear command line buffer and reset index pointer
    Cmd_Index = CMD_LINE_SIZE+1;
    while (Cmd_Index > 0)
    Cmd_Line[--Cmd_Index] = ' ';

    CmdLinePuts(Cmd_Prompt);
  }
  else if (tmp == '\b')
  {
    // backspace character
    if (Cmd_Index > 0)
    {
      Cmd_Index--;
      CmdLinePuts("\b \b");
    }
  }
  else
  {
    // add character to the command and echo it if there is room
    if (Cmd_Index < (CMD_LINE_SIZE - 1))
    {
      Cmd_Line[Cmd_Index++] = tmp;
      CmdLinePutChar(tmp);
    }
    else
    {
      CmdLinePutChar('\x07');
    }
  }
}


/*******************************************************************************
  End of file
*******************************************************************************/
