/******************************************************************************
  FILENAME: Common_Sci.c

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/30/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_Sci.h"                               // Sci function prototypes


//****************************************
//  global variables
//****************************************


//****************************************
//  local constant definitions
//****************************************
#define SCI_LSPCLK          (CPU_FREQ/6)              // LSPCLK set up in SysCtrlInit()
#define SCI_BUF_SIZE        (128)                     // buffer size in chars

//****************************************
// structure definitions
//****************************************
typedef struct
{                                                     // RX or TX buffer
  Uint16 buffer[SCI_BUF_SIZE];                        // read/write buffer for SCI channel
  Uint16 count;                                       // # of chars in buffer
  Uint16 indx_in;                                     // index to next free char
  Uint16 indx_out;                                    // index to next char to get
} SCI_BUFFER_T;


//****************************************
//  local variables
//****************************************
static volatile SCI_BUFFER_T SciA_TxBuf;              // transmit buffer
static volatile SCI_BUFFER_T SciA_RxBuf;              // receive buffer


//****************************************
// local function prototypes
//****************************************
interrupt void SciA_TxFifoIsr(void);
interrupt void SciA_RxFifoIsr(void);



/*****************************************************************************
    FUNCTION    SciAInit()

    Description:
      Initialize SCIA peripheral for given BAUD rate


    Entrance Conditions:
      LSPCLK is configured for 25MHz in SysCtrlInit()
      Clocks turned on to the SCI peripheral in SysCtrlInit()

    Exit Conditions:
      SCIA channel intialized as shown below

    args:
      Uint32 Baud       BAUD rate

    returns:
      void
 *****************************************************************************/
void SciAInit(Uint32 Baud)
{
  Uint32 brr;

  // initialize buffers
  SciA_TxBuf.indx_in  = 0;                            // tx buffer init
  SciA_TxBuf.indx_out = 0;
  SciA_TxBuf.count    = 0;

  SciA_RxBuf.indx_in  = 0;                            // rx buffer init
  SciA_RxBuf.indx_out = 0;
  SciA_RxBuf.count    = 0;

  EALLOW;
  // Initialize GPIO28 for SCI RXDA
  GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 1;                // set for SCIRXDA operation
  GpioCtrlRegs.GPAPUD.bit.GPIO28  = 0;                // Enable pull-up
  GpioCtrlRegs.GPAQSEL2.bit.GPIO28= 3;                // Asynch input
  // initialize GPIO29 for SCI TXDA
  GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 1;                // set for SCITXDA operation
  GpioCtrlRegs.GPAPUD.bit.GPIO29  = 1;                // Disable pull-up
  // initialize interrupt vectors
  PieVectTable.SCIRXINTA = &SciA_RxFifoIsr;           // if interrupts are used, they are re-mapped to
  PieVectTable.SCITXINTA = &SciA_TxFifoIsr;           // ISR functions found within this file.
  EDIS;

  // initialize frame format
  SciaRegs.SCICCR.all  = 0x0007;                      // 1 stop bit, No loopback, No parity, 8 char bits, async mode, idle-line protocol
  SciaRegs.SCICTL1.all = 0x0003;                      // enable TX, RX, internal SCICLK, Disable RX ERR, SLEEP, TXWAKE
  SciaRegs.SCICTL2.all = 0x0003;                      // Enable RXRDY & TXRDY interrupts to PIE

  // initialize baud rate
  brr = (SCI_LSPCLK / (Baud * 8L)) - 1L;              // calculate & set baud rate
  SciaRegs.SCIHBAUD    = (Uint16) ((brr>>8L) & 0xFF);
  SciaRegs.SCILBAUD    = (Uint16) (brr & 0xFF);

  // initialize FIFOs
  SciaRegs.SCIFFTX.all = 0x0040;                      // Reset SCI RX/TX, reset TX FIFO, Clear TXFFINT
  SciaRegs.SCIFFRX.all = 0x4044;                      // Clear RXFFOVF, reset RX FIFO, clear RXFFINT

  SciaRegs.SCIFFTX.all = 0xC022;                      // Enable SCI, Enable FIFO, Reset TX FIFO,Disable TX FIFO interrupt, TX FIFO level =2
  SciaRegs.SCIFFRX.all = 0x0021;                      // Enable RX FIFO interrupt, 
                                                      // FIFO interrupt with one or more entries in fifo
  SciaRegs.SCIFFCT.all = 0x0000;                      // Autobaud disabled, FIFO xfer delay =0
  SciaRegs.SCIPRI.all  = 0x0008;                      // Complete current event before emulate stop

  // start
  SciaRegs.SCICTL1.all = 0x0023;                      // Relinquish SCI from Reset
  SciaRegs.SCIFFTX.bit.TXFIFOXRESET = 1;              // Relinquish TX FIFO from reset
  SciaRegs.SCIFFRX.bit.RXFIFORESET  = 1;              // Relinquish RX FIFO from reset

  // enable interrupts
  PieCtrlRegs.PIEIER9.bit.INTx1 = 1;                  // PIE Group 9, INT1 (SCIRXINTA)
  PieCtrlRegs.PIEIER9.bit.INTx2 = 1;                  // PIE Group 9, INT2 (SCITXINTA)
  IER |= M_INT9;                                      // Enable CPU INT9
}


/*****************************************************************************
    FUNCTION    SciAKbHit()

    Description:
      Returns a value indicating whether a character is in the RX buffer or not.

    Entrance Conditions:

    Exit Conditions:

    args:
      void
    returns:
      0         no chars available
      non-zero  chars available
 *****************************************************************************/
Uint16 SciAKbHit(void)
{
  return(SciA_RxBuf.count);
}


/*****************************************************************************
    FUNCTION    SciAFlush()

    Description:
      Wait for TxBuf to empty.

    args:
      void
    returns:
      void
 *****************************************************************************/
void SciAFlush(void)
{
  while (SciA_TxBuf.count != 0)                        
  {
    // wait for empty tx buffer
  }

  while (SciaRegs.SCIFFTX.bit.TXFFST != 0)            
  {
    // wait for emty tx fifo
  }
}


/*****************************************************************************
    FUNCTION    SciAGetChar()

    Description:
      Returns a character from the receiver buffer or EOF if one doesn't exist.

    Entrance Conditions:

    Exit Conditions:

    args:
      void
    returns:
      int16    next char in buffer or EOF if buffer empty
 *****************************************************************************/
int16 SciAGetChar(void)
{
  int16 c;

  if (SciA_RxBuf.count == 0)
  {
    c = EOF;
  }
  else
  {
    c = SciA_RxBuf.buffer[SciA_RxBuf.indx_out++];
    SciA_RxBuf.count--;
    if (SciA_RxBuf.indx_out == SCI_BUF_SIZE)
      SciA_RxBuf.indx_out = 0;
  }
  return (c);
}


/*****************************************************************************
    FUNCTION    SciAPutChar()

    Description:
      Puts the given character into the transmit buffer and enables the
      transmitter interrupt.

    args:
      const char c       character to transmit
    returns:
      void
 *****************************************************************************/
void SciAPutChar(const char c)
{
  while(SciA_TxBuf.count >= SCI_BUF_SIZE)
  {
    // tx buffer is full - wait for an opening
  }

  SciA_TxBuf.buffer[SciA_TxBuf.indx_in++] = c;        // copy char into buffer

  if (SciA_TxBuf.indx_in == SCI_BUF_SIZE)
  {
    SciA_TxBuf.indx_in = 0;                           // if we reach the end, wrap buffer
  }

  SciA_TxBuf.count++;                                 // increment char counter

  SciaRegs.SCIFFTX.bit.TXFFIENA = 1;                  // enable FIFO TX interrupt to start sending
}
// This function defined so stdio function printf can be used
void putch(char c)
{
  SciAPutChar(c);
}


/*****************************************************************************
    FUNCTION    SciAPuts()

    Description:
      Puts a string into the transmit buffer and enables transmitter interrupts.

    Entrance Conditions:

    Exit Conditions:

    args:
      const char *ps    pointer to string to transmit
    returns:
      void
 *****************************************************************************/
void SciAPuts(const char *ps)
{
  while (*ps)
  {
    SciAPutChar(*ps++);
  }
}


/*****************************************************************************
    FUNCTION    SciA_TxFifoIsr()

    Description:
      ISR to transfer characters from tx buffer to SCITX FIFO

    Entrance Conditions:

    Exit Conditions:

    args:
      void
    returns:
      void
 *****************************************************************************/
#pragma CODE_SECTION(SciA_TxFifoIsr,"ramfuncs");
interrupt void SciA_TxFifoIsr(void)
{
  EINT;                                               // re-enable Global interrupt INTM
  if (SciA_TxBuf.count == 0)                          // TX buffer empty?
  {
    SciaRegs.SCIFFTX.bit.TXFFIENA = 0;                // disable TX interrupt, transmitter will send rest of FIFO
  }
  else
  {
    SciaRegs.SCITXBUF = SciA_TxBuf.buffer[SciA_TxBuf.indx_out++];// copy char from buffer to FIFO

    if (SciA_TxBuf.indx_out == SCI_BUF_SIZE)          // if we reach the end, wrap buffer
    {
      SciA_TxBuf.indx_out = 0;
    }

    SciA_TxBuf.count--;                               // decrement char counter
  }

  SciaRegs.SCIFFTX.bit.TXFFINTCLR = 1;                // Clear SCITX Interrupt flag in the peripheral
  PieCtrlRegs.PIEACK.all         |= PIEACK_GROUP9;    // ACK interrupt in the PIE
}


/*****************************************************************************
    FUNCTION    SciA_RxFifoIsr()

    Description:
      ISR to transfer characters from SCIRX FIFO to rx buffer

    Entrance Conditions:

    Exit Conditions:

    args:
      void
    returns:
      void
 *****************************************************************************/
#pragma CODE_SECTION(SciA_RxFifoIsr,"ramfuncs");
interrupt void SciA_RxFifoIsr(void)
{
  Uint16 tmp;

  EINT;                                                         // re-enable Global interrupt INTM
  if (SciaRegs.SCIRXST.bit.RXERROR)
    {
      // if we have an error, flush FIFO and reset SCI
      SciaRegs.SCIFFRX.bit.RXFIFORESET = 0;
      SciaRegs.SCICTL1.bit.SWRESET     = 0;
      SciaRegs.SCIFFRX.bit.RXFIFORESET = 1;
      SciaRegs.SCICTL1.bit.SWRESET     = 1;
    }
  else
    {                                                           //the following loop will empty the hardware fifo,
                                                                //chars will be tossed if software buffer is full   
      while (SciaRegs.SCIFFRX.bit.RXFFST != 0)                  // get chars from FIFO into rx buffer
        {
          tmp = SciaRegs.SCIRXBUF.all;                          // copy char from FIFO to tmp variable
          if (SciA_RxBuf.count<SCI_BUF_SIZE)
            {
              SciA_RxBuf.buffer[SciA_RxBuf.indx_in++] = tmp;    // copy char from tmp variable to buffer

              if (SciA_RxBuf.indx_in == SCI_BUF_SIZE)
                {
                  SciA_RxBuf.indx_in = 0;                       // if we reach the end, wrap buffer
                }

              SciA_RxBuf.count++;                               // increment char counter
            }
        }
    }

  SciaRegs.SCIFFRX.bit.RXFFOVRCLR = 1;                          // Clear FIFO Overflow flag
  SciaRegs.SCIFFRX.bit.RXFFINTCLR = 1;                          // Clear SCIRX Interrupt flag in the peripheral
  PieCtrlRegs.PIEACK.all         |= PIEACK_GROUP9;              // ACK interrupt in the PIE
}


/*****************************************************************************
End of file
******************************************************************************/
