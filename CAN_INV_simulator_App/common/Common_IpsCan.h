/******************************************************************************
  FILENAME: Common_IpsCan.h

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     5/7/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_IPSCAN_H        
#define COMMON_IPSCAN_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP2833x Header files
#include "Common_CanDriver.h"                         // eCAN driver


//****************************************
//  structure definitions
//****************************************
typedef enum eIPS_MSG_ID_FMT
{
  IPS_ID_FMTA = 0,                                    // FMT_A is used for direct communication with INV, PVGEN, BMS, system controller
  IPS_ID_FMTB                                         // FMT_B is used for communicating with the likes of DAQs
} IPS_MSG_ID_FMT_E;

typedef enum eIPS_FMTA_NODE_TYPES                     // Index of device types for IPS format A.
{
  IPS_FMTA_NODE_ALL = 0,
  IPS_FMTA_NODE_MASTER_CONTROLLER,
  IPS_FMTA_NODE_BATTERY_MNGR_SYS,
  IPS_FMTA_NODE_INVERTER,
  IPS_FMTA_NODE_DC2DC_CONV,
  IPS_FMTA_NODE_PV_PFC,
  IPS_FMTA_NODE_GEN,
  // The rest are not defined yet
  IPS_FMTA_NODE_RSVD7,
  IPS_FMTA_NODE_RSVD8,
  IPS_FMTA_NODE_RSVD9,
  IPS_FMTA_NODE_RSVD10,
  IPS_FMTA_NODE_RSVD11,
  IPS_FMTA_NODE_RSVD12,
  IPS_FMTA_NODE_RSVD13,
  IPS_FMTA_NODE_RSVD14,
  IPS_FMTA_NODE_RSVD15,
  IPS_FMTA_NODE_MAX
} IPS_FMTA_NODES_E;

typedef enum eIPS_FMTB_NODE_TYPES                     // Index of device types for IPS format B.
{
  IPS_FMTB_NODE_ALL = 0,
  IPS_FMTB_NODE_DAQ1,
  IPS_FMTB_NODE_BATTERY_MNGR_SYS,
// The rest are not defined yet
  IPS_FMTB_NODE_RSVD3,
  IPS_FMTB_NODE_RSVD4,
  IPS_FMTB_NODE_RSVD5,
  IPS_FMTB_NODE_RSVD6,
  IPS_FMTB_NODE_RSVD7,
  IPS_FMTB_NODE_MAX
} IPS_FMTB_NODES_E;

typedef enum eIPS_MSG_TYPES                           // Index of IPS message types on CAN Bus.
{                                                     // Use in combination with message index.
  IPS_MSG_TYPE_GLOBAL = 0,
  IPS_MSG_TYPE_INVENTORY,
  IPS_MSG_TYPE_STATUS,
  IPS_MSG_TYPE_COMMAND,
  IPS_MSG_TYPE_CONFIG,
// The rest are not defined yet
  IPS_MSG_TYPE_MAX
} IPS_MSG_TYPE_E;

typedef struct IPS_MSG_ID_FMT_A_LAYOUT_S              // IPS CAN ID message format A
{
  Uint32 txNodeAddr : 4;
  Uint32 txNodeType : 4;
  Uint32 rxNodeAddr : 4;                              // Index 0 = global within group
  Uint32 rxNodeType : 4;
  Uint32 msgIndex   : 8;
  Uint32 msgType    : 5;
  Uint32 reserved   : 3;                              // used by eCAN controller for extended addressing and enabling mailbox masks
} IPS_MSG_ID_FMTA_T;

typedef struct IPS_MSG_ID_FMT_B_LAYOUT_S              // IPS CAN ID message format B
{
  Uint32 txNodeAddr : 8;
  Uint32 txNodeType : 3;
  Uint32 rxNodeAddr : 8;                              // Index 0 = global within group
  Uint32 rxNodeType : 3;
  Uint32 msgIndex   : 3;
  Uint32 msgType    : 4;
  Uint32 reserved   : 3;                              // used by eCAN controller for extended addressing and enabling mailbox masks
} IPS_MSG_ID_FMTB_T;

typedef union IPS_MSG_ID_U                            // IPS message ID convenience union data structure.
{                                                     // Allows easy manipulation of different fields of CAN message ID.
  IPS_MSG_ID_FMTA_T MsgIdA;
  IPS_MSG_ID_FMTB_T MsgIdB;
  Uint32            MsgId;
} IPS_MSG_ID_T;

typedef union IPS_DATA_FLOAT32_U
{
  float32 all;
  Uint16  word[2];
} IPS_DATA_FLOAT32_T;

typedef union IPS_DATA_UINT32_U
{
  Uint32 all;
  Uint16 word[2];
} IPS_DATA_UINT32_T;

typedef struct IPS_VERSTR_S                           // version strings for module
{
  char BootFwVer[8];
  char AppFwVer[8];
} IPS_VERSTR_T;

typedef enum eIPS_FLAG_BITS                           // enumerate IPS Dictionary Flag bits
{
  IPS_NONE                    = 0x0000,
  IPS_FUNC                    = 0x0001,               // data pointer is special function
  IPS_NV                      = 0x0002,               // update NV data after write
  IPS_RO                      = 0x0004,               // data is read only
} IPS_FLAG_E;

typedef enum eIPS_DATA_TYPE                           // enumerate IPS Dictionary DataType
{
  IPS_BYTE,
  IPS_WORD16,
  IPS_WORD32,
  IPS_WORD64,
  IPS_FLOAT32,
  IPS_STRING,  
} IPS_TYPE_E;

typedef union IPS_FUNC_DATA_POINTER_U                 // IPS Dictionary Union of Data Pointers
{
  const volatile void   *pV;                          // generic void pointer
  Uint16   *pB;                                       // pointer to Byte (8-bit packed into TI 16-bit char)
  Uint16   *pW;                                       // pointer to word (16 bit)
  Uint32   *pL;                                       // pointer to long word (32 bit)
  Uint64   *pLL;                                      // pointer to long long word (64 bit)
  float    *pFl;                                      // pointer to float (32 bit)
  char     *pS;                                       // pointer to string (array of char)
  Boolean (*pF)(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
                                                      // function pointer for special processing
} IPS_FDP_T;

typedef struct IPS_TABLE_S                            // IPS Dictionary Table structure
{
  Uint16      msgTI;                                  // message Type(MSB)/Index(LSB)
  IPS_FLAG_E  flag;                                   // IPS data bit-flag options
  IPS_TYPE_E  type;                                   // IPS data pointer type
  Uint16      size;                                   // size of data in bytes
  IPS_FDP_T   pIPS;                                   // pointer to IPS data storage or function
} IPS_TABLE_T;


//****************************************
//  constant definitions
//****************************************
#define IPS_ALL_MASK                      (0x1FFFFFFF)
#define IPS_NONE_MASK                     (0x00000000)

#define IPS_RESET_TIMEOUT                 (10000)     // CAN hang reset timeout in ms
#define IPS_RESET_MAX_TRIES               (3)


//****************************************
//  global variable definitions 
//****************************************
extern IPS_VERSTR_T IpsVerStr;                        // Ips Version Strings for module


//****************************************
//  function macro definitions
//****************************************


//****************************************
//  function prototypes 
//****************************************
extern void IpsCanInit(CAN_HANDLE_T *CanHandle, IPS_MSG_ID_FMT_E format, Uint16 NodeType, Uint16 NodeAddr);
extern void IpsCanHandler(CAN_HANDLE_T *CanHandle, const IPS_TABLE_T *DevTable);
extern IPS_MSG_ID_T IpsCanBuildTxMsgId(CAN_HANDLE_T *pCanHandle, IPS_MSG_ID_T *pMsg);


#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
