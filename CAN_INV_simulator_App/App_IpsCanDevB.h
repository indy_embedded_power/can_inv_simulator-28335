/******************************************************************************
  FILENAME: App_IpsCanDevB.h

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     5/7/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/
#ifndef APP_IPSCANDEVB_H        
#define APP_IPSCANDEVB_H

#ifdef __cplusplus
extern "C" {
#endif
//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP2833x Header files
#include "Common_IpsCan.h"                            // IPS header file


//****************************************
//  structure definitions
//****************************************
typedef struct IPS_CAN_CMD_STATUS_BITS_S
{
  Uint16 PwmPh1       : 1;
  Uint16 PwmPh2       : 1;
  Uint16 PwmPh1and2   : 1;
  Uint16 Cb           : 1;
  Uint16 Led          : 1;
  Uint16 LedBlinkSlow : 1;
  Uint16 LedBlinkFast : 1;
  Uint16 ClrFaults    : 1;
  Uint16 DbugMode     : 1;
  Uint16 Rsvd9        : 1;
  Uint16 Rsvd10       : 1;
  Uint16 Rsvd11       : 1;
  Uint16 Rsvd12       : 1;
  Uint16 Rsvd13       : 1;
  Uint16 Rsvd14       : 1;
  Uint16 Rsvd15       : 1;
} IPS_CAN_CMD_STATUS_BITS_T;                          // added for simulator command status bits

typedef union IPS_CAN_CMD_STATUS_U
{
  Uint16                    all;
  IPS_CAN_CMD_STATUS_BITS_T bit;
} IPS_CAN_CMD_STATUS_T;                               // added for simulator command status bits


//****************************************
//  constant definitions
//****************************************
#define IPS_CAN_DEVA_TYPE                 IPS_FMTA_NODE_RSVD15        // eCANA not used on inverter
#define IPS_CAN_DEVB_TYPE                 IPS_FMTA_NODE_INVERTER      // eCANB device type is inverter


//****************************************
//  global variable definitions 
//****************************************
extern const IPS_TABLE_T IpsCanDevBTable[];

extern IPS_CAN_CMD_STATUS_T IpsCanSimCmd;             // added for simulator command status bits


//****************************************
//  function macro definitions
//****************************************


//****************************************
//  function prototypes 
//****************************************


#ifdef __cplusplus
}
#endif

#endif

/*******************************************************************************
  End of file
*******************************************************************************/
