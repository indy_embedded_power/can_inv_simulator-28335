/******************************************************************************
  FILENAME: App_IpsCanDevB.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     5/7/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "App_IpsCanDevB.h"
#include "App_InvCtrl.h"                              // prototypes for InvCtrl
#include "App_Faults.h"                               // prototypes for Faults
#include "App_System.h"                               // prototypes for SystemStatus

#include "Common_NvVars.h"                            // prototypes for NvVars
#include "Common_Led.h"                               // prototypes for LED control


//****************************************
//  local function prototypes
//****************************************
// IPS helper functions prototypes
// These prototypes are at the top of this file so that they can be used in IpsCanDevBTable[] definition below
Boolean IpsCan_UpdateNodeAddr(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);

Boolean IpsCan_Heartbeat(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_ACVandACI(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_ACPandDCV(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_ACFrequency(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);

Boolean IpsCan_CmdPwmPh1(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_CmdPwmPh2(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_CmdPwmPh1and2(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_CmdCb(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_CmdLed(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_CmdLedBlinkSlow(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_CmdLedBlinkFast(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_CmdClrFaults(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_CmdEnterDbugMode(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);

Boolean IpsCan_SetSineAmp1and2(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);
Boolean IpsCan_SetVout1and2(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg);





//****************************************
//  global variables
//****************************************
IPS_CAN_CMD_STATUS_T IpsCanSimCmd;

const IPS_TABLE_T IpsCanDevBTable[] = 
{
//{msgTI  , flag      , data-type   , size  , data-or-function pointer      }
// IPS_MSG_TYPE_INVENTORY
  {0x0101 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_UpdateNodeAddr         },
  {0x0102 , IPS_NV    , IPS_STRING  , 8     , &NvVars.Var.ModelNumber[0]    },
  {0x0103 , IPS_NV    , IPS_STRING  , 8     , &NvVars.Var.SerialNumber1[0]  },
  {0x0104 , IPS_NV    , IPS_STRING  , 8     , &NvVars.Var.SerialNumber2[0]  },
  {0x0105 , IPS_NV    , IPS_STRING  , 8     , &NvVars.Var.SeriesNumber[0]   },
  {0x0106 , IPS_NV    , IPS_STRING  , 8     , &NvVars.Var.HwVersion[0]      },
  {0x0107 , IPS_RO    , IPS_STRING  , 8     , &IpsVerStr.AppFwVer[0]        },
  {0x0108 , IPS_RO    , IPS_STRING  , 8     , &IpsVerStr.BootFwVer[0]       },
// IPS_MSG_TYPE_STATUS
  {0x0201 , IPS_FUNC  , IPS_BYTE    , 8     , IpsCan_Heartbeat              },
  {0x0202 , IPS_FUNC  , IPS_WORD16  , 8     , IpsCan_ACVandACI              },
  {0x0203 , IPS_FUNC  , IPS_WORD16  , 8     , IpsCan_ACPandDCV              },
  {0x0204 , IPS_FUNC  , IPS_WORD16  , 8     , IpsCan_ACFrequency            },
  {0x0223 , IPS_RO    , IPS_WORD64  , 8     , &SystemStatus.all             },
  {0x022E , IPS_RO    , IPS_WORD64  , 8     , &FaultStatus.all              },
  {0x0237 , IPS_RO    , IPS_WORD64  , 8     , &FaultWarningStatus.all       },
// IPS_MSG_TYPE_COMMAND
  {0x0301 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdPwmPh1              },
  {0x0302 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdPwmPh2              },
  {0x0303 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdPwmPh1and2          },
  {0x0306 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdCb                  },
  {0x0307 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdLed                 },
  {0x0308 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdLedBlinkSlow        },
  {0x0309 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdLedBlinkFast        },
  {0x030A , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdClrFaults           },
  {0x0313 , IPS_FUNC  , IPS_BYTE    , 1     , IpsCan_CmdEnterDbugMode       },
// IPS_MSG_TYPE_CONFIG
  {0x0401 , IPS_NONE  , IPS_FLOAT32 , 4     , &InvCtrlSinAmpPu[0]           },
  {0x0402 , IPS_NONE  , IPS_FLOAT32 , 4     , &InvCtrlSinAmpPu[1]           },
  {0x0403 , IPS_FUNC  , IPS_FLOAT32 , 4     , IpsCan_SetSineAmp1and2        },
  {0x0404 , IPS_NV    , IPS_FLOAT32 , 4     , &NvVars.Var.SetPointVout[0]   },
  {0x0405 , IPS_NV    , IPS_FLOAT32 , 4     , &NvVars.Var.SetPointVout[1]   },
  {0x0406 , IPS_FUNC  , IPS_FLOAT32 , 4     , IpsCan_SetVout1and2           },
  {0x041A , IPS_NV    , IPS_FLOAT32 , 4     , &NvVars.Var.SetPointUV        },
  {0x041B , IPS_NV    , IPS_FLOAT32 , 4     , &NvVars.Var.SetPointOV        },
  {0x041C , IPS_NV    , IPS_FLOAT32 , 4     , &NvVars.Var.SetPointOC        },
  {0x041D , IPS_NV    , IPS_FLOAT32 , 4     , &NvVars.Var.SetPointOVPK      },
  {0x041E , IPS_NV    , IPS_FLOAT32 , 4     , &NvVars.Var.SetPointUVLO      },
  {0x041F , IPS_NV    , IPS_FLOAT32 , 4     , &NvVars.Var.SetPointOVLO      },

  // NULL structure must be last
  {NULL   , IPS_NONE  , IPS_BYTE    , 0     , NULL                          }
};
  


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************
static Uint16 IpsCan_HeartbeatFlag = 1;




/******************************************************************************
    FUNCTION    IpsCan_UpdateNodeAddr()

    Description:

    Entrance Conditions:
      corret length for write operation checked OK before calling this function

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_UpdateNodeAddr(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  struct MBOX msg;
  IPS_MSG_ID_T txMsgId, rxMsgId;
  IPS_MSG_ID_T type, addr, mask;
  
  rxMsgId.MsgId = pMsg->MSGID.all;
  
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, send NodeIdx to requester
    txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId);

    // load up the message mailbox
    msg.MSGID.all       = txMsgId.MsgId;
    msg.MSGCTRL.bit.DLC = 1;
    msg.MDL.byte.BYTE0  = pCanHandle->NodeIdx;
    msg.MDL.byte.BYTE1  = 0;
    msg.MDL.byte.BYTE2  = 0;
    msg.MDL.byte.BYTE3  = 0;
    msg.MDH.all         = 0;
    // transmit the response
    CanTxMessage(pCanHandle, &msg);
  }
  else
  {
    // write data to NodeIdx and NvVars and update
    pCanHandle->NodeIdx = pMsg->MDL.byte.BYTE0;       // copy new Node Address to handle
    if (pCanHandle->pECanRegs == &ECanaRegs)          // copy to appropriate NvVars variable based on which eCAN we're using
    {
      NvVars.Var.CanAddrA = pCanHandle->NodeIdx;
    }
    else
    {
      NvVars.Var.CanAddrB = pCanHandle->NodeIdx;
    }
    NV_UPDATE_SET();                                  // schedule NvVars flash update

    type.MsgId = IPS_NONE_MASK;                       // start with msg cleared
    addr.MsgId = IPS_NONE_MASK;                       // start with msg cleared
    mask.MsgId = IPS_ALL_MASK;                        // start with allow all
    if (pCanHandle->NodeFmt == IPS_ID_FMTA)           // if FMTA, set up MBOX filters according to FMTA ID
    {
      type.MsgIdA.rxNodeType = pCanHandle->NodeType;  // MSGID with rxNodeType set for type A format msg
      addr.MsgIdA.rxNodeAddr = pCanHandle->NodeIdx;   // MSGID with rxNodeAddr set for type A format msg
      mask.MsgIdA.rxNodeType = 0;                     // set mask to match Type for type A format msg
      mask.MsgIdA.rxNodeAddr = 0;                     // set mask to match Addr for type A format msg
    }
    else // NodeFmt == IPS_ID_FMTB                    // if FMTB, set up MBOX filters according to FMTB ID
    {
      type.MsgIdB.rxNodeType = pCanHandle->NodeType;  // MSGID with rxNodeType set for type B format msg
      addr.MsgIdB.rxNodeAddr = pCanHandle->NodeIdx;   // MSGID with rxNodeAddr set for type B format msg
      mask.MsgIdB.rxNodeType = 0;                     // set mask to match Type for type B format msg
      mask.MsgIdB.rxNodeAddr = 0;                     // set mask to match Addr for type B format msg
    }
    // reset CAN acceptance filters with new Node Address
    CanSetAccpMask(pCanHandle, type.MsgId, addr.MsgId, mask.MsgId); 
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_Heartbeat()

    Description:
      Send device heartbeat message.
      Only sent when asked for with request msg from master

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_Heartbeat(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  struct MBOX msg;
  IPS_MSG_ID_T txMsgId,rxMsgId;

  rxMsgId.MsgId = pMsg->MSGID.all;

  if (pMsg->MSGCTRL.bit.DLC == 0)                     // this should be a read only msg, don't process if it has a length
  {
    txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID
  
    // load up the message mailbox
    msg.MSGID.all       = txMsgId.MsgId;
    msg.MSGCTRL.bit.DLC = 8;
    msg.MDL.byte.BYTE0  = IpsCan_HeartbeatFlag;       // Notifications Bits
    msg.MDL.byte.BYTE1  = InvCtrlMeas.Tstk[CHAN1];    // Stack Temp1
    msg.MDL.byte.BYTE2  = InvCtrlMeas.Tstk[CHAN2];    // Stack Temp2
    msg.MDL.byte.BYTE3  = InvCtrlMeas.Text[CHAN1];    // Ext Temp1
    msg.MDH.byte.BYTE4  = InvCtrlMeas.Text[CHAN2];    // Ext Temp2
    msg.MDH.byte.BYTE5  = 0x00;  
    msg.MDH.byte.BYTE6  = 0x00;  
    msg.MDH.byte.BYTE7  = 0x00;  
    
    IpsCan_HeartbeatFlag ^= 1;
    
    // transmit the response
    CanTxMessage(pCanHandle, &msg);
  }
  else
  {
    return FALSE;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_ACVandACI()

    Description:
      Send Vrms and Irms for Chan 1&2 as integer values *100

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_ACVandACI(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  struct MBOX msg;
  IPS_MSG_ID_T txMsgId,rxMsgId;

  rxMsgId.MsgId = pMsg->MSGID.all;

  if (pMsg->MSGCTRL.bit.DLC == 0)                     // this should be a read only msg, don't process if it has a length
  {
    txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID

    // load up the message mailbox
    msg.MSGID.all         = txMsgId.MsgId;
    msg.MSGCTRL.bit.DLC   = 8;
    msg.MDL.word.HI_WORD  = InvCtrlMeas.Vout[CHAN1] * 100;  // Vrms out Chan 1
    msg.MDL.word.LOW_WORD = InvCtrlMeas.Iout[CHAN1] * 100;  // Irms out Chan 1
    msg.MDH.word.HI_WORD  = InvCtrlMeas.Vout[CHAN2] * 100;  // Vrms out Chan 2
    msg.MDH.word.LOW_WORD = InvCtrlMeas.Iout[CHAN2] * 100;  // Irms out Chan 2

    // transmit the response
    CanTxMessage(pCanHandle, &msg);
  }
  else
  {
    return FALSE;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_ACPandDCV()

    Description:
      Send Prms for Chan 1&2 as integer values * 10
      Send Vdc for DC link ad integer value * 100
    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_ACPandDCV(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  struct MBOX msg;
  IPS_MSG_ID_T txMsgId,rxMsgId;

  rxMsgId.MsgId = pMsg->MSGID.all;

  if (pMsg->MSGCTRL.bit.DLC == 0)                     // this should be a read only msg, don't process if it has a length
  {
    txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID

    // load up the message mailbox
    msg.MSGID.all         = txMsgId.MsgId;
    msg.MSGCTRL.bit.DLC   = 8;
    msg.MDL.word.HI_WORD  = InvCtrlMeas.Pout[CHAN1] / 10.0f;  // Prms out Chan 1
    msg.MDL.word.LOW_WORD = InvCtrlMeas.Pout[CHAN2] / 10.0f;  // Prms out Chan 2
    msg.MDH.word.HI_WORD  = InvCtrlMeas.VdcLink * 100;        // Vdc in DC Link
    msg.MDH.word.LOW_WORD = 0x0000;

    // transmit the response
    CanTxMessage(pCanHandle, &msg);
  }
  else
  {
    return FALSE;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_ACFrequency()

    Description:
      Send Ferquency of Chan 1&2 as integer values * 100

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_ACFrequency(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  struct MBOX msg;
  IPS_MSG_ID_T txMsgId,rxMsgId;

  rxMsgId.MsgId = pMsg->MSGID.all;

  if (pMsg->MSGCTRL.bit.DLC == 0)                     // this should be a read only msg, don't process if it has a length
  {
    txMsgId = IpsCanBuildTxMsgId(pCanHandle, &rxMsgId); // create return ID

    // load up the message mailbox
    msg.MSGID.all         = txMsgId.MsgId;
    msg.MSGCTRL.bit.DLC   = 8;
    msg.MDL.word.HI_WORD  = InvCtrlMeas.Freq[CHAN1] * 100;  // Frequency out Chan 1
    msg.MDL.word.LOW_WORD = InvCtrlMeas.Freq[CHAN2] * 100;  // Frequency out Chan 2
    msg.MDH.word.HI_WORD  = 0x0000;
    msg.MDH.word.LOW_WORD = 0x0000;

    // transmit the response
    CanTxMessage(pCanHandle, &msg);
  }
  else
  {
    return FALSE;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdPwmPh1()

    Description:
      Turn PWM12 on/off

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdPwmPh1(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    IpsCanSimCmd.bit.PwmPh1 = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdPwmPh2()

    Description:
      Turn PWM45 on/off

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdPwmPh2(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    IpsCanSimCmd.bit.PwmPh2 = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdPwmPh1and2()

    Description:
      Turn all PWMs on/off

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdPwmPh1and2(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    IpsCanSimCmd.bit.PwmPh1and2 = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdCb()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdCb(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    IpsCanSimCmd.bit.Cb = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdLed()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdLed(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    if (pMsg->MDL.byte.BYTE0)
    {
      LedSetMode(LED_MODE_ON);
    }
    else
    {
      LedSetMode(LED_MODE_OFF);
    }
    IpsCanSimCmd.bit.Led = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdLedBlinkSlow()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdLedBlinkSlow(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    if (pMsg->MDL.byte.BYTE0)
    {
      LedSetMode(LED_MODE_BLINK_SLOW);
    }
    else
    {
      LedSetMode(LED_MODE_OFF);
    }
    IpsCanSimCmd.bit.LedBlinkSlow = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdLedBlinkFast()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdLedBlinkFast(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    if (pMsg->MDL.byte.BYTE0)
    {
      LedSetMode(LED_MODE_BLINK_FAST);
    }
    else
    {
      LedSetMode(LED_MODE_OFF);
    }
    IpsCanSimCmd.bit.LedBlinkFast = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdClrFaults()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdClrFaults(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    if (pMsg->MDL.byte.BYTE0)
    {
      FaultStatus.all = 0;
      LedSetMode(LED_MODE_OFF);
    }
    IpsCanSimCmd.bit.ClrFaults = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_CmdEnterDbugMode()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_CmdEnterDbugMode(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    SystemStatus.bit.Test = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
    IpsCanSimCmd.bit.DbugMode = (pMsg->MDL.byte.BYTE0) ? 1 : 0;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_SetSineAmp1and2()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB) 
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_SetSineAmp1and2(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  IPS_DATA_FLOAT32_T temp;

  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    temp.word[1] = pMsg->MDL.word.HI_WORD;
    temp.word[0] = pMsg->MDL.word.LOW_WORD;

    if ((temp.all >= 1.0f) || (temp.all < 0.0f))
    {
      return FALSE;
    }
    InvCtrlSinAmpPu[CHAN1] = temp.all;
    InvCtrlSinAmpPu[CHAN2] = temp.all;
  }

  return TRUE;
}


/******************************************************************************
    FUNCTION    IpsCan_SetVout1and2()

    Description:

    Entrance Conditions:

    Exit Conditions:

    args:
      CAN_HANDLE_T *pCanHandle    - pointer to handle for CAN peripheral used (&CanA/&CanB)
      struct MBOX  *pMsg          - pointer to hold msg
                        ->MSGID.all
                        ->MSGCTRL.bit.DLC
                        ->MDL.all
                        ->MDH.all

    returns:
      Boolean   TRUE if successful, FALSE if failed
*******************************************************************************/
Boolean IpsCan_SetVout1and2(CAN_HANDLE_T *pCanHandle, struct MBOX *pMsg)
{
  IPS_DATA_FLOAT32_T temp;

  if (pMsg->MSGCTRL.bit.DLC == 0)
  {
    // request for data, ignore for command
    return FALSE;
  }
  else
  {
    // write data, translate command
    temp.word[1] = pMsg->MDL.word.HI_WORD;
    temp.word[0] = pMsg->MDL.word.LOW_WORD;

    NvVars.Var.SetPointVout[CHAN1] = temp.all;
    NvVars.Var.SetPointVout[CHAN2] = temp.all;
    NV_UPDATE_SET();
  }

  return TRUE;
}


/*******************************************************************************
  End of file
*******************************************************************************/
