
//###########################################################################
//
// FILE:   DSP2833x_MemCopy.h
//
// TITLE:  Headerfile for MemCopy.c
//
//###########################################################################
// $Copyright: Copyright (C) 2007-2016 Texas Instruments Incorporated -
//             http://www.ti.com/ ALL RIGHTS RESERVED $
//###########################################################################

#ifndef DSP2833X_MEMCOPY_H_
#define DSP2833X_MEMCOPY_H_

#include "DSP2833x_Device.h"

extern void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr);

#endif /* DSP2833X_MEMCOPY_H_ */
