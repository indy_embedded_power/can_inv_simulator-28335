/******************************************************************************
  FILENAME: Common_NvVars.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     2/18/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "Common_NvVars.h"                            // prototypes for NvVars
#include "Common_SysCtrl.h"                           // Prototypes for watchdog timer manipulation
#include "Flash2833x_API_Library.h"                   // prototypes for Flash API

//****************************************
//  global variables
//****************************************
Uint16 NvUpdateFlag = NV_CLEAR;
APPTMR_HANDLE_T NvUpdateTimerHandle;
NV_VARS_T NvVars;
const NV_VARS_T NvDefault =
{ 0xFFFF,                             // Checksum
  0x0000,                             // PageGood
  (sizeof(NV_T)),                     // size of structure in words
  1,                                  // AutoBootFlag

  0x01,                               // eCANA node address
  0x01,                               // eCANB node address
  {'I','N','D','Y',' ','I','N','V'},  // Device Model #
  {'S','E','R','I','A','L','N','1'},  // Most significant characters of serial #
  {'S','E','R','I','A','L','N','2'},  // Least significant characters of serial #
  {'X','X','.','Y','Y','.','Z','Z'},  // Series #
  {'X','X','.','Y','Y','.','Z','Z'},  // HW Version #

  //  Set Points
  (80.0),                       // Peak Current Limit Set Point in Amps
  (55.0),                       // Over Current Alarm Set Point in Arms
  (180.0),                      // Under Voltage Alarm Set Point in Vrms
  (275.0),                      // Over Voltage Alarm Set Point in Vrms
  (390.0),                      // Peak Over Voltage Alarm Set Point in V
  {240.0, 240.0},               // Vout CHAN1 & CHAN2 in Vrms
  (1.0),                        // IlimVr current limit virtual resistance factor
  (0.5),                        // Default fan PWM Set Point in pu
  (400.0),                      // DcLink Under Voltage Lockout Set Point in V
  (600.0),                      // DcLink Over Voltage Lockout Set Point in V
  (1)                           // Output transformer scale factor, i.e. 0.5 = 2:1 turns ratio

};


//****************************************
//  local constant definitions
//****************************************
#define NV_ERASED_WORD          0xFFFF
#define NV_NUM_BANKS            (16)


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************
static int16 Nv_ActiveBank;
#pragma DATA_SECTION(Nv_Rom, "nv_vars");
static NV_VARS_T Nv_Rom[NV_NUM_BANKS];


//****************************************
//  local function prototypes
//****************************************
static void NvVars_Copy(NV_VARS_T *pDest, const NV_VARS_T *pSource);
static Uint16 NvVars_CheckSum(const NV_VARS_T *pSource);





/******************************************************************************
    FUNCTION    NvVarsInit()

    Description:
      Initialize NvVars

    Entrance Conditions:

    Exit Conditions:
      uses/modifies global var NvUpdateFlag

    args:
      void

    returns:
      void
*******************************************************************************/
void NvVarsInit(void)
{
  Uint16 indx;
  int16  bank = -1;
  Uint16 largest = 0;

  Flash_CPUScaleFactor = SCALE_FACTOR;                                    // Initialize Flash API global variables
  Flash_CallbackPtr    = NULL;                                            //

  for (indx = 0; indx < NV_NUM_BANKS; indx++)                             // find latest Flash bank to operate from
  {                                                                       // it will have the largest PageGood number
    if (Nv_Rom[indx].Var.PageGood != NV_ERASED_WORD)
    {
      if (Nv_Rom[indx].Var.PageGood >= largest)
      {
        largest = Nv_Rom[indx].Var.PageGood;
        bank = indx;
      }
    }
  }

  if (bank == -1)                                                         // Flash is empty
  {
    NvVars_Copy(&NvVars, &NvDefault);                                     // copy defaults to RAM variable
    NvVars.Var.Checksum = NvVars_CheckSum(&NvVars);                       // calculate new checksum and store in RAM variable
    Nv_ActiveBank = -1;                                                   // this ensures update happens to bank 0
    NV_UPDATE_SET();                                                      // set flag to update in handler
  }
  else                                                                    // Flash contains data
  {
    if (Nv_Rom[bank].Var.Checksum == NvVars_CheckSum(&Nv_Rom[bank]))      // bank contains ok data, copy ROM values to RAM variable
    {
      Nv_ActiveBank = bank;                                               // set ActiveBank to this bank

      if (Nv_Rom[Nv_ActiveBank].Var.Size < NvDefault.Var.Size)            // Default NvVars structure bigger than existing ROM structure due to program update
      {
        NvVars_Copy(&NvVars, &NvDefault);                                 // copy all defaults
        for (indx = 0 ; indx < Nv_Rom[Nv_ActiveBank].Var.Size ; indx++)   // copy saved ROM data over defaults
        {
          NvVars.Copy[indx] = Nv_Rom[Nv_ActiveBank].Copy[indx];
        }
        NvVars.Var.Size = NvDefault.Var.Size;                             // keep new size
        NvVars.Var.Checksum = NvVars_CheckSum(&NvVars);                   // calculate new checksum and store in RAM variable
        NV_UPDATE_SET();                                                  // set flag to update in handler
      }
      else                                                                // ROM structure same size as default structure
      {
        NvVars_Copy(&NvVars, &Nv_Rom[Nv_ActiveBank]);                     // just copy from Flash to RAM
      }
    }
    else                                                                  // existing data in Flash is corrupt, revert to default values
    {
      NvVars_Copy(&NvVars, &NvDefault);                                   // copy defaults to RAM variable
      NvVars.Var.PageGood = largest;                                      // keep the PageGood value
      NvVars.Var.Checksum = NvVars_CheckSum(&NvVars);                     // calculate new checksum and store in RAM variable
      Nv_ActiveBank = NV_NUM_BANKS;                                       // this ensures corrupted Flash gets erased
      NV_UPDATE_SET();                                                    // set flag to update in handler
    }
  }
  NvUpdateTimerHandle = AppTmrStart(NV_UPDATE_TIMEOUT);                   // start update timer
}


/******************************************************************************
    FUNCTION    NvVarsHandler()

    Description:
      Process Nv_ROM updates

    Entrance Conditions:

    Exit Conditions:
      uses/modifies global var NvUpdateFlag

    args:
      void

    returns:
      void
*******************************************************************************/
void NvVarsHandler(void)
{
  int16  bank;
  Uint16 eraseFlag = FALSE;
  FLASH_ST FlashStatus;

  if (AppTmrChkStatus(NvUpdateTimerHandle) == APPTMR_SUSPENDED)           // check for Update Timeout
  {
    AppTmrRestart(NvUpdateTimerHandle, NV_UPDATE_TIMEOUT);                // restart update timer
    
    if (NvUpdateFlag)                                                     // check if there is something to update
    {
      bank = Nv_ActiveBank + 1;                                           // get new bank to program
      NvVars.Var.PageGood++;                                              // update PageGood to highest
      NvVars.Var.Checksum = NvVars_CheckSum(&NvVars);                     // update checksum
      
      if (bank >= NV_NUM_BANKS)                                           // if we go past the last bank, wrap around
      {
        eraseFlag = TRUE;
        bank      = 0;
      }
      
      if (NvVars.Var.PageGood >= 0xFFFE)                                  // wrap around PageGood before going over
      {
        eraseFlag = TRUE;
        bank      = 0;                                                    // go back to bank 0 on PageGood rollover
        NvVars.Var.PageGood = 1;                                          // wrap PageGood around to beginning
        NvVars.Var.Checksum = NvVars_CheckSum(&NvVars);                   // update checksum
      }
      
      DINT;                                                               // Disable global interrupts
      DRTM;                                                               // Disable real time debug
      SysCtrlDisableDog();                                                // disable watchdog timer
      if (eraseFlag)
      {
        eraseFlag = FALSE;
        Flash_Erase(SECTORB,&FlashStatus);                                // erase Nv_Rom sector
      }
    
      Flash_Program(&Nv_Rom[bank].Copy[0], &NvVars.Copy[0], NVMEM_COPY_SIZE, &FlashStatus); // program flash with NvVars data

      SysCtrlServiceDog();                                                // Service watchdog prior to reeanbling watching
//x watchdog disabled for development debug
      SYSCTRL_ENABLE_WATCHDOG();                                          // Re-enable watchdog
      EINT;                                                               // Enable Global interrupt INTM
      ERTM;                                                               // Enable Global realtime interrupt DBGM

      Nv_ActiveBank = bank;                                               // this is now the new active bank
      NV_UPDATE_CLR();                                                    // clear the update flag
    }
  }
}


/******************************************************************************
    FUNCTION    NvVars_Copy()

    Description:
      copy NvVars structure

    Entrance Conditions:

    Exit Conditions:

    args:
      pDest     pointer to Destination structure
      pSource   pointer to Source structure

    returns:
      void
*******************************************************************************/
static void NvVars_Copy(NV_VARS_T *pDest, const NV_VARS_T *pSource)
{
  Uint16 indx;
  
  for (indx = 0 ; indx < NVMEM_COPY_SIZE ; indx++)
  {
    pDest->Copy[indx] = pSource->Copy[indx];
  }
}


/******************************************************************************
    FUNCTION    NvVars_CheckSum()

    Description:
      calculate 16-bit checksum of NvVars structure except first word

    Entrance Conditions:

    Exit Conditions:

    args:
      pSource    pointer to Source structure

    returns:
      chksum
*******************************************************************************/
static Uint16 NvVars_CheckSum(const NV_VARS_T *pSource)
{
  Uint16 chksum;
  Uint16 indx;
  
  chksum = 0;
  for (indx = 1; indx < NVMEM_COPY_SIZE; indx++)
  {
    chksum += pSource->Copy[indx];
  }
  return (chksum);
}






/*******************************************************************************
  End of file
*******************************************************************************/
