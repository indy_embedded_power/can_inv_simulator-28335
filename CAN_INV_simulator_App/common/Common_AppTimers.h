/******************************************************************************
  FILENAME: Common_AppTimers.h

 *****************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     1/25/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 *****************************************************************************
  Notes:

*******************************************************************************/
#ifndef COMMON_APPTIMER_HEADER
#define COMMON_APPTIMER_HEADER

#ifdef __cplusplus
extern "C" {
#endif

//****************************************
//  include files
//****************************************
#include "Common_Device.h"                            // DSP280xx Header files


//****************************************
//  constant definitions
//****************************************
#define APPTMR_MAX            32                      // maximum number of application timers to allocate (range 1-32)

#define APPTMR_SUSPENDED      0
#define APPTMR_RUNNING        1
#define APPTMR_NOT_FOUND      0xFFFF


//****************************************
// typedef definitions 
//****************************************
typedef unsigned int APPTMR_HANDLE_T;
typedef unsigned int APPTMR_STATUS_T;

//****************************************
// global variable definitions 
//****************************************


//****************************************
// function prototypes 
//****************************************
void AppTmrInit(void);
APPTMR_HANDLE_T AppTmrStart(Uint32 timeout);
APPTMR_STATUS_T AppTmrChkStatus(APPTMR_HANDLE_T tmr_id);
void AppTmrDealloc(APPTMR_HANDLE_T tmr_id);
void AppTmrStop(APPTMR_HANDLE_T tmr_id);
void AppTmrRestart(APPTMR_HANDLE_T tmr_id, Uint32 timeout);
void AppTmrHandler(void);

#ifdef __cplusplus
}
#endif

#endif
/******************************************************************************
  End of file
*******************************************************************************/
