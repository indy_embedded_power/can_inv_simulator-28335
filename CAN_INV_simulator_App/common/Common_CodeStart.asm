;******************************************************************************
; FILENAME: Common_CodeStart.asm
;
;******************************************************************************
; Copyright 2016 ALL RIGHTS RESERVED
;   Indy Power Systems
;   7702 Moller Rd
;   Indianapolis, IN  46268
;
; Created On:     1/21/2017
; Author:         S.Maduzia
; Target Device:  TMS320F28335
;
;******************************************************************************
; Notes:
;   based on TI files:  DSP2833x_CodeStartBranch.asm
;                       DSP2833x_CSMPasswords.asm
;                       DSP2833x_usDelay.asm
;                       DSP2833x_DBGIER.asm
;******************************************************************************

;******************************************************************************
; Function:         code_start
; Description:      Branch to code starting point
; Input Parameters: none
; Return Value:     does not return
;******************************************************************************
    .sect "codestart"
    .global code_start
code_start:
     LB wd_disable       ;Branch to watchdog disable code
;end codestart section


;******************************************************************************
; Function:         wd_disable
; Description:      Disables the watchdog timer and jumps to C init code
; Input Parameters: none
; Return Value:     does not return
;
;******************************************************************************
    .ref _c_int00       ; external reference for C environment entry point
    .text
wd_disable:
    SETC OBJMODE        ;Set OBJMODE for 28x object code
    EALLOW              ;Enable EALLOW protected register access
    MOVZ DP, #7029h>>6  ;Set data page for WDCR register
    MOV @7029h, #0068h  ;Set WDDIS bit in WDCR to disable WD
    EDIS                ;Disable EALLOW protected register access
    LB _c_int00         ;Branch to start of boot.asm in RTS library
;end wd_disable


;******************************************************************************
; Description:
; Section contains values that will be linked into the CSM password locations in flash.
; All 0xFFFF's (erased) is the default value for the password locations.
; Our SW doesnt use password protection, so leave these at default values
;******************************************************************************
    .sect "csm_passwords"
    .int  0xFFFF        ;PWL0 (LSW of 128-bit password)
    .int  0xFFFF        ;PWL1
    .int  0xFFFF        ;PWL2
    .int  0xFFFF        ;PWL3
    .int  0xFFFF        ;PWL4
    .int  0xFFFF        ;PWL5
    .int  0xFFFF        ;PWL6
    .int  0xFFFF        ;PWL7 (MSW of 128-bit password)
; end csm_passwords section


;******************************************************************************
; Description:
; For code security operation, all addresses between 0x33FF80 and
; 0X33fff5 cannot be used as program code or data.  These locations
; must be programmed to 0x0000 when the code security password locations
; (PWL) are programmed.
;******************************************************************************
    .sect "csm_reserved"
    .loop (33FFF5h - 33FF80h + 1)
    .int 0x0000
    .endloop
; end csm_reserved section


;******************************************************************************
; Function:         _DSP28x_usDelay
;
; Description:
;   This is a simple delay function that can be used to insert a specified
;   delay into code.
;
;   This function is only accurate if executed from internal zero-waitstate
;   SARAM. If it is executed from waitstate memory then the delay will be
;   longer then specified.
;
;   To use this function:
;
;     1 - update the CPU clock speed in the Common_CodeStart.h file.
;         For example: #define CPU_RATE 6.667L // for a 150MHz CPU clock speed
;
;     2 - Call this function by using the DELAY_US(A) macro
;         that is defined in the Common_CodeStart.h file.  This macro
;         will convert the number of microseconds specified
;         into a loop count for use with this function.
;         This count will be based on the CPU frequency you specify.
;
;     3 - For the most accurate delay
;       - Execute this function in 0 waitstate RAM.
;       - Disable interrupts before calling the function
;         If you do not disable interrupts, then think of
;         this as an "at least" delay function as the actual
;         delay may be longer.
;
;   There is a 9/10 cycle overhead and each loop takes five cycles.
;   The LoopCount is given by the following formula:
;     DELAY_CPU_CYCLES = 9 + 5*LoopCount
;     LoopCount = (DELAY_CPU_CYCLES - 9) / 5
;   The macro DELAY_US(A) performs this calculation for you
;
; Input Parameters:
;         MOV   AL,#LowLoopCount
;         MOV   AH,#HighLoopCount
;         LCR   _DSP28x_usDelay
;
; Return Value:
;         none
;******************************************************************************
    .def _DSP28x_usDelay
    .sect "ramfuncs"
    .global  _DSP28x_usDelay
_DSP28x_usDelay:
    SUB    ACC,#1
    BF     _DSP28x_usDelay,GEQ    ; Loop if ACC >= 0
    LRETR
;end _DSP28x_usDelay


;******************************************************************************
; Function: _SetDBGIER
;
; Description:
;   Function to set the DBGIER register (for realtime emulation).
;   Function Prototype: void SetDBGIER(Uint16)
;   Useage: SetDBGIER(value);
;   Input Parameters: Uint16 value = value to put in DBGIER register.
;   Return Value: none
;
;******************************************************************************
    .global _SetDBGIER
    .text

_SetDBGIER:
    MOV   *SP++,AL
    POP   DBGIER
    LRETR
; end _SetDBGIER


;******************************************************************************
; End of file
;*****************************************************************************
