/******************************************************************************
  FILENAME: App_System.c

 ******************************************************************************
  Copyright 2017 ALL RIGHTS RESERVED
    Indy Power Systems
    7702 Moller Rd
    Indianapolis, IN  46268

  Created on:     3/4/2017
  Author:         S.Maduzia
  Target Device:  TMS320F28335

 ******************************************************************************
  Notes:

*******************************************************************************/

//****************************************
//  include files
//****************************************
#include "App_System.h"
#include "App_InvCtrl.h"


//****************************************
//  global variables
//****************************************
SYSTEM_STATUS_T SystemStatus;


//****************************************
//  local constant definitions
//****************************************


//****************************************
//  local defines
//****************************************


//****************************************
//  local variables
//****************************************


//****************************************
//  local function prototypes
//****************************************





/******************************************************************************
    FUNCTION    SystemInit()

    Description:
      Initialize SystemStatus

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void SystemInit(void) 
{
  SystemStatus.all = 0x00000000;
}


/******************************************************************************
    FUNCTION    SystemHandler()

    Description:
      Update SystemStatus bits

    Entrance Conditions:

    Exit Conditions:

    args:
      void

    returns:
      void
*******************************************************************************/
void SystemHandler(void) 
{
  //--- BYTE0 bits
  // SystemStatus.bit.CB        updated in CbCtrlHandler()
  // SystemStatus.bit.Test      Updated in Cmd_TestSet()

  //--- BYTE1 bits
  // reserved

  //--- BYTE2 bits
  SystemStatus.bit.Phase1Op      = SystemStatus.bit.Test;
  SystemStatus.bit.Phase1En      = InvCtrlStatus[CHAN1].bit.OutEn;

  //--- BYTE3 bits
  // reserved

  //--- BYTE4 bits
  SystemStatus.bit.Phase2Op      = SystemStatus.bit.Test;
  SystemStatus.bit.Phase2En      = InvCtrlStatus[CHAN2].bit.OutEn;

  //--- BYTE5 bits
  // reserved

  //--- BYTE6 bits
  // reserved

  //--- BYTE7 bits
  // reserved

}



/*******************************************************************************
  End of file
*******************************************************************************/
